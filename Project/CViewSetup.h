#pragma once

class CViewSetup
{
public:

	DECLARE_OFFSET_FUNCTION(GetWidth, 0x10, uintptr_t);
	DECLARE_OFFSET_FUNCTION(GetHeight, 0x18, uintptr_t);
	DECLARE_OFFSET_FUNCTION(GetFOV, 0x38, float);
};
