#include <Windows.h>
#include <memory>
#include <Psapi.h>
#include <chrono>
#include <thread>
#include "Tools.h"
#include "SDK.h"

uintptr_t Tools::FindPattern(const char* szModule, const char* szSignature) noexcept
{
	MODULEINFO modInfo;

	GetModuleInformation(GetCurrentProcess(), GetModuleHandle(szModule), &modInfo, sizeof(MODULEINFO));

	uintptr_t startAddress = reinterpret_cast<uintptr_t>(modInfo.lpBaseOfDll);

	uintptr_t endAddress = startAddress + modInfo.SizeOfImage;

	const char* pat = szSignature;

	uintptr_t firstMatch = 0;

	for (uintptr_t pCur = startAddress; pCur < endAddress; pCur++)
	{
		if (!*pat) return firstMatch;

		if (*(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte(pat))
		{
			if (!firstMatch) firstMatch = pCur;

			if (!pat[2]) return firstMatch;

			if (*(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?') pat += 3;

			else pat += 2;
		}
		else
		{
			pat = szSignature;

			firstMatch = 0;
		}
	}

	ConMsg("[ERROR] Signature %s is outdated in module %s, Please copy the signature and Contact a developer", szSignature, szModule);
        
	return NULL;
}

uintptr_t Tools::CallableFromRelative(uintptr_t Address) noexcept
{
	if (Address == NULL) return NULL;

	return Address + *(uintptr_t*)(Address + 1) + 5;

}

bool Tools::IsCodePtr(void* ptr) noexcept
{
	constexpr const DWORD protect_flags = PAGE_EXECUTE | PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY;

	MEMORY_BASIC_INFORMATION out;

	VirtualQuery(ptr, &out, sizeof out);

	return out.Type
		&& !(out.Protect & (PAGE_GUARD | PAGE_NOACCESS))
		&& out.Protect & protect_flags;
}
