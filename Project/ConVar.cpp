#include "SDK.h"
#include "windows.h"
#include "Virtuals.h"
#include "ConVar.h"

void ConVar::Create(void* ECX, const char* Name, const char* DefaultValue, int Flags, const char* Description, void* Fn)
{
	static auto Create = Tools::FindPattern("engine.dll", "55 8B EC D9 EE 56 FF 75 18");

	using Type = void(__thiscall*)(void*, const char*, const char*, int, const char*, void*);

	reinterpret_cast<Type>(Create)(ECX, Name, DefaultValue, Flags, Description, Fn);
}

const char* ConVar::GetDefault(void) const
{
	return *(const char**)(*(unsigned int*)((unsigned int)this + 28) + 32);
}

const char* ConVar::GetString(void) const
{
	return *(const char**)(*(unsigned int*)((unsigned int)this + 28) + 36);
}

float& ConVar::GetFloat(void) const
{
	return *(float*)(*(unsigned int*)((unsigned int)this + 28) + 44);
}

int& ConVar::GetInt(void) const
{
	return *(int*)(*(unsigned int*)((unsigned int)this + 28) + 48);
}

void ConVar::Revert(void)
{
	SetValue(GetDefault());
}

void ConVar::SetValueAndFlags(const float Value, const int Flags)
{
	SetFlags(Flags);

	GetFloat() = Value;

	GetInt() = (int)Value;
}