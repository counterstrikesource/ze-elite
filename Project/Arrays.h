#ifndef Arrays_H
#define Arrays_H

#include <vector>
#include <string>
#include <array>

class IMaterial;

class CustomTextures // Move to somewhere else
{
public:

	std::vector<IMaterial*> Textures;

	std::vector<std::string> m_szNames{ "Default" };

	void PrepareFiles(const char* Folder) noexcept;

	void AssignTexture(IMaterial*& Pointer, int i) noexcept;
};

void ProcessEffectsList() noexcept; // Same as CustomTextures

inline CustomTextures Hands, World, Background;

inline std::vector<std::string> HitmarkerOverlays;

inline std::pair<CustomTextures, std::vector<void*>> ListOfEffects;

inline std::pair<std::vector<std::string>, std::vector<int>> TrailMaterials;

inline std::vector<std::string> SkyBoxes
{
	"Default",
	"cx",
	"hav",
	"italy",
	"jungle",
	"office",
	"tides",
	"train_hdr",
	"assault",
	"de_cobble",
	"sky_dust_hdr",
	"sky_c17_05",
	"militia_hdr",
	"de_cobble_hdr",
	"de_piranesi",
	"sky_day02_02",
	"sky_day01_01",
	"sky_day01_04",
	"sky_day01_05",
	"sky_day01_06",
	"sky_day01_07",
	"sky_day01_08",
	"sky_day01_09",
	"sky_wasteland02"
};

inline std::vector<const char*>ClanTagInfo
{
	"ZE-Elite",
	"UKnownCheats",
	"Banned",
};

inline std::array<const char*, 12> MenuFont
{
	"seguisym",
	"Arial",
	"Bahnschrift",
	"Gabriola",
	"Georgia",
	"Tahoma",
	"Taile",
	"Micross",
	"Himalaya",
	"Impact",
	"Times",
	"Trado",
};


inline constexpr std::array BlockList
{
	"door",
	"gate",
	"wall", 
	"fence",
	"building",
	"glass", 
	"decal",
	"stair",
	"prop", 
	"wnd",
	"metal",
	"generic",
	"model", 
	"crate", 
	"plaster",
	"roof", 
	"water", 
	"ivy",
	"laundry",
	"shield"
};

inline std::vector<std::string> Killmessages
{
	"LOL, you died like a chicken",
	"$nick deserved to be killed"
};

inline std::vector<std::string> NightVisions
{
	"Default"
};

inline constexpr std::array KillEffects
{
	"Off",
	"FX_Tesla",
	"ManhackSparks",
	"BoltImpact",
	"Explosion",
	"GlassImpact",
	"GunshipImpact",
	"gunshotsplash",
	"HelicopterMegaBomb",
	"HunterDamage",
	"ParticleEffect",
	"Smoke",
	"waterripple",
	"watersplash",
	"WaterSurfaceExplosion",
};

inline std::vector<std::string> ScopeOverlays
{
	"Default"
};

inline constexpr std::array TracerSprites
{
	"zerogxplode",
	"steam1",
	"bubble",
	"laserbeam",
	"blueglow1",
	"purpleglow1",
	"purplelaser1",
	"white",
	"physbeam",
	"gunsmoke",
	"radio",
	"ledglow",
	"glow01"
};

#endif
