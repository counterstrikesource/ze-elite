#ifndef GlobalVars_H
#define GlobalVars_H

class CUserCmd;
class CBaseEntity;

namespace GlobalVars
{
	extern CUserCmd* cmd;

	extern CBaseEntity* pLocal;
};

#endif
