#pragma once

struct ColorRGBExp32
{
	byte r, g, b;
	signed char exponent;
};

struct dlight_t
{
	int flags;
	Vector origin;
	float radius;
	ColorRGBExp32 color;
	float die;
	float decay;
	float minlight;
	int key;
	int style;
};

class IVEfx
{
public:

	dlight_t* CL_AllocDlight(const int key)
	{
		using Type = dlight_t* (__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(this, 4)(this, key);
	}
};
