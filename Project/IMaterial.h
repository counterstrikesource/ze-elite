#pragma once

class IMaterialVar;
struct studiohwdata_t;
struct StudioDecalHandle_t;

#define TEXTURE_GROUP_WORLD "World textures"
#define TEXTURE_GROUP_SKYBOX "SkyBox textures"

class ITexture
{
public:

    int GetActualWidth()
    {
        using Type = int(__thiscall*)(void*);

        return GetVirtualMethod<Type>(this, 3)(this);
    }

    int GetActualHeight()
    {
        using Type = int(__thiscall*)(void*);

        return GetVirtualMethod<Type>(this, 4)(this);
    }
};

class IMatRenderContext
{
public:

    void SetRenderTarget(ITexture* pTexture)
    {
        using Type = void(__thiscall*)(void*, ITexture*);

        GetVirtualMethod<Type>(this, 6)(this, pTexture);
    }

    void ClearBuffers(bool bClearColor, bool bClearDepth, bool bClearStencil = false)
    {
        using Type = void(__thiscall*)(void*, bool, bool, bool);

        GetVirtualMethod<Type>(this, 12)(this, bClearColor, bClearDepth, bClearStencil);
    }

    void Viewport(int x, int y, int width, int height)
    {
        using Type = void(__thiscall*)(void*, int, int, int, int);

        GetVirtualMethod<Type>(this, 38)(this, x, y, width, height);
    }

    void GetViewport(int& x, int& y, int& width, int& height)
    {
        using Type = void(__thiscall*)(void*, int&, int&, int&, int&);

        GetVirtualMethod<Type>(this, 39)(this, x, y, width, height);
    }

    void ClearColor3ub(unsigned char r, unsigned char g, unsigned char b)
    {
        using Type = void(__thiscall*)(void*, unsigned char, unsigned char, unsigned char);

        GetVirtualMethod<Type>(this, 72)(this, r, g, b);
    }

    void OverrideDepthEnable(bool bEnable, bool bDepthEnable)
    {
        using Type = void(__thiscall*)(void*, bool, bool);

        GetVirtualMethod<Type>(this, 74)(this, bEnable, bDepthEnable);
    }

    void DrawScreenSpaceRectangle(IMaterial* A, int B, int C, int D, int E, float F, float G, float H, float I, int J, int K, void* L = NULL, int M = 1, int N = 1)
    {
        using Type = void(__thiscall*)(void*, IMaterial*, int, int, int, int, float, float, float, float, int, int, void*, int, int);

        GetVirtualMethod<Type>(this, 103)(this, A, B, C, D, E, F, G, H, I, J, K, L, M, N);
    }

    void PushRenderTargetAndViewport()
    {
        using Type = void(__thiscall*)(void*);

        GetVirtualMethod<Type>(this, 108)(this);
    }

    void PopRenderTargetAndViewport(void)
    {
        using Type = void(__thiscall*)(void*);

        GetVirtualMethod<Type>(this, 109)(this);
    }

    void SetStencilEnable(bool onoff)
    {
        using Type = void(__thiscall*)(void*, bool);

        GetVirtualMethod<Type>(this, 117)(this, onoff);
    }

    void SetStencilFailOperation(int op)
    {
        using Type = void(__thiscall*)(void*, int);

        GetVirtualMethod<Type>(this, 118)(this, op);
    }

    void SetStencilZFailOperation(int op)
    {
        using Type = void(__thiscall*)(void*, int);

        GetVirtualMethod<Type>(this, 119)(this, op);
    }

    void SetStencilPassOperation(int op)
    {
        using Type = void(__thiscall*)(void*, int);

        GetVirtualMethod<Type>(this, 120)(this, op);
    }

    void SetStencilCompareFunction(int cmpfn)
    {
        using Type = void(__thiscall*)(void*, int);

        GetVirtualMethod<Type>(this, 121)(this, cmpfn);
    }
    
    void SetStencilReferenceValue(int ref)
    {
        using Type = void(__thiscall*)(void*, int);

        GetVirtualMethod<Type>(this, 122)(this, ref);
    }

    void SetStencilTestMask(uintptr_t msk)
    {
        using Type = void(__thiscall*)(void*, uintptr_t);

        GetVirtualMethod<Type>(this, 123)(this, msk);
    }

    void SetStencilWriteMask(uintptr_t msk)
    {
        using Type = void(__thiscall*)(void*, uintptr_t);

        GetVirtualMethod<Type>(this, 124)(this, msk);
    }

    void BeginPIXEvent(unsigned long color, const char* szName)
    {
        using Type = void(__thiscall*)(void*, unsigned long, const char*);

        GetVirtualMethod<Type>(this, 140)(this, color, szName);
    }

    void EndPIXEvent() 
    {
        using Type = void(__thiscall*)(void*);

        GetVirtualMethod<Type>(this, 141)(this);
    }
};

class CModelRender
{
public:
	void ForcedMaterialOverride(IMaterial* mat, int type = 0)
	{
        using Type = void(__thiscall*)(PVOID, IMaterial*, int);

		return GetVirtualMethod<Type>(this, 1)(this, mat, type);
	}
};

class IMaterial
{
public:

	const char* GetName()
	{
        using Type = const char* (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 0)(this);
	}

	const char* GetTextureGroupName()
	{
        using Type = const char* (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 1)(this);
	}

	void IncrementReferenceCount()
	{
        using Type = void(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 12)(this);
	}

	void DecrementReferenceCount()
	{
        using Type = void(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 13)(this);
	}

	void AlphaModulate(float alpha)
	{
        using Type = void(__thiscall*)(void*, float);

		return GetVirtualMethod<Type>(this, 27)(this, alpha);
	}

	void ColorModulate(float r, float g, float b)
	{
        using Type = void(__thiscall*)(void*, float, float, float);

		return GetVirtualMethod<Type>(this, 28)(this, r, g, b);
	}

	void SetMaterialVarFlag(MaterialVarFlags flag, bool on)
	{
        using Type = void(__thiscall*)(void*, MaterialVarFlags, bool);

		return GetVirtualMethod<Type>(this, 29)(this, flag, on);
	}

	bool GetMaterialVarFlag(MaterialVarFlags flag)
	{
        using Type = bool(__thiscall*)(void*, MaterialVarFlags);

		return GetVirtualMethod<Type>(this, 30)(this, flag);
	}

    bool IsErrorMaterial()
    {
        using Type = bool(__thiscall*)(void*);

        return GetVirtualMethod<Type>(this, 42)(this);
    }

	const bool IsPrecached()
	{
        using Type = const bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 56)(this);
	}

    void Precache()
    {
        using Type = void(__thiscall*)(void*);

        return GetVirtualMethod<Type>(this, 62)(this);
    }

    IMaterialVar* FindVar(const char* varName, bool* found, bool complain = true)
    {
        using Type = IMaterialVar * (__thiscall*)(void*, const char*, bool*, bool);

        return GetVirtualMethod<Type>(this, 11)(this, varName, found, complain);
    }

public:

    inline void AddRef()
    {
        IncrementReferenceCount();
    }

    inline void Release()
    {
        DecrementReferenceCount();
    }
};

class IMaterialSystem
{
    using Handle = unsigned short;
public:

	IMaterial* FindMaterial
    (
        char const* pMaterialName,
        const char* pTextureGroupName,
        bool complain = true, const char* pComplainPrefix = NULL) {

        using Type = IMaterial * (__thiscall*)(void*, char const*, const char*, bool, const char*);

		return GetVirtualMethod<Type>(this, 71)(this, pMaterialName, pTextureGroupName, complain, pComplainPrefix);
	}

	Handle FirstMaterial()
	{
        using Type = Handle(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 73)(this);
	}

	Handle NextMaterial(Handle h)
	{
        using Type = Handle(__thiscall*)(void*, Handle);

		return GetVirtualMethod<Type>(this, 74)(this, h);
	}

	Handle InvalidMaterial()
	{
        using Type = Handle(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 75)(this);
	}

	IMaterial* GetMaterial(Handle h)
	{
        using Type = IMaterial * (__thiscall*)(void*, Handle);

		return GetVirtualMethod<Type>(this, 76)(this, h);
	}

    ITexture* FindTexture(char const* pTextureName, const char* pTextureGroupName, bool complain = true, int nAdditionalCreationFlags = 0)
    {
        using Type = ITexture * (__thiscall*)(void*, const char*, const char*, bool, int);

        return GetVirtualMethod<Type>(this, 79)(this, pTextureName, pTextureGroupName, complain, nAdditionalCreationFlags);
    }

    IMatRenderContext* GetRenderContext()
    {
        using Type = IMatRenderContext * (__thiscall*)(void*);

        return GetVirtualMethod<Type>(this, 98)(this);
    }
};

struct ModelRenderInfo_t
{
    Vector origin;
    Vector angles;
    PVOID* pRenderable;
    const model_t* pModel;
    const matrix3x4* pModelToWorld;
    const matrix3x4* pLightingOffset;
    const Vector* pLightingOrigin;
    int flags;
    int entity_index;
    int skin;
    int body;
    int hitboxset;
    int instance;
};