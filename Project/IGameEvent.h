#pragma once

class IGameEvent
{
public:

	const char* GetName()
	{
		using Type = const char* (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 1)(this);
	}

	int GetInt(const char* keyName = NULL, int defaultValue = 0)
	{
		using Type = int(__thiscall*)(void*, const char*, int);

		return GetVirtualMethod<Type>(this, 6)(this, keyName, defaultValue);
	}

	float GetFloat(const char* keyName = NULL, float defaultValue = 0.0f)
	{
		using Type = float(__thiscall*)(void*, const char*, int);

		return GetVirtualMethod<Type>(this, 7)(this, keyName, defaultValue);
	}

	const char* GetString(const char* keyName = NULL, const char* defaultValue = "")
	{
		using Type = const char* (__thiscall*)(void*, const char*, const char*);

		return GetVirtualMethod<Type>(this, 8)(this, keyName, defaultValue);
	}
};

class IGameEventListener2
{
public:
	virtual ~IGameEventListener2(void) {};

	virtual void FireGameEvent(IGameEvent* event) = 0;
};