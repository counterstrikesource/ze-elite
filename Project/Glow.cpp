#include "SDK.h"
#include "Glow.h"
#include "glow_outline_effect.h"

void Glow::RenderEffect() noexcept
{
	auto pLocal = CBaseEntity::GetLocalPlayer();

	if (!pLocal) return;

	LoopTroughPlayers()
	{
		CBaseEntity* pEntity = GetEntity(i);

		if (!pEntity || pEntity->IsDormant() || !pEntity->IsPlayer() || !pEntity->IsAlive())
		{
			g_GlowObjectManager.UnregisterGlowObject(i);

			continue;
		}

		bool IsEnemy = pEntity->IsEnemy(pLocal);

		if (Menu::Get.Visuals.Glow.Enabled[IsEnemy])
		{
			Color color = (Color&)Menu::Get.Colors.Glow.Player[IsEnemy * sizeof(Color)];

			if (pEntity != g_GlowObjectManager.m_GlowObjectDefinitions[i].pEntity) {

				g_GlowObjectManager.RegisterGlowObject(pEntity);
			}
		}
		else
		{
			g_GlowObjectManager.UnregisterGlowObject(i);
		}
	}
}

void Glow::ClearObjects() noexcept
{
	for (auto i = 0; i < g_GlowObjectManager.m_GlowObjectDefinitions.size(); i++)
	{
		g_GlowObjectManager.UnregisterGlowObject(i);
	}
}
