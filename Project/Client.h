#ifndef CHLClient_H
#define CHLClient_H

extern float _flHurtTime, Last_Logged_Time;

using Shutdown_Type = void(__thiscall*)(void*);
using CreateMove_Type = void(__stdcall*)(int, float, bool);
using FrameStageNotify_Type = void(__thiscall*)(void*, int);
using DispatchUserMessage_Type = bool(__thiscall*)(void*, int, void*&);

extern Shutdown_Type Original_Shutdown;
extern CreateMove_Type Original_CreateMove;
extern FrameStageNotify_Type Original_FrameStageNotify;
extern DispatchUserMessage_Type Original_DispatchUserMessage;

void __stdcall LevelShutdown(void*, void*);

void __stdcall FrameStageNotify(int curStage) noexcept;

bool __stdcall DispatchUserMessage(int messageType, void*& msg_data) noexcept;

void __stdcall Hooked_CreateMove(int sequence_number, float input_sample_frametime, bool active) noexcept;

void __stdcall CreateMove(int sequence_number, float input_sample_frametime, bool active, bool& bSendPacket) noexcept;

#endif
