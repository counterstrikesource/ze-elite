#ifndef GLOW_OUTLINE_EFFECT_H
#define GLOW_OUTLINE_EFFECT_H

#pragma once

class CBaseEntity;
class CViewSetup;
class CMatRenderContextPtr;


class CGlowObjectManager
{
public:
	CGlowObjectManager() {}

	void RegisterGlowObject(CBaseEntity* pEntity)
	{
		m_GlowObjectDefinitions[pEntity->GetIndex()].pEntity = pEntity;
	}

	void UnregisterGlowObject(int index)
	{
		m_GlowObjectDefinitions[index].pEntity = NULL;
	}

	void RenderGlowEffects(CViewSetup* pSetup, int nSplitScreenSlot);

	void RenderGlowModels(CBaseEntity* LocalPlayer, CViewSetup* pSetup, int nSplitScreenSlot, IMatRenderContext* pRenderContext);

	void ApplyEntityGlowEffects(CViewSetup* pSetup, int nSplitScreenSlot, IMatRenderContext* pRenderContext, int x, int y, int w, int h);

	struct GlowObjectDefinition_t
	{
		bool ShouldDraw(int nSlot) const
		{
			return pEntity && pEntity->ShouldDraw() && !pEntity->IsDormant();
		}

		void DrawModel();

		CBaseEntity* pEntity;
	};

	std::array<GlowObjectDefinition_t, 65> m_GlowObjectDefinitions;
};

extern CGlowObjectManager g_GlowObjectManager;

auto Assign_Glow_Textures() -> void;

#endif // GLOW_OUTLINE_EFFECT_H
