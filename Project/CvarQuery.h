#ifndef CvarQuery_H
#define CvarQuery_H

class DesiredConVarsValueInfo
{
public:
	int Status;

	char* Name;

	char* Value;

	DesiredConVarsValueInfo(const char* N, const char* V, int S) : Status(S)
	{
		Name = new char[strlen(N) + 1];
		Value = new char[strlen(V) + 1];

		strcpy(Name, N); 
		strcpy(Value, V);
	}

	void Update(const char* V)
	{
		delete Value;

		Value = new char[strlen(V) + 1];

		strcpy(Value, V);
	}
};

#endif
