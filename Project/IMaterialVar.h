#ifndef IMATERIALVAR_H
#define IMATERIALVAR_H

#pragma once

class IMaterialVar
{
public:

	void SetFloatValue(float val)
	{
		using Type = void(__thiscall*)(void*, float);

		GetVirtualMethod<Type>(this, 3)(this, val);
	}

};

#endif // IMATERIALVAR_H