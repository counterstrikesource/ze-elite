#pragma once

#include "SDK.h"
#include "include.h"
#include "Assorted.h"
#include "CNadeTracer.h"
#include "ImGui/imgui.h"
#include "ImGui/ImGui_internal.h"
#include "ImGui/imgui_impl_dx9.h"

#include "ClientEntityListener.h"
#include "SkinChanger.h"
#include "WorldTextures.h"

#define IM_FLOOR(_VAL) ((float)(int)(_VAL))    

HFont Esp::Others[5]{ 0,0,0,0,0 }, Esp::courier_new{ 0 }, Esp::Counter_Strike_Logo{ 0 };

bool Esp::Fonts_Were_Initialized{ false };

struct BoundingBox
{
	float x0, y0;

	float x1, y1;

	BoundingBox(CBaseEntity* Entity) noexcept
	{
		int width, height;

		Engine->GetScreenSize(width, height);

		x0 = static_cast<float>(width * 2);

		y0 = static_cast<float>(height * 2);

		x1 = -x0;

		y1 = -y0;

		Vector mins = Entity->GetMins(), maxs = Entity->GetMaxs();

		for (int i = 0; i < 8; ++i)
		{
			const Vector point{ i & 1 ? maxs.x : mins.x,
								i & 2 ? maxs.y : mins.y,
								i & 4 ? maxs.z : mins.z };

			Vector Screen;

			if (!WorldToScreen(point.Transform(Entity->GetRgflCoordinateFrame()), Screen))
			{
				valid = false;

				return;
			}

			x0 = min(x0, Screen.x);
			y0 = min(y0, Screen.y);
			x1 = max(x1, Screen.x);
			y1 = max(y1, Screen.y);
		}

		valid = true;
	}

	operator bool() noexcept
	{
		return valid;
	}
private:
	bool valid;
};

static const char* WeaponIDToAlias(int id) noexcept
{
	static auto WeaponIDToAlias = reinterpret_cast<const char* (*)(int)>(
		Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 50 FF 75 94")));

	return WeaponIDToAlias(id);
}

static const wchar_t* GetWC(const char* c) noexcept
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

static void RenderText(int x, int y, Color color, HFont font, const wchar_t* text) noexcept
{
	Surface->DrawSetTextPos(x, y);
	Surface->DrawSetTextFont(font);
	Surface->DrawSetTextColor(color);
	Surface->DrawPrintText(text, wcslen(text));
}


bool ScreenTransform(const Vector& point, Vector& screen) noexcept
{
	const matrix3x4& w2sMatrix = Engine->WorldToScreenMatrix();
	screen.x = w2sMatrix[0][0] * point.x + w2sMatrix[0][1] * point.y + w2sMatrix[0][2] * point.z + w2sMatrix[0][3];
	screen.y = w2sMatrix[1][0] * point.x + w2sMatrix[1][1] * point.y + w2sMatrix[1][2] * point.z + w2sMatrix[1][3];
	screen.z = 0.0f;

	float w = w2sMatrix[3][0] * point.x + w2sMatrix[3][1] * point.y + w2sMatrix[3][2] * point.z + w2sMatrix[3][3];

	if (w < 0.001f)
	{
		screen.x *= 100000;
		screen.y *= 100000;

		return true; (0xEE8 - 0xd8d);
	}

	float invw = 1.0f / w;
	screen.x *= invw;
	screen.y *= invw;

	return false;
}


bool WorldToScreen(const Vector& origin, Vector& screen) noexcept
{
	if (ScreenTransform(origin, screen)) return false;
	{
		int iScreenWidth, iScreenHeight;

		Engine->GetScreenSize(iScreenWidth, iScreenHeight);

		screen.x = (iScreenWidth / 2.0f) + (screen.x * iScreenWidth) / 2;

		screen.y = (iScreenHeight / 2.0f) - (screen.y * iScreenHeight) / 2;

		return true;
	}
}

void Esp::Initialize() noexcept
{
	Counter_Strike_Logo = Surface->CreateFont();

	courier_new = Surface->CreateFont();

	for (auto& Font : Others) Font = Surface->CreateFont();

	Surface->SetFontGlyphSet(Others[0], "Tahoma", 14, 0, 0, 0, FONTFLAG_ANTIALIAS);

	Surface->SetFontGlyphSet(Others[1], "Tahoma", 25, 15, 1, 0, FONTFLAG_DROPSHADOW | FONTFLAG_ANTIALIAS);

	Surface->SetFontGlyphSet(Others[2], "Arial", 16, 0, 0, 0, FONTFLAG_ANTIALIAS);

	Surface->SetFontGlyphSet(Others[3], "Verdana", 12, 0, 0, 0, FONTFLAG_OUTLINE);

	Surface->SetFontGlyphSet(courier_new, "Courier New", 40, 20, 1, 0, FONTFLAG_ANTIALIAS | FONTFLAG_OUTLINE);

	Surface->SetFontGlyphSet(Counter_Strike_Logo, "Counter-Strike", 40, 0, 0, 0, FONTFLAG_ANTIALIAS);

	Fonts_Were_Initialized = true;
}

void RenderCrosshair(CBaseEntity* pLocal)
{
	static int Width, Height;

	constexpr auto& Crosshair = Menu::Get.Visuals.Crosshair;

	constexpr auto& Circle_Shape = Crosshair.Circle_Shape;

	constexpr auto& Plus_Shape = Crosshair.Plus_Shape;

	constexpr auto& X_Shape = Crosshair.X_Shape;

	Engine->GetScreenSize(Width, Height); int screenWide = Width / 2; int screenTall = Height / 2;

	constexpr auto& CrosshairColor = Menu::Get.Colors.General.Crosshair;

	if (Crosshair.Recoil) {

		Vector PunchAngle = pLocal->GetPunchAngle();

		screenWide = (Width / 2) - ((Width / 90) * PunchAngle.y);

		screenTall = (Height / 2) + ((Height / 90) * PunchAngle.x);
	}

	if (Plus_Shape.Enabled)
	{
		constexpr int& gap = Plus_Shape.Gap;

		constexpr int& length = Plus_Shape.Length;

		Surface->DrawSetColor(CrosshairColor.Plus_Shape);

		Surface->DrawLine(screenWide - (length + gap), screenTall, screenWide - gap, screenTall);

		Surface->DrawLine(screenWide + (length + gap), screenTall, screenWide + gap, screenTall);

		Surface->DrawLine(screenWide, screenTall - (length + gap), screenWide, screenTall - gap);

		Surface->DrawLine(screenWide, screenTall + (length + gap), screenWide, screenTall + gap);
	}

	if (X_Shape.Enabled)
	{
		constexpr int& gap = X_Shape.Gap;

		constexpr int& length = X_Shape.Length;

		Surface->DrawSetColor(CrosshairColor.X_Shape);

		Surface->DrawLine(screenWide - (length + gap), screenTall - (length + gap), screenWide - gap, screenTall - gap);

		Surface->DrawLine(screenWide - (length + gap), screenTall + (length + gap), screenWide - gap, screenTall + gap);

		Surface->DrawLine(screenWide + (length + gap), screenTall + (length + gap), screenWide + gap, screenTall + gap);

		Surface->DrawLine(screenWide + (length + gap), screenTall - (length + gap), screenWide + gap, screenTall - gap);
	}

	if (Circle_Shape.Enabled)
	{
		Surface->DrawSetColor(CrosshairColor.Circle_Shape);

		Surface->DrawOutlinedCircle(screenWide, screenTall, Circle_Shape.Radius, Circle_Shape.Segments);
	}


#undef DrawLine
}

struct DynamicBoundingBox
{
	float x0, y0;

	float x1, y1;

	DynamicBoundingBox(CBaseEntity* Entity) noexcept
	{
		valid = false;

		int width, height;

		Engine->GetScreenSize(width, height);

		x0 = static_cast<float>(width * 2);

		y0 = static_cast<float>(height * 2);

		x1 = -x0;

		y1 = -y0;

		const auto model = Entity->GetModel();
		if (!model) return;

		auto pStudioHdr = ModelInfo->GetStudiomodel(model);
		if (!pStudioHdr)
			return;

		auto pHitboxSet = pStudioHdr->GetHitboxSet(Entity->GetHitboxSet());
		if (!pHitboxSet)
			return;

		matrix3x4 matrix[128];

		if (!Entity->SetupBones(matrix, 128, 0x00000100, Globals->curtime))
			return;

		for (auto i = pHitboxSet->numhitboxes; i--; ) {

			if (auto HitBox = pHitboxSet->GetHitbox(i); HitBox)
			{
				auto maxs = HitBox->bbmax;

				auto mins = HitBox->bbmin;

				for (int i = 0; i < 8; ++i)
				{
					const Vector point{ i & 1 ? maxs.x : mins.x,
										i & 2 ? maxs.y : mins.y,
										i & 4 ? maxs.z : mins.z };

					Vector Screen;

					if (!WorldToScreen(point.Transform(matrix[HitBox->bone]), Screen)) return;

					x0 = min(x0, Screen.x);
					x1 = max(x1, Screen.x);

					y0 = min(y0, Screen.y);
					y1 = max(y1, Screen.y);
				}
			}
		}

		valid = true;
	}

	operator bool() noexcept
	{
		return valid;
	}
private:
	bool valid;
};

auto ProcessGrenadesTracer(CBaseEntity* Entity) -> void
{
	if (Menu::Get.Colors.General.NadeTracer[Color::Elements::Alpha]) {

		if (Entity->GetVelocity().Length() > 1.f) {

			if (strncmp(Entity->GetDebugName(), "grenade", 8) == 0) {

				if (Entity->GetThrower() == Engine->GetLocalPlayer()) {

					pNadeTracer.AddTracer(Entity, 60);

					pNadeTracer.Draw();
				}
			}
		}
	}
}

auto Render_Dropped_Weapons(CBaseEntity* Entity) -> void
{
	char Buffer[64];

	Vector ScreenPos;

	int width, height;

	if (Entity->IsDormant())
		return;

	auto pModel = Entity->GetModel();
	if (!pModel)
		return;

	const char* Modelname = pModel->GetName();
	if (!Modelname)
		return;

	if (strstr(Modelname, "weapon") == 0 && strstr(Modelname, "Weapon") == 0)
		return;

	if (Entity->GetOwner() != 255)
		return;

	DynamicBoundingBox DynamicBBox{ Entity };
	if (!DynamicBBox)
		return;

	auto Name = [&]() {

		if (Menu::Get.Visuals.Name[DroppedWpns] == 0)
			return;

		using CWeaopon = CBaseCombatWeapon*;

		CWeaopon pWeapon = CWeaopon(Entity);

		snprintf(Buffer, sizeof Buffer, "%s", WeaponIDToAlias(pWeapon->GetWeaponID()));

		Buffer[0] = std::toupper(Buffer[0]);

		const wchar_t* Name = GetWC(Buffer);

		auto Origin = pWeapon->GetAbsOrigin(); Origin.z -= 10.f;

		Surface->GetTextSize(Esp::Others[Menu::Get.Visuals.NameFont[DroppedWpns]], Name, width, height);

		if (WorldToScreen(Origin, ScreenPos))
			RenderText(ScreenPos.x - width / 2, ScreenPos.y,
				(Color&)Menu::Get.Colors.PlayerEsp.Name[DroppedWpns * sizeof(Color)], Esp::Others[Menu::Get.Visuals.NameFont[DroppedWpns]], Name);
	};

	Name();
	Esp::Box(DynamicBBox, DroppedWpns);
	Esp::SnapLine(Entity, DroppedWpns);
}

void Esp::Render() noexcept
{
	auto pLocal{ CBaseEntity::GetLocalPlayer() };
	if (!pLocal || !pLocal->IsAlive())
		return;

	Hit_Marker_On_Paint();

	RenderCrosshair(pLocal);

	DamageIndicator::Paint(pLocal);

	constexpr auto& DLights = Menu::Get.Visuals.DLights;

	constexpr auto& DLightsColor = Menu::Get.Colors.PlayerEsp.DLights;

	if (DLights == 1 || DLights == 4)
	{
		const unsigned short INDEX(Engine->GetLocalPlayer());

		dlight_t* dLight = Effects->CL_AllocDlight(INDEX);
		dLight->die = Globals->curtime + 0.05f;
		dLight->radius = 200.f;

		dLight->color.r = DLightsColor[0];
		dLight->color.g = DLightsColor[1];

		dLight->color.b = DLightsColor[2];

		dLight->color.exponent = 5;
		dLight->key = INDEX;
		dLight->decay = dLight->radius / 5.0f;
		dLight->origin = pLocal->GetAbsOrigin() + Vector(0, 0, 2);
	}

	LoopTroughEntities()
	{
		if (auto Entity = GetEntity(i); Entity && Entity != pLocal) {

			Debug(pLocal, Entity);

			ProcessGrenadesTracer(Entity);

			Render_Dropped_Weapons(Entity);

			if (Entity->IsPlayer() && Entity->IsAlive() && !Entity->IsDormant()) {

				const auto IsEnemy(Entity->IsEnemy(pLocal));

				const auto& AbsOrigin = Entity->GetAbsOrigin();

				if (DLights > 1)
				{
					if ((IsEnemy && DLights == 2) || (!IsEnemy && DLights == 3) || DLights == 4)
					{
						dlight_t* dLight = Effects->CL_AllocDlight(i);
						dLight->die = Globals->curtime + 0.05f;
						dLight->radius = 200.f;

						dLight->color.r = DLightsColor[0];
						dLight->color.g = DLightsColor[1];
						dLight->color.b = DLightsColor[2];

						dLight->color.exponent = 5;
						dLight->key = i;
						dLight->decay = dLight->radius / 5.0f;
						dLight->origin = AbsOrigin + Vector(0, 0, 2);
					}
				}


				DynamicBoundingBox DynamicBBox{ Entity };

				BoundingBox BBox{ Entity };

				if (!BBox)
					continue;

				Box(DynamicBBox, IsEnemy);
				Skeleton(Entity, IsEnemy);
				Name(BBox, Entity, IsEnemy);
				Health(BBox, Entity, IsEnemy);
				Weapon(AbsOrigin, Entity, IsEnemy);
				SnapLine(Entity, IsEnemy);
			}
		}
	}

	pNadeTracer.Clear();
}

void Esp::Box(DynamicBoundingBox bbox, int Category) noexcept
{
	if (Menu::Get.Visuals.Box[Category])
	{
		Surface->DrawSetColor((Color&)Menu::Get.Colors.PlayerEsp.Box[Category * sizeof(Color)]);

		switch (Menu::Get.Visuals.BoxType[Category])
		{
		case 0:
			Surface->DrawOutlinedRect(bbox.x0, bbox.y0, bbox.x1, bbox.y1);
			break;
		case 1:

			Surface->DrawLine(bbox.x0, bbox.y0, bbox.x0, IM_FLOOR(bbox.y0 * 0.75f + bbox.y1 * 0.25f));

			Surface->DrawLine(bbox.x0, bbox.y0, IM_FLOOR(bbox.x0 * 0.75f + bbox.x1 * 0.25f), bbox.y0);

			Surface->DrawLine(bbox.x1, bbox.y0, IM_FLOOR(bbox.x1 * 0.75f + bbox.x0 * 0.25f), bbox.y0);

			Surface->DrawLine(bbox.x1 - 1.0f, bbox.y0, bbox.x1 - 1.0f, IM_FLOOR(bbox.y0 * 0.75f + bbox.y1 * 0.25f));

			Surface->DrawLine(bbox.x0, bbox.y1, bbox.x0, IM_FLOOR(bbox.y1 * 0.75f + bbox.y0 * 0.25f));

			Surface->DrawLine(bbox.x0, bbox.y1 - 1.0f, IM_FLOOR(bbox.x0 * 0.75f + bbox.x1 * 0.25f), bbox.y1 - 1.0f);

			Surface->DrawLine(bbox.x1 - 0.5f, bbox.y1 - 1.0f, IM_FLOOR(bbox.x1 * 0.75f + bbox.x0 * 0.25f), bbox.y1 - 1.0f);

			Surface->DrawLine(bbox.x1 - 1.0f, bbox.y1, bbox.x1 - 1.0f, IM_FLOOR(bbox.y1 * 0.75f + bbox.y0 * 0.25f));
		}
	}
}

void Esp::Health(BoundingBox bbox, CBaseEntity* Entity, bool IsEnemy) noexcept
{
	if (Menu::Get.Visuals.Health[IsEnemy])
	{
		Surface->DrawSetColor((Color&)Menu::Get.Colors.PlayerEsp.Health[IsEnemy * sizeof(Color)]);

		if (Menu::Get.Visuals.HealthType[IsEnemy] == 0)
		{
			Vector ScreenPos;

			static int width, height;

			auto Str = std::to_wstring(Entity->GetHealth()) + L" HP";

			Surface->GetTextSize(Others[Menu::Get.Visuals.HealthFont[IsEnemy]], Str.c_str(), width, height);

			auto Origin = Entity->GetAbsOrigin(); Origin.z += Entity->GetMaxs().z + 50.f;

			if (WorldToScreen(Origin, ScreenPos))
				RenderText(ScreenPos.x - width / 2, ScreenPos.y,
					(Color&)Menu::Get.Colors.PlayerEsp.Health[IsEnemy * sizeof(Color)], Others[Menu::Get.Visuals.HealthFont[IsEnemy]], Str.c_str());
		}
		else if (Menu::Get.Visuals.HealthType[IsEnemy] == 1)
		{
			if (auto EntityIndex = Entity->GetIndex(); EntityIndex < 65)
			{
				auto MaxHealth = HP_Bar::Entities[EntityIndex].second;

				MaxHealth = max(MaxHealth, Entity->GetMaxHealth());

				auto  hp = Entity->GetHealth();

				float box_h = (float)fabs(bbox.y0 - bbox.y1);

				constexpr float off = 8;

				int height = (box_h * hp) / MaxHealth;

				int x = bbox.x0 - off;

				int y = bbox.y0;

				constexpr int w = 4;

				int h = box_h;

				Surface->DrawSetColor(Color::Black());

				Surface->DrawFilledRect(x, y, x + w, y + h);

				Surface->DrawSetColor((Color&)Menu::Get.Colors.PlayerEsp.Health[IsEnemy * sizeof(Color)]);

				Surface->DrawFilledRect(x + 1, bbox.y1 - (height + 2), x + w - 1, bbox.y1 - 2);
			}
		}
	}
}

class CHudTexture
{
public:
	DECLARE_OFFSET_FUNCTION(GetCharacterInFont, 134, char);
};

void Esp::Weapon(const Vector& EntityOrigin, CBaseEntity* Entity, bool IsEnemy) noexcept
{
	if (Menu::Get.Visuals.Weapon[IsEnemy])
	{
		if (const auto ActiveWeapon{ Entity->GetActiveWeapon() }; ActiveWeapon)
		{
			auto Origin = EntityOrigin; Origin.z -= 10.f;

			Vector ScreenPos;

			int width, height;

			char Buffer[64]{ 0 };

			if (!Menu::Get.Visuals.WeaponRenderingMode)
			{
				snprintf(Buffer, sizeof Buffer, "%s", WeaponIDToAlias(ActiveWeapon->GetWeaponID()));

				Buffer[0] = std::toupper(Buffer[0]);

				Surface->GetTextSize(Others[Menu::Get.Visuals.WeaponFont[IsEnemy]], GetWC(Buffer), width, height);

				if (WorldToScreen(Origin, ScreenPos))
					RenderText(ScreenPos.x - width / 2, ScreenPos.y,
						(Color&)Menu::Get.Colors.PlayerEsp.Weapon[IsEnemy * sizeof(Color)], Others[Menu::Get.Visuals.WeaponFont[IsEnemy]], GetWC(Buffer));
			}
			else
			{
				if (auto WeaponData = ActiveWeapon->GetWpnData(); WeaponData)
				{
					if (auto WpnIcon = WeaponData->GetIconActive(); WpnIcon)
					{
						snprintf(Buffer, sizeof Buffer, "%c", WpnIcon->GetCharacterInFont());

						Surface->GetTextSize(Counter_Strike_Logo, GetWC(Buffer), width, height);

						if (WorldToScreen(Origin, ScreenPos))
							RenderText(ScreenPos.x - width / 2, ScreenPos.y,
								(Color&)Menu::Get.Colors.PlayerEsp.Weapon[IsEnemy * sizeof(Color)], Counter_Strike_Logo, GetWC(Buffer));
					}
				}
			}
		}
	}
}

void Esp::SnapLine(CBaseEntity* Entity, int Category) noexcept
{
	if (Menu::Get.Visuals.SnapLine[Category])
	{
		Vector ScreenPos;

		int width, height;

		if (!WorldToScreen(Entity->GetAbsOrigin(), ScreenPos))
			return;

		Engine->GetScreenSize(width, height);

		Surface->DrawSetColor((Color&)Menu::Get.Colors.PlayerEsp.SnapLine[Category * sizeof(Color)]);

		Surface->DrawLine(width / 2, height, static_cast<int>(ScreenPos.x), static_cast<int>(ScreenPos.y));
	}
}

void Esp::Name(BoundingBox bbox, CBaseEntity* Entity, int Category) noexcept
{
	int width, height;

	if (Menu::Get.Visuals.Name[Category])
	{
		if (wchar_t name[128]; MultiByteToWideChar(CP_UTF8, 0, Entity->GetName(), -1, name, 128))
		{
			Surface->GetTextSize(Others[Menu::Get.Visuals.NameFont[Category]], name, width, height);

			RenderText(bbox.x0 - width * 2, bbox.y0 - 5 - height, (Color&)Menu::Get.Colors.PlayerEsp.Name[Category * sizeof(Color)], Others[Menu::Get.Visuals.NameFont[Category]], name);
		}
	}
}

void Esp::Skeleton(CBaseEntity* pEntity, bool IsEnemy) noexcept
{
	if (!Menu::Get.Visuals.Skeleton[IsEnemy])
		return;

	studiohdr_t* pStudioHdr = ModelInfo->GetStudiomodel(pEntity->GetModel());

	if (!pStudioHdr)
		return;

	Vector vParent, vChild, sParent, sChild;

	for (int j = 0; j < pStudioHdr->numbones; j++)
	{
		mstudiobone_t* pBone = pStudioHdr->GetBone(j);

		if (pBone && (pBone->flags & 0x100) && (pBone->parent != -1))
		{
			vChild = pEntity->GetBonePos(j);

			vParent = pEntity->GetBonePos(pBone->parent);

			if (WorldToScreen(vParent, sParent) && WorldToScreen(vChild, sChild))
			{
				Surface->DrawSetColor((Color&)Menu::Get.Colors.PlayerEsp.Skeleton[IsEnemy * sizeof(Color)]);

				Surface->DrawLine(sParent[0], sParent[1], sChild[0], sChild[1]);
			}
		}
	}
}

void Esp::Debug(CBaseEntity* pLocal, CBaseEntity* Entity) noexcept
{
	Vector vScreenPos;

	char Buffer[64]{ NULL };

	bool Can_Complete = Menu::Get.Visuals.Debug.INDEX
		|| Menu::Get.Visuals.Debug.Classname
		|| Menu::Get.Visuals.Debug.ModelName
		|| Menu::Get.Visuals.Debug.ModelIndex
		|| Menu::Get.Visuals.Debug.Velocity
		|| Menu::Get.Visuals.Debug.Origin;

	if (Can_Complete)
	{
		if (Menu::Get.Visuals.Debug.Filter[0] && Entity->IsPlayer())
			return;

		auto pModel = Entity->GetModel();
		if (!pModel)
			return;

		auto ModelName = pModel->GetName();

		if (Menu::Get.Visuals.Debug.Filter[2] && strstr(ModelName, "sprite"))
			return;

		if (Menu::Get.Visuals.Debug.Filter[3] && !Entity->EstimateAbsVelocity().Length())
			return;

		if (Menu::Get.Visuals.Debug.Filter[1] && (strstr(ModelName, "weapon") || strstr(ModelName, "Weapon")))
			return;

		Vector vWorldPos{ Entity->WorldSpaceCenter() };

		if (pLocal->GetAbsOrigin().DistTo(Entity->GetAbsOrigin()) <= Menu::Get.Visuals.Debug.MaxDistance)
		{
			if (WorldToScreen(vWorldPos, vScreenPos))
			{
				int8_t Value{ 0 };

				if (Menu::Get.Visuals.Debug.INDEX)
				{
					snprintf(Buffer, sizeof Buffer, "Index: %i", Entity->GetIndex());

					RenderText(vScreenPos.x, vScreenPos.y, Color(255, 255, 255, 255), Esp::Others[2], GetWC(Buffer));

					Value += 15;
				}

				if (Menu::Get.Visuals.Debug.Classname)
				{
					snprintf(Buffer, sizeof Buffer, "Classname: %s", Entity->GetClassname());

					RenderText(vScreenPos.x, vScreenPos.y + Value, Color(255, 255, 255, 255), Esp::Others[2], GetWC(Buffer));

					Value += 15;
				}

				if (Menu::Get.Visuals.Debug.ModelName)
				{
					auto pModel = Entity->GetModel();

					snprintf(Buffer, sizeof Buffer, "Modelname: %s", pModel ? pModel->GetName() : "Unknown");

					RenderText(vScreenPos.x, vScreenPos.y + Value, Color(255, 255, 255, 255), Esp::Others[2], GetWC(Buffer));

					Value += 15;
				}

				if (Menu::Get.Visuals.Debug.ModelIndex)
				{
					snprintf(Buffer, sizeof Buffer, "Model Index: %i", Entity->GetModelIndex());

					RenderText(vScreenPos.x, vScreenPos.y + Value, Color(255, 255, 255, 255), Esp::Others[2], GetWC(Buffer));

					Value += 15;
				}

				if (Menu::Get.Visuals.Debug.Origin)
				{
					snprintf(Buffer, sizeof Buffer, "Origin: %.2f %.2f %.2f", Entity->GetAbsOrigin().x, Entity->GetAbsOrigin().y, Entity->GetAbsOrigin().z);

					RenderText(vScreenPos.x, vScreenPos.y + Value, Color(255, 255, 255, 255), Esp::Others[2], GetWC(Buffer));

					Value += 15;
				}

				if (Menu::Get.Visuals.Debug.Velocity)
				{
					snprintf(Buffer, sizeof Buffer, "Velocity: %i", static_cast<int>(Entity->EstimateAbsVelocity().Length()));

					RenderText(vScreenPos.x, vScreenPos.y + Value, Color(255, 255, 255, 255), Esp::Others[2], GetWC(Buffer));
				}
			}
		}
	}
}

CBaseEntity* RainEntity = nullptr;

void* RainNetworkable = nullptr;

void Precipitation() {

	static ClientClass* Precipitation_ClientClass = nullptr;

	static auto& g_Precipitations_Count = **(int**)(Tools::FindPattern("client.dll", "39 05 ? ? ? ? 0F 8E ? ? ? ? 53") + 2);

	if (!Menu::Get.Visuals.Weather.Enabled || ClientState->GetDeltaTick() == -1 || g_Precipitations_Count > 1)
	{
		if (RainEntity)
		{
			if (auto Entity = EntityList->GetClientEntity(MAX_EDICTS - 1); Entity)
				Entity->Remove();

			RainNetworkable = RainEntity = nullptr;
		}

		return;
	}

	if (!Precipitation_ClientClass)
		for (auto pClass = BaseClientDLL->GetAllClasses(); pClass && !Precipitation_ClientClass; pClass = pClass->m_pNext)
			if (pClass->m_ClassID == 87)
				Precipitation_ClientClass = pClass;


	if (!EntityList->GetClientEntity(MAX_EDICTS - 1) && Precipitation_ClientClass && Precipitation_ClientClass->m_pCreateFn) {

		RainNetworkable = Precipitation_ClientClass->m_pCreateFn(MAX_EDICTS - 1, 0);

		if (RainNetworkable) {

			RainEntity = EntityList->GetClientEntity(MAX_EDICTS - 1);

			RainEntity->GetPrecipType() = Menu::Get.Visuals.Weather.Type;

			RainEntity->PreDataUpdate(0);

			RainEntity->OnPreDataChanged(0);

			RainEntity->GetMins() = Vector(-MAX_POSITION_FLT, -MAX_POSITION_FLT, -MAX_POSITION_FLT);

			RainEntity->GetMaxs() = Vector(MAX_POSITION_FLT, MAX_POSITION_FLT, MAX_POSITION_FLT);

			RainEntity->OnDataChanged(0);

			RainEntity->PostDataUpdate(0);
		}
	}
}

DamageIndicator_t DamageIndicator::data;

void DamageIndicator::Paint(CBaseEntity* pLocal)
{
	if (!Menu::Get.Visuals.DamageIndicator.Enabled)
		return;

	float current_time = pLocal->GetTickBase() * Globals->interval_per_tick;

	if (data.flEraseTime < current_time) {
		return;
	}

	int Width, Height;

	Engine->GetScreenSize(Width, Height); Width /= 2; Height /= 2;

	auto Text = GetWC((std::string("- ") + std::to_string(data.iDamage) + std::string(" HP")).c_str());

	RenderText(Width - 40, Height - 155, Menu::Get.Colors.General.DamageIndicator, Esp::Others[1], Text);
}