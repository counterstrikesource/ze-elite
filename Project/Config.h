#ifndef Config_H
#define Config_H

namespace Config
{
	void SaveConfig();

	void LoadConfig();
}

#endif
