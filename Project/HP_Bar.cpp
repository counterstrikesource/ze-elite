#include <array>
#include "SDK.h"
#include "Assorted.h"

#include "HP_Bar.h"

std::array<std::pair<CSprite*, int>, 65> HP_Bar::Entities;

bool HP_Bar::IsLocalEntity(CSprite* pEntity) noexcept
{
	for (auto pEntity1 : Entities)
	{
		if (pEntity1.first == pEntity)
		{
			return true;
		}
	}

	return false;
}

void HP_Bar::UpdateSpritesScale() noexcept
{
	if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal)
	{
		LoopTroughPlayers()
		{
			auto Entity = GetEntity(i);
			auto Sprite = Entities[i].first;

			if (Entity && Sprite)
				Sprite->GetSpriteScale() = Menu::Get.Visuals.HP_SpriteScale[Entity->IsEnemy(pLocal)];
		}
	}
}

void HP_Bar::ClearSprites(bool Remove) noexcept
{
	for (auto& [Entity, Temp] : Entities) {

		if (Entity)
		{
			if (Remove)
				Entity->Remove();

			Entity = nullptr;
		}
	}
}

void HP_Bar::UpdateSpriteModelIndex(std::pair<CSprite*, int> Sprite, int Health) noexcept
{
	auto& [pEntity, MaxHealth] = Sprite;

	if (!pEntity) return;

	if (Health < 1)
	{
		pEntity->Remove();

		pEntity = nullptr;

		return;
	}

	float Total{ std::round((Health / MaxHealth) * 100.f) };

	int modelIndex = 6;

	if (Total >= 100) {

		modelIndex = 1;

	}
	else if (Total >= 80) {

		modelIndex = 2;
	}
	else if (Total >= 60) {

		modelIndex = 3;
	}
	else if (Total >= 40) {

		modelIndex = 4;
	}
	else if (Total > 20) {

		modelIndex = 5;
	}

	std::string ModelPath("sprites/franug/hp_bar_2/hp_bar");

	ModelPath.append(std::to_string(modelIndex));

	ModelPath.append(".vmt");

	auto ModelIndex = ModelInfo->GetModelIndex(ModelPath.c_str());

	if (ModelIndex == -1)
	{
		if (PrecacheModel(ModelPath.c_str()))
		{
			ModelIndex = ModelInfo->GetModelIndex(ModelPath.c_str());
		}
	}

	assert(ModelIndex != -1);

	if (ModelIndex != -1 && ModelIndex != pEntity->GetModelIndex())
	{
		pEntity->SetModelByIndex(ModelIndex);
	}
}

void HP_Bar::CreateSpriteForEntity(CBaseEntity* pEntity, bool IsEnemy) noexcept
{
	auto Index = pEntity->GetIndex();

	auto& pSprite = Entities[Index];

	using Type = CSprite*(__cdecl*)(const char*);

	if (auto Entity = reinterpret_cast<Type>(CreateEntityByName)("env_sprite"); Entity)
	{
		pSprite.first = Entity;

		Entity->FollowEntity(pEntity, 1);

		Entity->PreDataUpdate(0);

		Entity->OnPreDataChanged(0);

		Entity->GetSpriteScale() = Menu::Get.Visuals.HP_SpriteScale[IsEnemy];

		Entity->GetRenderColor() = Color(255, 255, 255);

		Entity->GetRenderMode() = kRenderTransColor;

		Entity->OnDataChanged(0);

		UpdateSpriteModelIndex(pSprite, pEntity->GetHealth());
	}
}

void HP_Bar::Process(CBaseEntity* pLocal) noexcept
{
	for (auto i = 0; i < Entities.size(); i++)
	{
		auto& [pEntity, MaxHealth] = Entities[i];

		if (auto Entity1 = GetEntity(i); Entity1 && !Entity1->IsDormant() && Entity1->IsPlayer() && Entity1->IsAlive() && Entity1 != pLocal) {

			bool IsEnemy = Entity1->IsEnemy(pLocal);

			if (Menu::Get.Visuals.Health[IsEnemy] && Menu::Get.Visuals.HealthType[IsEnemy] == 2)
			{
				if (pEntity == nullptr)
					CreateSpriteForEntity(Entity1, IsEnemy);

				continue;
			}
		}

		if (MaxHealth != 100)
			MaxHealth = 100;

		if (pEntity)
		{
			pEntity->Remove();
			pEntity = nullptr;
		}
	}
}
