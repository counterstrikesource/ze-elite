#pragma once

class IVDebugOverlay
{
public:

	void AddBoxOverlay(const Vector& origin, const Vector& mins, const Vector& max, QAngle const& orientation, int r, int g, int b, int a, float duration)
	{
		using Type = void(__thiscall*)(void*, const Vector&, const Vector&, const Vector&, QAngle const&, int, int, int, int, float);

		GetVirtualMethod<Type>(this, 1)(this, origin, mins, max, orientation, r, g, b, a, duration);
	}
};
