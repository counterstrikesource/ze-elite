#pragma once

class ConCommandBase;
class ConCommand;
class ConVar;
class Color;

class ICvar
{
public:

	ConVar* FindVar(const char* Name)
	{
		using Type = ConVar * (__thiscall*)(void*, const char*);

		return GetVirtualMethod<Type>(this, 13)(this, Name);
	}

	ConCommand* FindCommand(const char* Name)
	{
		using Type = ConCommand * (__thiscall*)(void*, const char*);

		return GetVirtualMethod<Type>(this, 15)(this, Name);
	}

	ConCommandBase* GetCommands(void)
	{
		using Type = ConCommandBase*(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 17)(this);
	}
};