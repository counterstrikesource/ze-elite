#pragma once

class IInputSystem
{
public:

	void EnableInput(bool bEnable)
	{
		using Type = void(__thiscall*)(void*, bool);

		return GetVirtualMethod<Type>(this, 7)(this, bEnable);
	}

	bool IsButtonDown(int code)
	{
		using Type = bool(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(this, 11)(this, code);
	}

	const char* ButtonCodeToString(int code)
	{
		using Type = const char* (__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(this, 27)(this, code);
	}

	int VirtualKeyToButtonCode(int nVirtualKey)
	{
		if (nVirtualKey <= VK_XBUTTON2) {
			if (nVirtualKey > VK_CANCEL) nVirtualKey--;
			return nVirtualKey + 106;
		}

		using Type = int(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(this, 32)(this, nVirtualKey);
	}

	auto VirtualKeyToString(int virtualKey) noexcept
	{
		return ButtonCodeToString(VirtualKeyToButtonCode(virtualKey));
	}
};