#ifndef VoiceChat_H
#define VoiceChat_H

class AudioVoice
{
public:
	char* Soundname;

	bool KeyWasPressed;

	float SoundDuration;

	unsigned short KeyBind;

	AudioVoice(const char* soundname, float Duration) : SoundDuration(Duration), KeyWasPressed(0), KeyBind(0)
	{
		Soundname = new char[strlen(soundname) + 1];

		strcpy(Soundname, soundname);
	}
};

namespace VoiceChat
{
	void PrepareFiles() noexcept;

	void ProcessVoiceOff() noexcept;

	void Run(AudioVoice* Data = 0) noexcept;

	extern float EndTime;

	extern std::vector<AudioVoice> SoundList;
}

#endif
