#ifndef SkinChanger_H
#define SkinChanger_H

namespace SkinChanger
{
	struct SC_ModelInfo
	{
		SC_ModelInfo(std::string Name)
		{
			data.second = 0;

			data.first = Name;
		}

		std::pair<std::string, int>data;

		std::vector<std::pair<std::string, int>>models;
	};

	void PrepareFiles() noexcept;

	extern void PrecacheModels() noexcept;

	extern std::vector<SC_ModelInfo>WeaponsInfo;

	extern void Run(CBaseEntity* pLocal) noexcept;
}

#endif
