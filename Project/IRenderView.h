#pragma once

#include "Vector.h"
#include "Color.h"

class IVRenderView
{
public:
	void SetBlend(float blend)
	{
		using Type = void(__thiscall*)(void*, float);

		GetVirtualMethod<Type>(this, 4)(this, blend);
	}

	float GetBlend()
	{
		using Type = float(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 5)(this);
	}

	void SetColorModulation(float const* blend)
	{
		using Type = void(__thiscall*)(void*, float const*);

		GetVirtualMethod<Type>(this, 6)(
			this, blend);
	}

	void GetColorModulation(float* blend)
	{
		using Type = void(__thiscall*)(void*, float*);

		GetVirtualMethod<Type>(this, 7)(
			this, blend);
	}

	inline void SetColorModulation(float r, float g, float b, float a = 255)
	{
		float clr[4] = { r, g, b,a };

		SetColorModulation(clr);
	}
};
