#ifndef General_H
#define General_H

struct EmitSound_t
{
	int m_nChannel;
	char const* m_pSoundName;
	float m_flVolume;
};

void __fastcall Hooked_EmitSound
(
	void* ECX,
	void* EDX,
	void*& filter,
	int iEntIndex,
	int iChannel,
	const char* pSample,
	float flVolume,
	float flAttenuation,
	int iFlags = 0,
	int iPitch = 100,
	int iSpecialDSP = 0,
	const Vector* pOrigin = NULL,
	const Vector* pDirection = NULL,
	CUtlVector< Vector >* pUtlVecOrigins = NULL,
	bool bUpdatePositions = true,
	float soundtime = 0.0f,
	int speakerentity = -1
);

extern HWND hWindow;

extern char Load_Other_Stuff;

extern WNDPROC Original_Wnd_Proc;

extern float Global_Jumps, Perfect_Jumps;

extern CLC_ListenEvents* CLC_ListenEvents_Table;

extern CLC_RespondCvarValue* CLC_RespondCvarValue_Table;

extern std::vector<class DesiredConVarsValueInfo> DesiredConVarsValue;

using DoPostScreenSpaceEffects_Type = bool(__thiscall*)(void*, const CViewSetup*);

using PaintTraverse_Type = void(__thiscall*)(void*, unsigned int, bool, bool);

using OverrideView_Type = void(__thiscall*)(void*, void*);

using EmitSound_Type = decltype(&Hooked_EmitSound);

using GetViewModelFov_Type = float(__thiscall*)();

using DrawModelExecute_Type = void(__thiscall*)(
	void*,
	const void*&,
	const ModelRenderInfo_t&,
	matrix3x4*
	);

extern EmitSound_Type Original_EmitSound;
extern OverrideView_Type Original_OverrideView;
extern PaintTraverse_Type Original_PaintTraverse;
extern GetViewModelFov_Type Original_GetViewModelFov;
extern DrawModelExecute_Type Original_DrawModelExecute;
extern DoPostScreenSpaceEffects_Type Original_DoPostScreenSpaceEffects;

void __fastcall PaintTraverse(PVOID pPanels, int edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce) noexcept;

unsigned __int32 __stdcall Wnd_Proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) noexcept;

void __stdcall DrawModelExecute(const void*& state, const ModelRenderInfo_t& pInfo, matrix3x4* pCustomBoneToWorld) noexcept;

void __fastcall OverrideView(void* _this, void* _edx, CViewSetup* pSetup) noexcept;

float __fastcall GetViewModelFov() noexcept;

bool __stdcall DoPostScreenSpaceEffects(const CViewSetup* pSetup) noexcept;

// Detours \\


class DetourHookInfo
{
public:
	PVOID Original;

	DetourHookInfo(uintptr_t Addr, PVOID Fn, size_t offset);

	~DetourHookInfo();

private:

	PVOID Location;
};

extern DetourHookInfo* Warning;
extern DetourHookInfo* CheckCRCs;
extern DetourHookInfo* DrawModel;
extern DetourHookInfo* EmitSound;
extern DetourHookInfo* GetFgColor;
extern DetourHookInfo* ReadWavFile;
extern DetourHookInfo* R_LevelInit;
extern DetourHookInfo* GetCvarValue;
extern DetourHookInfo* GetPMaterial;
extern DetourHookInfo* SetViewModel;
extern DetourHookInfo* CheckWhitelist;
extern DetourHookInfo* ProcessFixAngle;
extern DetourHookInfo* DrawSpriteModel;
extern DetourHookInfo* FireEventIntern;
extern DetourHookInfo* CL_QueueDownload;
extern DetourHookInfo* SimulateEntities;
extern DetourHookInfo* ProcessStringCmd;
extern DetourHookInfo* ConsistencyCheck;
extern DetourHookInfo* CalcViewModelView;
extern DetourHookInfo* OnScreenSizeChanged;
extern DetourHookInfo* WriteListenEventList;
extern DetourHookInfo* CMatRenderContext_Bind;
extern DetourHookInfo* C_ParticleSmokeGrenade;
extern DetourHookInfo* ProcessMuzzleFlashEvent;
extern DetourHookInfo* C_BaseAnimating_DrawModel;
extern DetourHookInfo* CSpriteTrail_GetRenderOrigin;
extern DetourHookInfo* ClientModeShared_FireGameEvent;
extern DetourHookInfo* CCSMapOverview_CanPlayerBeSeen;
extern DetourHookInfo* CSteam3Client_InitiateConnection;
extern DetourHookInfo* CTraceFilterSimple_ShouldHitEntity;

void Hooked_R_LevelInit();

void __fastcall Hooked_CMatRenderContext_Bind(void* Self, void*, IMaterial* iMaterial, void* proxyData);

int __fastcall Hooked_C_BaseAnimating_DrawModel(CBaseEntity* ECX, void*, int flags);

const Vector& __fastcall Hooked_CSpriteTrail_GetRenderOrigin(void* ECX);

void __fastcall Hooked_ProcessMuzzleFlashEvent(void* ECX);

void __fastcall Hooked_SetViewModel(void* ECX);

void Hooked_DrawSpriteModel
(
	CSprite* baseentity,
	void* psprite,
	const Vector& origin,
	float fscale,
	float frame,
	int rendermode,
	int r, int g, int b, int a,
	const Vector& forward,
	const Vector& right,
	const Vector& up,
	float flHDRColorScale
);

Color& __fastcall Hooked_GetFgColor(void* Self, void* EDX, void* UnknownArg);

bool __cdecl Hooked_ReadWavFile
(
	const char* pFilename, char*& pData,
	int& nDataBytes, int& wBitsPerSample,
	int& nChannels, int& nSamplesPerSec
);

void __stdcall Hooked_OnScreenSizeChanged(int OldWidht, int OldHeight) noexcept;

void __cdecl Hooked_SimulateEntities();

bool __fastcall Hooked_CheckCRCs(void*, void*, const char* szMap) noexcept;

void __fastcall Hooked_CheckWhitelist(void*, void*, int iUnknown) noexcept;

void __fastcall Hooked_ConsistencyCheck(void* pThis, void*, bool bChanged) noexcept;

bool __fastcall Hooked_ProcessFixAngle(void* ECX, void* EDX, SVC_FixAngle* msg);

void __fastcall Hooked_SmokeGrenade_Start(CBaseEntity* ecx, void* edx, void* pParticleMgr, void* pArgs);

void __stdcall Hooked_ClientModeShared_FireGameEvent(IGameEvent* event);

void __fastcall Hooked_CalcViewModelView(CBaseEntity* pThis, void*, CBaseEntity* owner, const Vector& eyePosition, const QAngle& eyeAngles);

void __cdecl Hooked_Warning(char* msg, ...);

void __cdecl Hooked_CL_QueueDownload(const char* Name) noexcept;

bool __stdcall Hooked_ProcessStringCmd(NET_StringCmd* msg);

int __fastcall Hooked_DrawModel(CBaseEntity* pThis, void*, int flags);

bool __fastcall Hooked_CCSMapOverview_CanPlayerBeSeen(void* Self, void* EDX, void* player);

bool __fastcall Hooked_CTraceFilterSimple_ShouldHitEntity(void* Self, void*, CBaseEntity* pEntity, int contentsMask);

void* __fastcall Hooked_GetPMaterial(void* pThis, void* EDX, const char* MaterialName);

bool __fastcall Hooked_FireEventIntern(void* pThis, void* EDX, IGameEvent* event, bool bServerOnly, bool bClientOnly);

bool __fastcall Hooked_GetCvarValue(void* ECX, void* EDX, SVC_GetCvarValue* msg);

void __stdcall Hooked_WriteListenEventList(void* msg) noexcept;

void __stdcall Hooked_CSteam3Client_InitiateConnection
(
	void* data,
	int nMaxData, 
	int& nOutSize,
	long nIP, 
	short nPort,
	unsigned long long nGSSteamID,
	bool bSecure
);

#endif
