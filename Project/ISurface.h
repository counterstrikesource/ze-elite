#pragma once

#ifdef CreateFont
#undef CreateFont
#endif

typedef unsigned int VPANEL;
typedef unsigned long HFont;

class IPanel
{
public:

	void SetMouseInputEnabled(VPANEL vguiPanel, bool state)
	{
		using Type = void(__thiscall*)(void*, VPANEL, bool);
			
		return GetVirtualMethod<Type>(this, 32)(this, vguiPanel, state);
	}

	const char* GetName(VPANEL vguiPanel)
	{
		using Type = const char* (__thiscall*)(void*, VPANEL);

		return GetVirtualMethod<Type>(this, 36)(this, vguiPanel);
	}
};

class ISurface
{
public:

	void DrawSetColor(Color col)
	{
		using Type = void(__thiscall*)(void*, Color);

		GetVirtualMethod<Type>(this, 10)(this, col);
	}

	void DrawFilledRect(int x0, int y0, int x1, int y1)
	{
		using Type = void(__thiscall*)(void*, int, int, int, int);

		GetVirtualMethod<Type>(this, 12)(this, x0, y0, x1, y1);
	}

	void DrawOutlinedRect(int x0, int y0, int x1, int y1)
	{
		using Type = void(__thiscall*)(void*, int, int, int, int);

		GetVirtualMethod<Type>(this, 14)(this, x0, y0, x1, y1);
	}

	void DrawLine(int x0, int y0, int x1, int y1)
	{
		using Type = void(__thiscall*)(void*, int, int, int, int);

		GetVirtualMethod<Type>(this, 15)(this, x0, y0, x1, y1);
	}

	void DrawSetTextFont(HFont font)
	{
		using Type = void(__thiscall*)(void*, HFont);

		GetVirtualMethod<Type>(this, 17)(this, font);
	}

	void DrawSetTextColor(Color col)
	{
		using Type = void(__thiscall*)(void*, Color);

		GetVirtualMethod<Type>(this, 18)(this, col);
	}

	void DrawSetTextPos(int x, int y)
	{
		using Type = void(__thiscall*)(void*, int, int);

		GetVirtualMethod<Type>(this, 20)(this, x, y);
	}

	void DrawPrintText(const wchar_t* text, int textLen, int drawType = 0)
	{
		using Type = void(__thiscall*)(void*, const wchar_t*, int, int);

		GetVirtualMethod<Type>(this, 22)(this, text, textLen, drawType);
	}

	HFont CreateFont()
	{
		using Type = HFont(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 66)(this);
	}

	bool SetFontGlyphSet
	(
		HFont font,
		const char* windowsFontName,
		int tall, int weight, int blur, int scanlines,
		int flags, int nRangeMin = 0, int nRangeMax = 0) {

		using Type = bool(__thiscall*)(void*, HFont, const char*, int, int, int, int, int, int, int);

		return GetVirtualMethod<Type>(this, 67)(this, font, windowsFontName, tall, weight, blur, scanlines, flags, nRangeMin, nRangeMax);
	}

	void GetTextSize(HFont font, const wchar_t* text, int& wide, int& tall)
	{
		using Type = void(__thiscall*)(void*, HFont, const wchar_t*, int&, int&);

		GetVirtualMethod<Type>(this, 75)(this, font, text, wide, tall);
	}

	void Play_Sound(const char* fileName)
	{
		using Type = void(__thiscall*)(void*, const char*);

		GetVirtualMethod<Type>(this, 78)(this, fileName);
	}

	void DrawOutlinedCircle(int x, int y, int radius, int segments)
	{
		using Type = void(__thiscall*)(void*, int, int, int, int);

		GetVirtualMethod<Type>(this, 99)(this, x, y, radius, segments);
	}
};