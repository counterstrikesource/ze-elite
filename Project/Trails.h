#ifndef Trails_H
#define Trails_H

class CBaseEntity;
class CSpriteTrail;

namespace Trails
{
	void Precache() noexcept;

	void Create(CBaseEntity* pParent) noexcept;

	extern CSpriteTrail* Entity;
}

#endif
