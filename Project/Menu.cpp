#include "include.h"
#include "SDK.h"
#include "Menu.h"
#include <string>
#include <vector>
#include <array>

#include "Assorted.h"
#include "MaterialManager.h"
#include "ImGui/ImGui_impl_win32.h"
#include "ImGui/ImGui_impl_dx9.h"
#define IMGUI_DEFINE_MATH_OPERATORS
#include "ImGui/ImGui_internal.h"
#include "ImGui/Imgui.h"
#include "SkinChanger.h"
#include "CvarQuery.h"
#include "VoiceChat.h"
#include "Config.h"
#include "Glow.h"
#include "Trails.h"
#include "SteamID/SteamID.h"
#include "WorldTextures.h"
#include "General.h"

#define Push() ImGui::PushID(__LINE__);

#define Pop() ImGui::PopID();

#define PushW(W) ImGui::PushItemWidth(W);

#define PopW() ImGui::PopItemWidth();

#define SpawnTestVar() static float Var(100);\
\
Push();\
ImGui::SliderFloat("", &Var, 20, 500.f);\
Pop();

constexpr auto windowFlags = ImGuiWindowFlags_NoCollapse
| ImGuiWindowFlags_NoResize
| ImGuiWindowFlags_NoScrollbar
| ImGuiWindowFlags_AlwaysAutoResize
| ImGuiWindowFlags_NoScrollWithMouse;

extern void mmcopy
(
	void* address,
	const void* value,
	size_t bytes
);

extern float Since_Last_Used;

namespace ImGui
{
	static auto vector_getter = [](void* vec, int idx, const char** out_text)
	{
		auto& vector = *static_cast<std::vector<std::string>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*out_text = vector.at(idx).c_str();
		return true;
	};

	bool Combo(const char* label, int* currIndex, std::vector<std::string>& values)
	{
		if (values.empty()) { return false; }
		return Combo(label, currIndex, vector_getter,
			static_cast<void*>(&values), values.size());
	}

	bool ListBox(const char* label, int* currIndex, std::vector<std::string>& values)
	{
		if (values.empty()) { return false; }
		return ListBox(label, currIndex, vector_getter,
			static_cast<void*>(&values), values.size());
	}
}

void HotKey(unsigned short& key) noexcept
{
	std::string KeyName = InputSystem->VirtualKeyToString(key);

	std::transform(KeyName.begin(), KeyName.begin() + 1, KeyName.begin(), ::toupper);

	if (KeyName.size() > 2)
	{
		std::transform(KeyName.begin() + 1, KeyName.end(), KeyName.begin() + 1, ::tolower);
	}

	key ? ImGui::Text("[ %s ]", KeyName.c_str()) : ImGui::TextUnformatted("[ Key ]");

	if (!ImGui::IsItemHovered())
		return;

	ImGui::SetTooltip("Press Key");

	ImGuiIO& io = ImGui::GetIO();

	for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++)
	{
		if (ImGui::IsKeyPressed(i) && i != Menu::Get.Menu.Key)
		{
			key = i != VK_ESCAPE ? i : 0;
		}
	}

	for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); i++)
	{
		if (ImGui::IsMouseDown(i) && i + (i > 1 ? 2 : 1) != Menu::Get.Menu.Key)
		{
			key = i + (i > 1 ? 2 : 1);
		}
	}
}

void DrawRectRainbow(int x, int y, int width, int height, float flSpeed, float& flRainbow) noexcept
{
	ImDrawList* windowDrawList = ImGui::GetWindowDrawList();

	Color colColor(0, 0, 0, 255);

	flRainbow += flSpeed;

	if (flRainbow > 1.f) flRainbow = 0.f;

	for (int i = 0; i < width; i++)
	{
		float hue = (1.f / (float)width) * i;
		hue -= flRainbow;
		if (hue < 0.f) hue += 1.f;

		Color colRainbow = colColor.FromHSB(hue, 1.f, 1.f);

		windowDrawList->AddRectFilled(ImVec2(x + i, y), ImVec2(width, height), colRainbow.GetU32());
	}
}

bool ColorPopup(Color& color, const char* name,int ID,bool p4Float = false) noexcept
{
	std::array<float, 4> Clr{ color.r() / 255.f , color.g() / 255.f, color.b() / 255.f, color.a() / 255.f };

	ImGui::PushID(ID);

	bool ReturnValue{ false };

	ImColor Colorr{ Clr[0], Clr[1], Clr[2], Clr[3] };

	bool openPopup = ImGui::ColorButton("", Colorr, ImGuiColorEditFlags_NoTooltip | ImGuiColorEditFlags_AlphaPreview);

	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(IMGUI_PAYLOAD_TYPE_COLOR_4F))
			Clr = *(std::array<float, 4>*)payload->Data;
		ImGui::EndDragDropTarget();
	}

	ImGui::SameLine(0.0f, 5.0f);

	ImGui::TextUnformatted(name);

	if (openPopup)
		ImGui::OpenPopup("");

	if (ImGui::BeginPopup(""))
	{
		if (ImGui::ColorPicker4("", Clr.data(), ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaPreview | ImGuiColorEditFlags_AlphaBar))
		{
			color.SetColor((float*)&Clr);

			ReturnValue = true;
		}

		ImGui::EndPopup();
	}
	
	Pop();

	return ReturnValue;
}

bool ColorPopupV2(std::array<float, 4>& color, const char* name, int ID, bool p4Float = false) noexcept
{
	ImGui::PushID(ID);

	bool ReturnValue{ false };

	ImColor Colorr{ color[0], color[1], color[2], color[3] };

	bool openPopup = ImGui::ColorButton("", Colorr, ImGuiColorEditFlags_NoTooltip | ImGuiColorEditFlags_AlphaPreview);

	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(IMGUI_PAYLOAD_TYPE_COLOR_4F))
			color = *(std::array<float, 4>*)payload->Data;
		ImGui::EndDragDropTarget();
	}

	ImGui::SameLine(0.0f, 5.0f);

	ImGui::TextUnformatted(name);

	if (openPopup)
		ImGui::OpenPopup("");

	if (ImGui::BeginPopup(""))
	{
		if (ImGui::ColorPicker4("", color.data(), ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaPreview | ImGuiColorEditFlags_AlphaBar))
		{
			ReturnValue = true;
		}

		ImGui::EndPopup();
	}

	Pop();

	return ReturnValue;
}

void Menu::ZE() noexcept
{
	constexpr auto& General = Menu::Get.General;

	ImGui::Checkbox(General.ZeStuff.AutoLaser, &General.ZeStuff.AutoLaser);

	ImGui::Checkbox(Menu::Get.Misc.AutoKevlar, &Menu::Get.Misc.AutoKevlar);

	if (ImGui::Checkbox(General.ZeStuff.Wireframe_lasers, &General.ZeStuff.Wireframe_lasers)) {

		blueblacklargebeam.first = MaterialActions::Update;
	}

	if (ImGui::Checkbox(General.ZeStuff.Special_items_IgnoreZ, &General.ZeStuff.Special_items_IgnoreZ)) {

		ItemsMat.first = MaterialActions::Update;
	}

	ImGui::PushItemWidth(100);

	//ImGui::Text("Filter");

	Push();

	ImGui::Combo(General.ZeStuff.Filter, &General.ZeStuff.Filter, "Jump Only\0Duck Only\0Both\0");

	Pop();

	Push();

	ImGui::Combo(Menu::Get.Visuals.ZE_LasersTexture, &Menu::Get.Visuals.ZE_LasersTexture, ListOfEffects.first.m_szNames);

	Pop();

	ImGui::PopItemWidth();

	ImGui::Separator();

	if (ImGui::Button("Save", ImVec2(100, 20)))
	{
		Config::SaveConfig();
	}

	if (ImGui::Button("Load", ImVec2(100, 20)))
	{
		Config::LoadConfig();

		Load::Repeatedly();
	}

	if (ImGui::Button("Reset", ImVec2(100, 20)))
	{
		AssignVariables(1);
	}

	if (ImGui::Button(General.Panic, ImVec2(100, 22)))
	{
		General.Panic = !General.Panic;
	}

	ImGui::SameLine();

	HotKey(Menu::Get.Keys.PanicKey);

	if (ImGui::Button(General.Unload, ImVec2(100, 22)))
	{
		General.Unload = true;
	}

	ImGui::SameLine();

	HotKey(Menu::Get.Keys.Unload);
}

void Menu::Aimbot() noexcept
{
	ImGui::Columns(2, nullptr, false);

	ImGui::SetColumnOffset(1, 200);

	ImGui::SetColumnWidth(0, 357.91f);

	constexpr auto& Aimbot = Menu::Get.Aimbot;

	ImGui::Checkbox(Aimbot.Ragebot.Enabled, &Aimbot.Ragebot.Enabled);

	ImGui::Checkbox(Aimbot.Ragebot.AutoStop, &Aimbot.Ragebot.AutoStop);

	ImGui::Checkbox(Aimbot.Ragebot.AutoShoot, &Aimbot.Ragebot.AutoShoot);

	ImGui::Checkbox(Aimbot.Ragebot.AutoCrouch, &Aimbot.Ragebot.AutoCrouch);

	ImGui::Checkbox(Aimbot.Ragebot.FriendlyFire, &Aimbot.Ragebot.FriendlyFire);

	ImGui::Checkbox(Aimbot.Ragebot.Predicted, &Aimbot.Ragebot.Predicted);

	ImGui::Checkbox(Aimbot.LAC_Bypass, &Aimbot.LAC_Bypass);

	ImGui::NextColumn();

	ImGui::PushItemWidth(130);

	ImGui::SliderInt(Aimbot.Ragebot.FOV, &Aimbot.Ragebot.FOV, 1, 360);

	ImGui::SliderFloat(Aimbot.Ragebot.SmoothVal, &Aimbot.Ragebot.SmoothVal, 1, 35);

	ImGui::SliderInt(Aimbot.Ragebot.Timeout, &Aimbot.Ragebot.Timeout, 0, 1000, "%d ms");

	static int IHitBox{ 0 };

	constexpr std::array HitBoxsName
	{
		"Head",
		"Neck",
		"Spine2",
		"Spine1",
		"Pelvis",
		"Left Hand", "Right Hand",
		"Left Foot", "Right Foot",
		"Left Thigh", "Right Thigh",
		"Left Forearm", "Right Forearm",
		"Left Upper arm", "Right Upper arm",
	};

	constexpr std::array HitBoxsID
	{
		HITBOX_HEAD,
		HITBOX_NECK,
		HITBOX_SPINE2,
		HITBOX_SPINE1,
		HITBOX_PELVIS,
		HITBOX_L_HAND, HITBOX_R_HAND,
		HITBOX_L_FOOT, HITBOX_R_FOOT,
		HITBOX_L_THIGH, HITBOX_R_THIGH,
		HITBOX_L_FOREARM, HITBOX_R_FOREARM,
		HITBOX_L_UPPERARM, HITBOX_R_UPPERARM,
	};

	static_assert(HitBoxsName.size() == HitBoxsID.size());

	if (ImGui::Combo(Aimbot.Ragebot.HitBox, &IHitBox, HitBoxsName.data(), HitBoxsName.size()))
	{
		Aimbot.Ragebot.HitBox = HitBoxsID[IHitBox];
	}

	ImGui::Combo("Silent", &Menu::Get.Aimbot.Ragebot.Silent, "Off\0" "Client Side\0""Server Side\0");

	ImGui::Combo("Selection", &Menu::Get.Aimbot.Ragebot.TargetSelection, "Distance\0" "FOV\0");

	ImGui::Combo("Usage", &Menu::Get.Aimbot.Ragebot.Usage, "Always\0" "On Attack\0");

	ImGui::PopItemWidth();

	ImGui::Columns(1);
}

void Menu::TriggerBot() noexcept
{
	constexpr auto& TriggerBot = Menu::Get.Misc.Triggerbot;

	PushW(100);

	ImGui::Checkbox(TriggerBot.Enabled, &TriggerBot.Enabled);

	ImGui::Checkbox("Friendly fire", &TriggerBot.Filter[5]);

	Push();

	ImGui::Combo("", &TriggerBot.Usage, "Auto\0""On Key\0");

	Pop();

	ImGui::SameLine();

	HotKey(Menu::Get.Keys.Triggerbot);

	PopW();

	ImGui::Text(TriggerBot.Filter);

	ImGui::Separator();

	ImGui::Selectable(" Head", &TriggerBot.Filter.Get(0));

	ImGui::Selectable(" Chest", &TriggerBot.Filter.Get(1));

	ImGui::Selectable(" Stomach", &TriggerBot.Filter.Get(2));

	ImGui::Selectable(" Arms", &TriggerBot.Filter.Get(3));

	ImGui::Selectable(" Legs", &TriggerBot.Filter.Get(4));
}

void RenderGlowTab() noexcept
{
	ImGui::TextColored(ImColor(255, 20, 20, 255), "Enemies");

	ImGui::SameLine(430.f);

	ImGui::TextColored(ImColor(20, 20, 255, 255), "Teammates");

	ImGui::Columns(2, nullptr, false);

	ImGui::SetColumnOffset(1, 200);

	ImGui::SetColumnWidth(0, 425.f);

	Push();

	constexpr auto& Glow = Menu::Get.Visuals.Glow;

	ImGui::Checkbox(Glow.Enabled, &Glow.Enabled[Enemies]);
	{
		ImGui::SameLine(89.0f);

		ColorPopup((Color&)Menu::Get.Colors.Glow.Player[Enemies * sizeof(Color)], "", __LINE__);
	}

	Pop();

	ImGui::NextColumn();

	Push();

	ImGui::Checkbox(Glow.Enabled, &Glow.Enabled[Teammates]);
	{
		ImGui::SameLine(89.0f);

		ColorPopup((Color&)Menu::Get.Colors.Glow.Player[Teammates * sizeof(Color)], "", __LINE__);
	}

	Pop();

	ImGui::Columns();
}

void RenderChamsTab() noexcept
{
	constexpr auto& Player = Menu::Get.Visuals.Chams.Player;

	ImGui::Checkbox(Player.Enabled, &Player.Enabled);
	{
		ImGui::SameLine(90.5f);

		ColorPopup(Menu::Get.Colors.Chams.Player, "", __LINE__);
		{
			ImGui::SameLine(121.1F);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup(Player.Enabled.GetDebugName());
			}

			if (ImGui::BeginPopup(Player.Enabled.GetDebugName()))
			{
				ImGui::Checkbox(Player.Wireframe, &Player.Wireframe);

				ImGui::Checkbox(Player.Ignorez, &Player.Ignorez);

				ImGui::PushItemWidth(105.f);

				ImGui::Combo(Menu::Get.Visuals.Chams.Player.Type, &Menu::Get.Visuals.Chams.Player.Type, "Normal\0Flat\0Debug\0Water\0");

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}
	}

	constexpr auto& Weapon = Menu::Get.Visuals.Chams.Weapon;

	ImGui::Checkbox(Weapon.Enabled, &Weapon.Enabled);
	{
		ImGui::SameLine(90.5f);

		ColorPopup(Menu::Get.Colors.Chams.Weapon, "", __LINE__);
		{
			ImGui::SameLine(121.1F);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup(Weapon.Enabled.GetDebugName());
			}

			if (ImGui::BeginPopup(Weapon.Enabled.GetDebugName()))
			{
				ImGui::Checkbox(Weapon.Wireframe, &Weapon.Wireframe);

				ImGui::Checkbox(Weapon.Ignorez, &Weapon.Ignorez);

				ImGui::PushItemWidth(105.f);

				ImGui::Combo(Menu::Get.Visuals.Chams.Weapon.Type, &Menu::Get.Visuals.Chams.Weapon.Type, "Normal\0Flat\0Debug\0Water\0");

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}
	}
}

extern CBaseEntity* RainEntity;

void Menu::Visuals() noexcept
{
	static int CurrentTab{ 0 };

	constexpr auto& VisualsV = Menu::Get.Visuals;

	if (ImGui::Button("Player Esp", ImVec2(174.71f, 25)))
	{
		if (CurrentTab)
			CurrentTab = 0;
	}

	ImGui::SameLine();

	if (ImGui::Button("Assorted", ImVec2(174.71f, 25)))
	{
		if (CurrentTab != 1)
			CurrentTab = 1;
	}

	ImGui::SameLine();

	if (ImGui::Button("Skin Changer", ImVec2(174.71f, 25)))
	{
		if (CurrentTab != 2)
			CurrentTab = 2;
	}

	ImGui::Separator();

	if (CurrentTab == NULL)
	{
		ImGui::TextColored(ImColor(255, 20, 20, 255), "Enemies");

		ImGui::SameLine(380.f);

		ImGui::TextColored(ImColor(20, 20, 255, 255), "Teammates");
	
		ImGui::Columns(2, nullptr, false);

		ImGui::SetColumnOffset(1, 200);

		ImGui::SetColumnWidth(0, 375.f);

		Push();

		ImGui::Checkbox(VisualsV.Box, &VisualsV.Box[Enemies]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Box[Enemies * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup(VisualsV.Box.GetDebugName());
				}

				if (ImGui::BeginPopup(VisualsV.Box.GetDebugName()))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.BoxType, &VisualsV.BoxType[Enemies], "2D\0" "2D corners\0");

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}

		ImGui::Checkbox(VisualsV.Name, &VisualsV.Name[Enemies]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Name[Enemies * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup(VisualsV.Name.GetDebugName());
				}

				if (ImGui::BeginPopup(VisualsV.Name.GetDebugName()))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.NameFont, &VisualsV.NameFont[Enemies], "Tahoma [14]\0Tahoma [25, 15, 1]\0Arial [16]\0Verdana [12]\0");

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}

		ImGui::Checkbox(VisualsV.Health, &VisualsV.Health[Enemies]);

		ImGui::SameLine(89.0f);

		ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Health[Enemies * sizeof(Color)], "", __LINE__);
		{
			ImGui::SameLine(119.1F);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup(VisualsV.Health.GetDebugName());
			}

			if (ImGui::BeginPopup(VisualsV.Health.GetDebugName()))
			{
				ImGui::PushItemWidth(105.f);

				ImGui::Combo("Mode", &VisualsV.HealthType[Enemies], "Text\0Bar\0Sprite Bar\0");

				if (VisualsV.HealthType[Enemies] == 2)
				{
					if (ImGui::SliderFloat(VisualsV.HP_SpriteScale, &VisualsV.HP_SpriteScale[Enemies], 0.05, 1))
					{
						HP_Bar::UpdateSpritesScale();
					}
				}
				else if (VisualsV.HealthType == 0)
				{
					ImGui::Combo(VisualsV.HealthFont, &VisualsV.HealthFont[Enemies], "Tahoma [14]\0Tahoma [25, 15, 1]\0Arial [16]\0Verdana [12]\0");
				}

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}



		ImGui::Checkbox(VisualsV.Weapon, &VisualsV.Weapon[Enemies]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Weapon[Enemies * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup(VisualsV.Weapon.GetDebugName());
				}

				if (ImGui::BeginPopup(VisualsV.Weapon.GetDebugName()))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.WeaponRenderingMode, &VisualsV.WeaponRenderingMode[Enemies], "Name\0Icon\0");

					if (VisualsV.WeaponRenderingMode[Enemies] == 0)
					{
						ImGui::Combo(VisualsV.WeaponFont, &VisualsV.WeaponFont[Enemies], "Tahoma [14]\0Tahoma [25, 15, 1]\0Arial [16]\0Verdana [12]\0");
					}

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}


		ImGui::Checkbox(VisualsV.SnapLine, &VisualsV.SnapLine[Enemies]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.SnapLine[Enemies * sizeof(Color)], "", __LINE__);
		}

		ImGui::Checkbox(VisualsV.Skeleton, &VisualsV.Skeleton[Enemies]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Skeleton[Enemies * sizeof(Color)], "", __LINE__);
		}

		Pop();

		ImGui::NextColumn();

		Push();

		ImGui::Checkbox(VisualsV.Box, &VisualsV.Box[Teammates]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Box[Teammates * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup(VisualsV.Box.GetDebugName());
				}

				if (ImGui::BeginPopup(VisualsV.Box.GetDebugName()))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.BoxType, &VisualsV.BoxType[Teammates], "2D\0" "2D corners\0");

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}

		ImGui::Checkbox(VisualsV.Name, &VisualsV.Name[Teammates]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Name[Teammates * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup(VisualsV.Name.GetDebugName());
				}

				if (ImGui::BeginPopup(VisualsV.Name.GetDebugName()))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.NameFont, &VisualsV.NameFont[Teammates], "Tahoma [14]\0Tahoma [25, 15, 1]\0Arial [16]\0Verdana [12]\0");

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}

		ImGui::Checkbox(VisualsV.Health, &VisualsV.Health[Teammates]);

		ImGui::SameLine(89.0f);

		ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Health[Teammates * sizeof(Color)], "", __LINE__);
		{
			ImGui::SameLine(119.1F);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup(VisualsV.Health.GetDebugName());
			}

			if (ImGui::BeginPopup(VisualsV.Health.GetDebugName()))
			{
				ImGui::PushItemWidth(105.f);

				ImGui::Combo(VisualsV.HealthType, &VisualsV.HealthType[Teammates], "Text\0Bar\0Sprite Bar\0");

				if (VisualsV.HealthType[Teammates] == 2)
				{
					if (ImGui::SliderFloat(VisualsV.HP_SpriteScale, &VisualsV.HP_SpriteScale[Teammates], 0.05, 1))
					{
						HP_Bar::UpdateSpritesScale();
					}
				}
				else if (VisualsV.HealthType[Teammates] == 0)
				{
					ImGui::Combo(VisualsV.HealthFont, &VisualsV.HealthFont[Teammates], "Tahoma [14]\0Tahoma [25, 15, 1]\0Arial [16]\0Verdana [12]\0");
				}

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Checkbox(VisualsV.Weapon, &VisualsV.Weapon[Teammates]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Weapon[Teammates * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup("Weapon");
				}

				if (ImGui::BeginPopup("Weapon"))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.WeaponRenderingMode, &VisualsV.WeaponRenderingMode[Teammates], "Name\0Icon\0");

					if (VisualsV.WeaponRenderingMode[Teammates] == 0)
					{
						ImGui::Combo(VisualsV.WeaponFont, &VisualsV.WeaponFont[Teammates], "Tahoma [14]\0Tahoma [25, 15, 1]\0Arial [16]\0Verdana [12]\0");
					}

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}

		ImGui::Checkbox(VisualsV.SnapLine, &VisualsV.SnapLine[Teammates]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.SnapLine[Teammates * sizeof(Color)], "", __LINE__);
		}

		ImGui::Checkbox(VisualsV.Skeleton, &VisualsV.Skeleton[Teammates]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Skeleton[Teammates * sizeof(Color)], "", __LINE__);
		}

		Pop();

		ImGui::Columns();

		ImGui::Separator();

		Push();

		ImGui::TextColored(ImColor(255, 255, 20, 255), "Dropped Weapons");

		ImGui::Checkbox(VisualsV.Box, &VisualsV.Box[DroppedWpns]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Box[DroppedWpns * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup(VisualsV.Box.GetDebugName());
				}

				if (ImGui::BeginPopup(VisualsV.Box.GetDebugName()))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.BoxType, &VisualsV.BoxType[DroppedWpns], "2D\0" "2D corners\0");

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}

		ImGui::Checkbox(VisualsV.Name, &VisualsV.Name[DroppedWpns]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.Name[DroppedWpns * sizeof(Color)], "", __LINE__);
			{
				ImGui::SameLine(119.1F);

				Push();

				if (ImGui::Button("...", ImVec2(50, 20)))
				{
					ImGui::OpenPopup(VisualsV.Name.GetDebugName());
				}

				if (ImGui::BeginPopup(VisualsV.Name.GetDebugName()))
				{
					ImGui::PushItemWidth(105.f);

					ImGui::Combo(VisualsV.NameFont, &VisualsV.NameFont[DroppedWpns], "Tahoma [14]\0Tahoma [25, 15, 1]\0Arial [16]\0Verdana [12]\0");

					ImGui::PopItemWidth();

					ImGui::EndPopup();
				}

				Pop();
			}
		}

		ImGui::Checkbox(VisualsV.SnapLine, &VisualsV.SnapLine[DroppedWpns]);
		{
			ImGui::SameLine(89.0f);

			ColorPopup((Color&)Menu::Get.Colors.PlayerEsp.SnapLine[DroppedWpns * sizeof(Color)], "", __LINE__);
		}

		Pop();
	}
	else if (CurrentTab == 1)
	{
		ImGui::Columns(2, nullptr, false);

		ImGui::SetColumnOffset(1, 200);

		ImGui::SetColumnWidth(0, 351.f);

		PushW(135.F);

		ImGui::Combo(VisualsV.Hands, &VisualsV.Hands, Hands.m_szNames);

		Push();

		if (ImGui::Combo(VisualsV.ScopeLen, &VisualsV.ScopeLen, ScopeOverlays))
		{
			UpdateScopeLens();
		}

		Pop();

		ImGui::Combo(VisualsV.KillEffect_Type, &VisualsV.KillEffect_Type, KillEffects.data(), KillEffects.size());

		ImGui::Combo(VisualsV.BackgroundTexture, &VisualsV.BackgroundTexture, Background.m_szNames);

		ImGui::Combo(VisualsV.MuzzleFlash, &VisualsV.MuzzleFlash, ListOfEffects.first.m_szNames);

		Push();

		ImGui::Combo(VisualsV.WorldTexture, &VisualsV.WorldTexture, World.m_szNames);

		ImGui::SameLine();

		if (ColorPopup(Menu::Get.Colors.General.World, "World", __LINE__))
		{
			World_Textures::WorldColorModulateAction = MaterialActions::Update;
		}

		Pop();

		Push();

		if (ImGui::Combo(VisualsV.SkyBoxIndex, &VisualsV.SkyBoxIndex, SkyBoxes)) {

			using Type = bool(__cdecl*)(const char*);

			if (!VisualsV.SkyBoxIndex)
			{
				if (sv_skyname) 
				{
					reinterpret_cast<Type>(LoadSkyBox)(sv_skyname->GetString());
				}
			}
			else
			{
				reinterpret_cast<Type>(LoadSkyBox)(SkyBoxes[VisualsV.SkyBoxIndex].c_str());
			}
				
			
		}Pop();

		ImGui::SameLine();

		if (ColorPopup(Menu::Get.Colors.General.SkyBox, Menu::Get.Colors.General.SkyBox, __LINE__))
		{
			World_Textures::SkyBoxColorModulateAction = MaterialActions::Update;
		}

		Push();

		if (ImGui::Combo(VisualsV.NightVision, &VisualsV.NightVision, NightVisions))
		{
			UpdateNightVision();
		}

		ImGui::SameLine();

		if (ColorPopup(Menu::Get.Colors.General.Nightvision, "Nightvision", __LINE__))
		{
			mmcopy(overlaycolor, (Color&)Menu::Get.Colors.General.Nightvision.Get(), 4);

			if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal)
			{
				pLocal->IsNightVisionOn() = pLocal->GetNightVisionAlpha() = Menu::Get.Colors.General.Nightvision[Color::Elements::Alpha];
			}
		}

		Pop();

		Push();

		ImGui::Combo("", &VisualsV.DLights, "Off\0My Player\0Enemies\0Teammates\0All\0");

		ImGui::SameLine();

		ColorPopup(Menu::Get.Colors.PlayerEsp.DLights, "Dynamic-Lights", __LINE__);

		Pop();

		PopW();

		ColorPopup(Menu::Get.Colors.General.HudColor, "Hud", __LINE__);

		if (ColorPopup(Menu::Get.Colors.General.Net_graph, "Net graph", __LINE__))
		{
			Update_Net_Graph_Text_Colors();
		}

		ColorPopup(Menu::Get.Colors.General.NadeTracer, "Nade Tracer", __LINE__);

		if (ImGui::IsItemHovered() || ImGui::IsItemActive())
		{
			ImGui::SameLine();

			ImGui::Text("Set alpha to active");
		}

		if (ColorPopup(Menu::Get.Colors.General.BulletImpact, "Bullet Impact", __LINE__))
		{
			ProcessGameEventListeners();
		}

		if (ImGui::IsItemHovered() || ImGui::IsItemActive())
		{
			ImGui::SameLine();

			ImGui::Text("Set alpha to active");
		}

		if (ImGui::Checkbox(VisualsV.FullBright, &VisualsV.FullBright))
		{
			if (mat_fullbright)
				mat_fullbright->SetValueAndFlags(VisualsV.FullBright);
		}

		if (ImGui::Checkbox(VisualsV.NoFog, &VisualsV.NoFog))
		{
			if (Fog_Enable)
				Fog_Enable->SetValueAndFlags(!VisualsV.NoFog);

			if (Fog_Override)
				Fog_Override->SetValueAndFlags(VisualsV.NoFog);
		}

		ImGui::Checkbox(VisualsV.NoSmoke, &VisualsV.NoSmoke);

		ImGui::Checkbox(VisualsV.Disable_Fire_Particles, &VisualsV.Disable_Fire_Particles);

		if (ImGui::IsItemHovered()) {

			ImGui::SameLine();

			ImGui::TextUnformatted("Burning Character");
		}

		ImGui::Checkbox(VisualsV.NoVisualRecoil, &VisualsV.NoVisualRecoil);

		ImGui::Checkbox(VisualsV.NoTeammates, &VisualsV.NoTeammates);

		if (ImGui::Checkbox(VisualsV.NoFlashLight, &VisualsV.NoFlashLight)) {

			if (VisualsV.NoFlashLight && Engine->IsInGame())
			{
				auto LocalPlayer = CBaseEntity::GetLocalPlayer();

				LoopTroughPlayers()
				{
					if (auto Entity(GetEntity(i)); Entity && Entity->IsPlayer() && Entity != LocalPlayer)
						if (Entity->GetEffects() & EF_DIMLIGHT)
							Entity->GetEffects() &= ~EF_DIMLIGHT;

				}
			}
		}

		ImGui::Checkbox(VisualsV.Disable_Players_MuzzleFlash, &VisualsV.Disable_Players_MuzzleFlash);

		ImGui::NextColumn();

		PushW(100.f);

		Push();

		ImGui::SliderInt(VisualsV.ViewModelFov, &VisualsV.ViewModelFov, -60, 60, "%d");

		Pop();

		Push();

		ImGui::SliderInt(VisualsV.FieldOfView, &VisualsV.FieldOfView, -60, 60, "%d");

		Pop();

		Push();

		ImGui::SliderInt(VisualsV.FlashPercentage, &VisualsV.FlashPercentage, 0.f, 100.f, "%d%%");

		Pop(); 

		if (ImGui::SliderInt(VisualsV.NightMode, &VisualsV.NightMode, 1, 100, "%d%%")) {

			World_Textures::NightModeModulateAction = MaterialActions::Update;
		}

		PopW();


		ImGui::Text("\n\n");

		ImGui::Text("Fog");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("Fog");
			}

			if (ImGui::BeginPopup("Fog"))
			{
				constexpr auto& Fog = VisualsV.FOG;

				if (ImGui::Checkbox(Fog.Enabled, &Fog.Enabled))
				{
					if (Fog_Override)
					{
						Fog_Override->SetValueAndFlags(Fog.Enabled);
					}

					if (Fog_Enable)
					{
						Fog_Enable->SetValueAndFlags(Fog.Enabled && !VisualsV.NoFog);
					}
				}

				ImGui::SameLine();

				if (ColorPopup(Menu::Get.Colors.General.Fog, "", __LINE__))
				{
					if (fog_color)
					{
						char Buffer[64];

						snprintf(Buffer, sizeof Buffer, "%i %i %i", Menu::Get.Colors.General.Fog[0], Menu::Get.Colors.General.Fog[1], Menu::Get.Colors.General.Fog[2]);

						fog_color->SetValue(Buffer);
					}
				}

				ImGui::PushItemWidth(91.f);

				if (ImGui::SliderFloat(Fog.start, &Fog.start, -1, 9999))
				{
					if (static auto fog_start = Cvar->FindVar("fog_start"); fog_start)
					{
						fog_start->SetValueAndFlags(Fog.start);
					}
				}

				if (ImGui::SliderFloat(Fog.end, &Fog.end, -1, 9999))
				{
					if (static auto fog_end = Cvar->FindVar("fog_end"); fog_end)
					{
						fog_end->SetValueAndFlags(Fog.end);
					}
				}

				if (ImGui::SliderFloat(Fog.maxdensity, &Fog.maxdensity, 0.f, 1.f))
				{
					if (fog_maxdensity)
					{
						fog_maxdensity->SetValueAndFlags(VisualsV.FOG.maxdensity);
					}
				}

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Text("Debug");

		ImGui::SameLine(107.f);

		Push();

		if (ImGui::Button("...", ImVec2(50, 20)))
		{
			ImGui::OpenPopup("Debug");
		}

		if (ImGui::BeginPopup("Debug"))
		{
			PushW(300);

			constexpr auto& Debug = VisualsV.Debug;

			ImGui::Checkbox(Debug.INDEX, &Debug.INDEX);

			ImGui::Checkbox(Debug.Classname, &Debug.Classname);

			ImGui::Checkbox(Debug.ModelName, &Debug.ModelName);

			ImGui::Checkbox(Debug.ModelIndex, &Debug.ModelIndex);

			ImGui::Checkbox(Debug.Origin, &Debug.Origin);

			ImGui::Checkbox(Debug.Velocity, &Debug.Velocity);

			PopW();

			PushW(180.f);

			ImGui::Text("Ignore:");

			ImGui::Separator();

			ImGui::Checkbox("Players", &VisualsV.Debug.Filter[0]);

			ImGui::Checkbox("Weapons", &VisualsV.Debug.Filter[1]);

			ImGui::Checkbox("Sprites", &VisualsV.Debug.Filter[2]);

			ImGui::Checkbox("Motionless", &VisualsV.Debug.Filter[3]);

			ImGui::SliderInt(VisualsV.Debug.MaxDistance, &VisualsV.Debug.MaxDistance, 0, 5000, "if distance over: %d unit");

			PopW();

			ImGui::EndPopup();
		}

		Pop();

		ImGui::Text("Trails");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("Trails");
			}

			if (ImGui::BeginPopup("Trails"))
			{
				constexpr auto& TrailsOptions = VisualsV.Trails;

				ImGui::Checkbox(TrailsOptions.Enabled, &TrailsOptions.Enabled);

				ImGui::SameLine();

				if (ColorPopup(Menu::Get.Colors.General.Trails, "", __LINE__))
				{
					if (Trails::Entity)
					{
						Trails::Entity->GetRenderColor() = Menu::Get.Colors.General.Trails;
					}
				}

				ImGui::PushItemWidth(91.f);

				if (ImGui::Combo(TrailsOptions.SpriteTexture, &TrailsOptions.SpriteTexture, TrailMaterials.first) && Trails::Entity)
				{
					auto ModelIndex = TrailMaterials.second[Menu::Get.Visuals.Trails.SpriteTexture];

					if (ModelIndex != -1)
					{
						Trails::Entity->SetModelByIndex(ModelIndex);
					}
				}

				if (ImGui::Combo(TrailsOptions.RenderMode, &TrailsOptions.RenderMode, "Normal\0TransColor\0TransTexture\0Glow\0TransAlpha\0TransAdd\0"))
				{
					if (Trails::Entity)
					{
						Trails::Entity->GetRenderMode() = TrailsOptions.RenderMode;
					}
				}

				if (ImGui::InputFloat(TrailsOptions.Width, &TrailsOptions.Width))
				{
					if (Trails::Entity)
					{
						Trails::Entity->GetStartWidth() = TrailsOptions.Width;
					}
				}

				if (ImGui::InputFloat(TrailsOptions.EndWidth, &TrailsOptions.EndWidth))
				{
					if (Trails::Entity)
					{
						Trails::Entity->GetEndWidth() = TrailsOptions.EndWidth;
					}
				}

				if (ImGui::InputFloat(TrailsOptions.LifeTime, &TrailsOptions.LifeTime))
				{
					if (Trails::Entity)
					{
						Trails::Entity->GetLifeTime() = TrailsOptions.LifeTime;
					}
				}


				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Text("Tracers");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("Tracers");
			}

			if (ImGui::BeginPopup("Tracers"))
			{
				if(ImGui::Checkbox(VisualsV.Tracers.Enabled, &VisualsV.Tracers.Enabled))
				{
					ProcessGameEventListeners();
				}

				ImGui::SameLine();

				ColorPopup(Menu::Get.Colors.General.Tracers, "", __LINE__);

				ImGui::PushItemWidth(91.f);

				ImGui::Combo(VisualsV.Tracers.SpriteTexture, &VisualsV.Tracers.SpriteTexture, TracerSprites.data(), TracerSprites.size());

				ImGui::SliderFloat(VisualsV.Tracers.PointScale, &VisualsV.Tracers.PointScale, -1.f, 1.f);

				ImGui::SameLine();

				if (ImGui::Button("Reset"))
				{
					VisualsV.Tracers.PointScale = 0.03f;
				}

				ImGui::InputFloat(VisualsV.Tracers.m_flLife, &VisualsV.Tracers.m_flLife);

				ImGui::InputFloat(VisualsV.Tracers.m_flWidth, &VisualsV.Tracers.m_flWidth);

				ImGui::InputFloat(VisualsV.Tracers.m_flEndWidth, &VisualsV.Tracers.m_flEndWidth);

				ImGui::InputFloat(VisualsV.Tracers.m_flFadeLength, &VisualsV.Tracers.m_flFadeLength);

				ImGui::InputFloat(VisualsV.Tracers.m_flAmplitude, &VisualsV.Tracers.m_flAmplitude);

				ImGui::InputFloat(VisualsV.Tracers.m_flSpeed, &VisualsV.Tracers.m_flSpeed);

				ImGui::InputInt(VisualsV.Tracers.m_nSegments, &VisualsV.Tracers.m_nSegments, 0, 0);

				ImGui::InputInt(VisualsV.Tracers.m_nFlags, &VisualsV.Tracers.m_nFlags, 0, 0);

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Text("Weather");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("Weather");
			}

			if (ImGui::BeginPopup("Weather"))
			{
				constexpr auto& Weather = VisualsV.Weather;

				ImGui::Checkbox(Weather.Enabled, &Weather.Enabled);

				//ImGui::SameLine();

				//CreateColorFunction(Tracers);

				//Tracers(Menu::Get.Colors.General.Tracers, "");

				ImGui::PushItemWidth(91.f);

				if (ImGui::Combo(Weather.Type, &Weather.Type, "Rain\0Snow\0"))
				{
					if (RainEntity)
					{
						RainEntity->GetPrecipType() = Weather.Type;
					}
				}

				if(ImGui::SliderFloat(Weather.Width, &Weather.Width, 0.5f, 5.f))
				{
					if (r_rainwidth)
					{
						r_rainwidth->SetValueAndFlags(Weather.Width);
					}
				}

				if (ImGui::SliderFloat(Weather.Speed, &Weather.Speed, 600.f, 2000.f))
				{
					if (r_rainspeed)
					{
						r_rainspeed->SetValueAndFlags(Weather.Speed);
					}
				}

				/*if (ImGui::SliderFloat("Radius", &Weather.Radius, 500.f, 2000.f))
				{
					if (r_RainRadius)
					{
						r_RainRadius->SetValueAndFlags(Weather.Radius);
					}
				}*/

				if (ImGui::SliderFloat(Weather.Length, &Weather.Length, 0.1f, 0.8f))
				{
					if (r_rainlength)
					{
						r_rainlength->SetValueAndFlags(Weather.Length);
					}
				}

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			Pop();
		}


		ImGui::Text("Crosshair");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("Crosshair");
			}

			if (ImGui::BeginPopup("Crosshair"))
			{
				//SpawnTestVar();

				constexpr auto& CrosshairStruct = VisualsV.Crosshair;

				constexpr auto& CrosshairColor = Menu::Get.Colors.General.Crosshair;

				ImGui::Checkbox(CrosshairStruct.Recoil, &CrosshairStruct.Recoil);

				constexpr auto& Circle_Shape = CrosshairStruct.Circle_Shape;

				constexpr auto& Plus_Shape = CrosshairStruct.Plus_Shape;

				constexpr auto& X_Shape = CrosshairStruct.X_Shape;

				ImGui::Text("Pluse Shape");
				{
					ImGui::SameLine(80.f);

					Push();

					if (ImGui::Button("...", ImVec2(40, 20)))
					{
						ImGui::OpenPopup("Plus_Shape");
					}

					if (ImGui::BeginPopup("Plus_Shape"))
					{
						if (ImGui::Checkbox(Plus_Shape.Enabled, &Plus_Shape.Enabled))
						{
							if (CCCrosshair)
							{
								CCCrosshair->SetValueAndFlags(!Plus_Shape.Enabled && !X_Shape.Enabled && !Circle_Shape.Enabled);
							}
						}

						ImGui::SameLine();

						ColorPopup(CrosshairColor.Plus_Shape, "", __LINE__);

						ImGui::PushItemWidth(90.f);

						ImGui::SliderInt(Plus_Shape.Length, &Plus_Shape.Length, 0, 20, "%d");

						ImGui::SliderInt(Plus_Shape.Gap, &Plus_Shape.Gap, 0, 20, "%d");

						ImGui::PopItemWidth();

						ImGui::EndPopup();
					}

					Pop();
				}

				ImGui::Text("X Shape");
				{
					ImGui::SameLine(80.f);

					Push();

					if (ImGui::Button("...", ImVec2(40, 20)))
					{
						ImGui::OpenPopup("X_Shape");
					}

					if (ImGui::BeginPopup("X_Shape"))
					{
						if (ImGui::Checkbox(X_Shape.Enabled, &X_Shape.Enabled))
						{
							if (CCCrosshair)
							{
								CCCrosshair->SetValueAndFlags(!Plus_Shape.Enabled && !X_Shape.Enabled && !Circle_Shape.Enabled);
							}
						}

						ImGui::SameLine();

						ColorPopup(CrosshairColor.X_Shape, "", __LINE__);

						ImGui::PushItemWidth(90.f);

						ImGui::SliderInt(X_Shape.Length, &X_Shape.Length, 0, 20, "%d");

						ImGui::SliderInt(X_Shape.Gap, &X_Shape.Gap, 0, 20, "%d");

						ImGui::PopItemWidth();

						ImGui::EndPopup();
					}

					Pop();
				}

				ImGui::Text("Circle");
				{
					ImGui::SameLine(80.f);

					Push();

					if (ImGui::Button("...", ImVec2(40, 20)))
					{
						ImGui::OpenPopup("Circle_Shape");
					}

					if (ImGui::BeginPopup("Circle_Shape"))
					{

						if (ImGui::Checkbox(Circle_Shape.Enabled, &Circle_Shape.Enabled))
						{
							if (CCCrosshair)
							{
								CCCrosshair->SetValueAndFlags(!Plus_Shape.Enabled && !X_Shape.Enabled && !Circle_Shape.Enabled);
							}
						}

						ImGui::SameLine();

						ColorPopup(CrosshairColor.Circle_Shape, "", __LINE__);

						ImGui::PushItemWidth(90.f);

						ImGui::SliderInt(Circle_Shape.Radius, &Circle_Shape.Radius, 0, 20, "%d");

						ImGui::SliderInt(Circle_Shape.Segments, &Circle_Shape.Segments, 0, 20, "%d");

						ImGui::PopItemWidth();

						ImGui::EndPopup();
					}

					Pop();
				}

				ImGui::Text("You can use more than one shape, Customize a cool crosshair");

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Text("Hitmarker");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("Hitmarker");
			}

			if (ImGui::BeginPopup("Hitmarker"))
			{
				if(ImGui::Checkbox(VisualsV.Hitmarker.Enabled, &VisualsV.Hitmarker.Enabled))
				{
					ProcessGameEventListeners();
				}

				ImGui::SameLine();

				ColorPopup(Menu::Get.Colors.General.Hitmarker, "", __LINE__);

				ImGui::PushItemWidth(90.f);

				ImGui::Combo(VisualsV.Hitmarker.Mode, &VisualsV.Hitmarker.Mode, "Normal\0Overlay\0");

				ImGui::Combo(VisualsV.Hitmarker.HitSound, &VisualsV.Hitmarker.HitSound, HitSounds);

				if (!VisualsV.Hitmarker.Mode)
				{
					ImGui::SliderInt(VisualsV.Hitmarker.Length, &VisualsV.Hitmarker.Length, 0, 20, "%d");

					ImGui::SliderInt(VisualsV.Hitmarker.Gap, &VisualsV.Hitmarker.Gap, 0, 20, "%d");
				}
				else
				{
					ImGui::Combo(VisualsV.Hitmarker.Overlay, &VisualsV.Hitmarker.Overlay, HitmarkerOverlays);
				}

				ImGui::SliderInt(VisualsV.Hitmarker.Time, &VisualsV.Hitmarker.Time, 1, 1500, "%d ms");

				ImGui::PopItemWidth();

				ImGui::Text("Show Damage");
				{
					ImGui::SameLine();

					Push();

					if (ImGui::Button("...", ImVec2(50, 20)))
					{
						ImGui::OpenPopup("DmgIndicator");
					}

					if (ImGui::BeginPopup("DmgIndicator"))
					{
						if (ImGui::Checkbox(VisualsV.DamageIndicator.Enabled, &VisualsV.DamageIndicator.Enabled))
						{
							ProcessGameEventListeners();
						}

						ImGui::SameLine();

						ColorPopup(Menu::Get.Colors.General.DamageIndicator, "", __LINE__);

						ImGui::EndPopup();
					}

					Pop();
				}

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Text("ViewModel");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("ViewModel");
			}

			if (ImGui::BeginPopup("ViewModel"))
			{
				PushW(150.f);

				ImGui::TextUnformatted("Origin");

				Push(); ImGui::SliderFloat("", &VisualsV.ViewModel.Origin[0], -5.f, 5.f, "Forward %.2f"); Pop();
				Push(); ImGui::SliderFloat("", &VisualsV.ViewModel.Origin[1], -5.f, 5.f, "Right %.2f"); Pop();
				Push(); ImGui::SliderFloat("", &VisualsV.ViewModel.Origin[2], -5.f, 5.f, "Up %.2f"); Pop();

				ImGui::TextUnformatted("Angles");

				Push(); ImGui::SliderFloat("", &VisualsV.ViewModel.Angles[0], -90.f, 90.f, "Pitch %.2f"); Pop();
				Push(); ImGui::SliderFloat("", &VisualsV.ViewModel.Angles[1], -90.f, 90.f, "Yaw %.2f"); Pop();
				Push(); ImGui::SliderFloat("", &VisualsV.ViewModel.Angles[2], -90.f, 90.f, "Roll %.2f"); Pop();

				PopW();

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Text("Third Person");
		{
			ImGui::SameLine(107.f);

			Push();

			if (ImGui::Button("...", ImVec2(50, 20)))
			{
				ImGui::OpenPopup("Third Person");
			}

			if (ImGui::BeginPopup("Third Person"))
			{
				PushW(150.f);

				if (ImGui::SliderInt(Menu::Get.Misc.m_vecCameraOffset, &Menu::Get.Misc.m_vecCameraOffset, 0, 300, "%d"))
				{
					void UpdateThirdPerson();
					UpdateThirdPerson();
				}

				PopW();

				ImGui::EndPopup();
			}

			Pop();
		}

		ImGui::Columns(1);
	}
	else
	{
		unsigned short Iteration = 0;

		PushW(152.f);

		if (ImGui::Button("Force Full Update", ImVec2(152.f,0)))
		{
			ForceFullUpdate();
		}

		std::vector<std::string>ModelsNames;

		ImGui::BeginChild("Valid-Models", ImVec2{ 538.5f , 284.3f });
		{
			PushW(152.f);

			for (auto&& WeaponInfo : SkinChanger::WeaponsInfo)
			{
				auto& Models = WeaponInfo.models;

				if (Models.size() > 1)
				{
					ModelsNames.clear();

					for (auto Model : Models)
					{
						ModelsNames.push_back(Model.first);
					}

					auto& data = WeaponInfo.data;

					if (ImGui::Combo(data.first.data(), &data.second, ModelsNames))
					{
						if (data.second == 0 && Engine->IsInGame())
							ForceFullUpdate();
					}
				}
			}

			PopW();

		}ImGui::EndChild();

		ImGui::Separator();

		ImGui::BeginChild("Non-Valid-Models", ImVec2{ 538.5f , 210 });
		{
			PushW(152.f);

			for (auto&& WeaponInfo : SkinChanger::WeaponsInfo)
			{
				auto& Models = WeaponInfo.models;

				if (Models.size() == 1)
				{
					ModelsNames.clear();

					for (auto Model : Models)
					{
						ModelsNames.push_back(Model.first);
					}

					auto& data = WeaponInfo.data;

					ImGui::Combo(data.first.data(), &data.second, ModelsNames);
				}
			}

			PopW();

		}ImGui::EndChild();


		PopW();
	}
}

void Menu::Miscellaneous() noexcept
{
	constexpr auto& Misc = Menu::Get.Misc;

	ImGui::Separator();

	ImGui::Columns(2, nullptr, false);

	ImGui::SetColumnOffset(1, 200);

	ImGui::SetColumnWidth(0, 384.4f);
	
	ImGui::Checkbox(Misc.Fastrun, &Misc.Fastrun);

	ImGui::Checkbox(Misc.Faststop, &Misc.Faststop);

	ImGui::Checkbox(Misc.AutoPisol, &Misc.AutoPisol);

	ImGui::Checkbox(Misc.Sv_Pure_Bypass, &Misc.Sv_Pure_Bypass);

	ImGui::Checkbox(Misc.AntiAFKkick, &Misc.AntiAFKkick);

	ImGui::Checkbox(Misc.MuteRadio, &Misc.MuteRadio);

	ImGui::Checkbox(Misc.NoMotd, &Misc.NoMotd);

	ImGui::Checkbox(Misc.NoRecoil, &Misc.NoRecoil);

	ImGui::Checkbox(Misc.NoSpread, &Misc.NoSpread);

	ImGui::Checkbox(Misc.FastLadder, &Misc.FastLadder);

	ImGui::Checkbox(Misc.NoDrug, &Misc.NoDrug);

	ImGui::Checkbox(Misc.NoShake, &Misc.NoShake);

	ImGui::Checkbox(Misc.Disable_Game_Console_Warnings, &Misc.Disable_Game_Console_Warnings);

	ImGui::NextColumn();

	PushW(110.f);

	PopW();

	ImGui::Checkbox(Misc.Autostrafe, &Misc.Autostrafe);

	ImGui::SameLine(122.f);

	HotKey(Menu::Get.Keys.Autostrafe);

	ImGui::Checkbox(Misc.CircleStrafe, &Misc.CircleStrafe);

	ImGui::SameLine(122.f);

	HotKey(Menu::Get.Keys.CircleStrafe);

	ImGui::Checkbox(Misc.Edgejump, &Misc.Edgejump);

	ImGui::SameLine(122.f);

	HotKey(Menu::Get.Keys.Edgejump);

	ImGui::Text("\n");

	ImGui::Text("KnifeBot");
	{
		ImGui::SameLine(107.f);

		Push();

		if (ImGui::Button("...", ImVec2(50, 20)))
		{
			ImGui::OpenPopup("KnifeBot");
		}

		if (ImGui::BeginPopup("KnifeBot"))
		{
			constexpr auto& KnifeBot = Misc.KnifeBot;

			ImGui::Checkbox(KnifeBot.Enabled, &KnifeBot.Enabled);

			ImGui::SameLine();

			ImGui::TextColored(ImColor(20, 255, 20), "Key");

			ImGui::SameLine();

			HotKey(Menu::Get.Keys.KnifeBot);

			ImGui::PushItemWidth(150.f);

			ImGui::Combo(KnifeBot.Method, &KnifeBot.Method, "Left\0Right\0Smart\0");

			ImGui::Combo(KnifeBot.Usage, &KnifeBot.Usage, "On Key\0Always\0");

			ImGui::PopItemWidth();

			ImGui::EndPopup();
		}

		Pop();
	}

	ImGui::Text("Bunnyhop");
	{
		ImGui::SameLine(107.f);

		Push();

		if (ImGui::Button("...", ImVec2(50, 20)))
		{
			ImGui::OpenPopup("Bhop");
		}

		if (ImGui::BeginPopup("Bhop"))
		{
			ImGui::PushItemWidth(300);

			ImGui::Checkbox(Misc.Bunnyhop, &Misc.Bunnyhop);

			Push();

			ImGui::SliderInt(Misc.Bunnyhop_Perfect_Rate, &Misc.Bunnyhop_Perfect_Rate, 1, 100, "Perfection %d%%");

			Pop();

			HotKey(Menu::Get.Keys.Bunnyhop);

			std::string KeyName = InputSystem->VirtualKeyToString(Menu::Get.Keys.Bunnyhop);

			std::transform(KeyName.begin(), KeyName.begin() + 1, KeyName.begin(), ::toupper);

			if (KeyName.size() > 2)
			{
				std::transform(KeyName.begin() + 1, KeyName.end(), KeyName.begin() + 1, ::tolower);
			}

			ImGui::SameLine();

			ImGui::TextColored(ImColor{ 20,255,20 }, "Make sure that [%s] is unbounded in Game!", Menu::Get.Keys.Bunnyhop ? KeyName.c_str() : "Key");

			ImGui::PopItemWidth();

			ImGui::EndPopup();
		}

		Pop();
	}

	ImGui::Text("Strafe Optimizer");
	{
		ImGui::SameLine(107.f);

		Push();

		if (ImGui::Button("...", ImVec2(50, 20)))
		{
			ImGui::OpenPopup("Optimizer");
		}

		if (ImGui::BeginPopup("Optimizer"))
		{
			constexpr auto& StrafeOptimizer = Misc.StrafeOptimizer;

			ImGui::Checkbox(StrafeOptimizer.Enabled, &StrafeOptimizer.Enabled);

			ImGui::SameLine();

			ImGui::TextColored(ImColor{ 20,255,20 }, "We appreciate TraitCore for this <3");

			PushW(250);

			ImGui::SliderFloat(StrafeOptimizer.Desired_Gain, &StrafeOptimizer.Desired_Gain, 0, 100, "%.2f");

			ImGui::SliderFloat(StrafeOptimizer.Required_Speed, &StrafeOptimizer.Required_Speed, 0, 4000, "%.0f");

			ImGui::SliderFloat(StrafeOptimizer.Greatest_Possible_Strafe_Angle, &StrafeOptimizer.Greatest_Possible_Strafe_Angle, 0, 180, "%.2f");

			PopW();

			ImGui::EndPopup();
		}

		Pop();
	}

	PushW(156.f);

	ImGui::Text("\n");

	ImGui::Text(""); ImGui::SameLine(67.3f); ImGui::Text("Sounds\n");

	Push();

	if (ImGui::SliderInt(Misc.Sounds.MusicVolume, &Misc.Sounds.MusicVolume, 0, 100, "Music Volume: %d%%"))
	{
		if (snd_musicvolume)
		{
			snd_musicvolume->SetValueAndFlags(Misc.Sounds.MusicVolume * 0.01f);
		}
	}

	Pop();

	Push();

	ImGui::SliderInt(Misc.Sounds.WeaponsAudio, &Misc.Sounds.WeaponsAudio, 0, 100, "Weapons Volume: %d%%");

	Pop();

	Push();

	ImGui::SliderInt(Misc.Sounds.FootStepsVolume, &Misc.Sounds.FootStepsVolume, 0, 100, "Footsteps Volume: %d%%");

	Pop();

	ImGui::Text("\n");

	ImGui::Text(""); ImGui::SameLine(67.3f); ImGui::Text("Other\n");

	Push();

	ImGui::SliderInt(Misc.Ragdoll_Force, &Misc.Ragdoll_Force, 1, 1000, "Ragdoll Force: Mult by %d");

	Pop();


	PopW();

	ImGui::Columns(1);

	ImGui::Separator();

	ImGui::TextUnformatted("Kill Message");

	ImGui::SameLine();

	if (ImGui::Button("...", ImVec2(50, 20)))
	{
		ImGui::OpenPopup("Killmsg");
	}

	if (ImGui::BeginPopup("Killmsg"))
	{
		ImGui::Checkbox(Misc.KillMessage, &Misc.KillMessage);

		ImGui::SameLine();

		ImGui::TextUnformatted("\t\tRandom Msg will be chosen when you kill someone");

		static char KillMessage[64]{ 0 };

		PushW(500.F);

		Push();

		ImGui::InputText("", KillMessage, IM_ARRAYSIZE(KillMessage));

		Pop();

		if (ImGui::Button("Insert", ImVec2(500.F, 0)))
		{
			if (KillMessage && KillMessage[0])
			{
				Killmessages.push_back(std::string(KillMessage));

				memset(KillMessage, 0, IM_ARRAYSIZE(KillMessage));
			}
		}

		for (size_t ID = 0; ID < Killmessages.size(); ID++)
		{
			ImGui::TextUnformatted(Killmessages[ID].c_str());

			ImGui::SameLine(408.f);

			ImGui::PushID(__LINE__ + ID);

			if (ImGui::Button("Delete", ImVec2(100, 0)))
			{
				Killmessages.erase(Killmessages.begin() + ID);
			}

			ImGui::PopID();
		}

		PopW();

		ImGui::EndPopup();
	}

	ImGui::SameLine();

	ImGui::Text("Player Info");

	ImGui::SameLine();

	Push();

	if (ImGui::Button("...", ImVec2(50, 20)))
	{
		ImGui::OpenPopup("Info");
	}

	if (ImGui::BeginPopup("Info"))
	{
		ImGui::PushItemWidth(100);

		static char Name[100]{ "" }, ClanTag[100]{ "" }, DisConnectMsg[1000]{ "" };

		ImGui::BeginChild("PlayerName", ImVec2{ 200 , 210 });
		{
			ImGui::Text("Name");

			Push();

			ImGui::InputTextMultiline("", Name, IM_ARRAYSIZE(Name), ImVec2(100, 85));

			Pop();

			Push();

			if (ImGui::Button("Clear Input", ImVec2(100, 20)))
			{
				memset(Name, 0, sizeof(Name));
			}

			if (ImGui::Button("Horizontal Tab", ImVec2(100, 20)))
			{
				Name[strlen(Name)] = '\t';
			}

			if (ImGui::Button("New Line", ImVec2(100, 20)))
			{
				Name[strlen(Name)] = '\n';
			}

			if (ImGui::Button("Apply", ImVec2(100, 0)))
			{
				if (static auto NameConVar = Cvar->FindVar("name"); NameConVar)
				{
					NameConVar->SetValue(Name);
				}

				if (Engine->IsInGame())
				{
					ChangeName(Name);
				}
			}

			ImGui::PopID();

		}ImGui::EndChild();


		ImGui::SameLine();

		ImGui::BeginChild("ClanTag", ImVec2{ 200 , 210 });
		{
			constexpr auto& Clan_Tag = Misc.ClanTag;

			ImGui::Text("ClanTag");

			Push();

			ImGui::InputTextMultiline("", ClanTag, IM_ARRAYSIZE(ClanTag), ImVec2(100, 60));

			Pop();

			Push();

			if (ImGui::Button("Clear Input", ImVec2(100, 20)))
			{
				memset(ClanTag, 0, sizeof(ClanTag));
			}

			if (ImGui::Button("Horizontal Tab", ImVec2(100, 20)))
			{
				ClanTag[strlen(ClanTag)] = '\t';
			}

			if (ImGui::Button("New Line", ImVec2(100, 20)))
			{
				ClanTag[strlen(ClanTag)] = '\n';
			}

			if (ImGui::Button("Apply", ImVec2(100, 0)))
			{
				SetClanTag(ClanTag);

				Since_Last_Used = Globals->curtime;
			}

			if (ImGui::Button("Spammer", ImVec2(100, 0)))
			{
				ImGui::OpenPopup("Spammer");
			}

			if (ImGui::BeginPopup("Spammer"))
			{
				if (ImGui::Button("Changer", ImVec2(135, 0)))
				{
					ImGui::OpenPopup("Changer");
				}

				if (ImGui::BeginPopup("Changer"))
				{
					static char CustomClanTag[100]{};

					ImGui::Checkbox(Clan_Tag.Changer, &Clan_Tag.Changer);

					Push();

					ImGui::InputTextMultiline("", CustomClanTag, IM_ARRAYSIZE(CustomClanTag), ImVec2(120, 60));

					Pop();

					if (ImGui::Button("Insert", ImVec2(115, 0)))
					{
						if (CustomClanTag && CustomClanTag[0])
						{
							ClanTagInfo.push_back(CustomClanTag);

							memset(CustomClanTag, 0, IM_ARRAYSIZE(CustomClanTag));
						}

						if (ClanTagInfo.size() > 100)
						{
							ClanTagInfo.pop_back();
						}
					}

					for (size_t ID = 0; ID < ClanTagInfo.size(); ID++)
					{
						ImGui::Text("%s\n", ClanTagInfo[ID]);

						ImGui::SameLine(140.f);

						ImGui::PushID(__LINE__ + ID);

						if (ImGui::Button("Delete", ImVec2(100, 0)))
						{
							ClanTagInfo.erase(ClanTagInfo.begin() + ID);

							memset(CustomClanTag, 0, IM_ARRAYSIZE(CustomClanTag));
						}

						ImGui::PopID();
					}

					ImGui::EndPopup();
				}

				if (ImGui::Button("Stealer", ImVec2(135, 0)))
				{
					ImGui::OpenPopup("Stealer");
				}

				if (ImGui::BeginPopup("Stealer"))
				{
					ImGui::Checkbox(Clan_Tag.Stealer, &Clan_Tag.Stealer);

					ImGui::EndPopup();
				}

				ImGui::PushItemWidth(140);

				ImGui::SliderFloat("Delay", &Clan_Tag.Delay, 1.f, 10.f, "%.2f Seconds");

				ImGui::PopItemWidth();

				ImGui::EndPopup();
			}

			ImGui::PopID();

		}ImGui::EndChild();

		ImGui::SameLine();

		ImGui::BeginChild("Disconnect", ImVec2{ 200 , 210 });
		{
			ImGui::Text("Disconnect Msg");

			Push();

			ImGui::InputTextMultiline("", DisConnectMsg, IM_ARRAYSIZE(DisConnectMsg), ImVec2(120, 60));

			Pop();

			Push();

			if (ImGui::Button("Clear Input", ImVec2(120, 20)))
			{
				memset(DisConnectMsg, 0, sizeof(DisConnectMsg));
			}

			if (ImGui::Button("Horizontal Tab", ImVec2(120, 20)))
			{
				DisConnectMsg[strlen(DisConnectMsg)] = '\t';
			}

			if (ImGui::Button("New Line", ImVec2(120, 20)))
			{
				DisConnectMsg[strlen(DisConnectMsg)] = '\n';
			}

			if (ImGui::Button("Apply", ImVec2(120, 0)))
			{
				Set_DisConnection_Msg(DisConnectMsg);
			}

			if (ImGui::Button("Default Msg", ImVec2(120, 0)))
			{
				Set_DisConnection_Msg("Disconnect by user.");
			}

			ImGui::PopID();

		}ImGui::EndChild();

		ImGui::SameLine();

		ImGui::BeginChild("SteamID Spoofer", ImVec2{ 200 , 210 });
		{
			constexpr auto& SteamIDSpoofer = Misc.SteamIDSpoofer;

			ImGui::Text("SteamID Spoofer");

			ImGui::PushItemWidth(130);

			ImGui::Checkbox(SteamIDSpoofer.Enabled, &SteamIDSpoofer.Enabled);

			ImGui::Checkbox(SteamIDSpoofer.Random, &SteamIDSpoofer.Random);

			Push();

			ImGui::InputInt("", &Misc.SteamIDSpoofer.SteamID);

			ImGui::PopID();

			ImGui::TextUnformatted(SteamID(Misc.SteamIDSpoofer.SteamID).Format(SteamID::Formats::STEAMID32).c_str());

			ImGui::PopItemWidth();

		}ImGui::EndChild();

		ImGui::PopItemWidth();

		ImGui::EndPopup();
	}

	Pop();

	ImGui::SameLine();

	ImGui::Text("Voice Chat");

	ImGui::SameLine();

	Push();

	if (ImGui::Button("...", ImVec2(50, 20)))
	{
		ImGui::OpenPopup("Voice Chat");
	}

	if (ImGui::BeginPopup("Voice Chat"))
	{
		int ID = 0;

		for (auto& SoundInfo : VoiceChat::SoundList)
		{
			ImGui::TextUnformatted(SoundInfo.Soundname);
			
			ImGui::SameLine(300.F); HotKey(SoundInfo.KeyBind);
			
			ImGui::SameLine(350.f);

			ImGui::PushID(__LINE__ + ID++);

			if (ImGui::Button("Play"))
			{
				VoiceChat::Run(&SoundInfo);
			}

			Pop();
		}

		ImGui::EndPopup();
	}

	Pop();


	ImGui::Separator();

	PushW(210.3f);

	ImGui::TextColored(ImColor{ 20,255,20 }, "Download Manager");

	ImGui::SameLine();

	Push();

	ImGui::Combo(Misc.DownloadManagerFilter, &Misc.DownloadManagerFilter, "All\0No Sounds\0Maps Only\0None\0Custom File Extension\0");

	Pop();

	ImGui::SameLine();

	Push();

	ImGui::InputText("", Misc.CustomizeableFiles, 100);

	Pop();

	PopW();
}

void Menu::ConVars() noexcept
{
	static float Value{ 0 };

	static int Flags{ 0 };

	static char name[64]{};

	ImGui::TextColored((ImVec4)ImColor(20, 255, 20), "Enforce ConVars");

	ImGui::TextColored((ImVec4)ImColor(20, 20, 255), "Name");

	ImGui::SameLine(140.9f);

	ImGui::TextColored((ImVec4)ImColor(255, 20, 20), "Value");

	ImGui::SameLine(267.9f);

	ImGui::TextColored((ImVec4)ImColor(255, 255, 20), "Flags");

	PushW(121.1f);

	Push();

	ImGui::InputText("", name, IM_ARRAYSIZE(name));

	Pop();

	ImGui::SameLine();

	Push();

	ImGui::InputFloat("", &Value);

	Pop();

	ImGui::SameLine();

	Push();

	ImGui::InputInt("", &Flags);

	Pop();

	ImGui::SameLine();

	Push();

	if (ImGui::Button("Apply", ImVec2(152.8f, 22)))
	{
		if (name && name[0])
		{
			if (auto CV(Cvar->FindVar(name)); CV)
				CV->SetValueAndFlags(Value, Flags);
			else
				ColoredConMsg(
					Color(20, 255, 20, 255),
					"ConVar %s is not found.\n",
					name);
		}
	}

	Pop();

	ImGui::PopItemWidth();

	ImGui::Separator();

	ImGui::Separator();

	PushW(186.5f);

	static int Command_Flag{ 0 };

	static char Command_name[64]{};

	ImGui::TextColored((ImVec4)ImColor(20, 50, 50), "Set Command Flags");

	ImGui::TextColored((ImVec4)ImColor(20, 255, 20), "Name");

	ImGui::SameLine(207.f);

	ImGui::TextColored((ImVec4)ImColor(255, 20, 255), "Flags");

	Push();

	ImGui::InputText("", Command_name, IM_ARRAYSIZE(Command_name));

	Pop();

	ImGui::SameLine();

	Push();

	ImGui::InputInt("", &Command_Flag);

	Pop();

	ImGui::SameLine();

	Push();

	if (ImGui::Button("Apply", ImVec2(152.8f, 22)))
	{
		if (Command_name && Command_name[0])
		{
			if (auto Command_Ptr = Cvar->FindCommand(Command_name); Command_Ptr)
				Command_Ptr->SetFlags(Command_Flag);
			else
				ColoredConMsg(
					Color(20, 255, 20, 255),
					"Command %s is not found.\n",
					Command_name);

		}
	}

	Pop();

	PopW();

	ImGui::Separator();

	ImGui::Separator();

	ImGui::TextColored(ImColor(20, 255, 20, 255), "Desired Console Variable Data Response");
	{
		static char Variable_Name[64], Value[64]; static int Status = 0;

		ImGui::TextColored((ImVec4)ImColor(20, 20, 255), "Name");

		ImGui::SameLine(158.8f);

		ImGui::TextColored((ImVec4)ImColor(255, 20, 20), "Value");

		ImGui::SameLine(301.6f);

		ImGui::TextColored((ImVec4)ImColor(255, 255, 20), "Existence");

		ImGui::PushItemWidth(138);
		Push();
		ImGui::InputText("", Variable_Name, IM_ARRAYSIZE(Variable_Name));
		Pop();
		ImGui::SameLine();
		Push();
		ImGui::InputText("", Value, IM_ARRAYSIZE(Value));
		Pop();
		ImGui::SameLine();
		Push();
		ImGui::Combo("", &Status, "Yes\0No\0");
		Pop();
		ImGui::SameLine();

		if (ImGui::Button("Add", ImVec2(103.3f, 22)))
		{
			if (Cvar->FindVar(Variable_Name) || Cvar->FindCommand(Variable_Name))
			{
				for (int i = 0; i < DesiredConVarsValue.size(); i++)
				{
					auto& CCOVar = DesiredConVarsValue.at(i);

					if (strcmp(CCOVar.Name, Variable_Name) == 0)
					{
						CCOVar.Update(Value);

						CCOVar.Status = Status;

						memset(Variable_Name, 0, IM_ARRAYSIZE(Variable_Name));

						memset(Value, 0, IM_ARRAYSIZE(Value));

						Status = 0;

						goto Label;
					}
				}

				DesiredConVarsValue.push_back({ Variable_Name, Value, Status });

				memset(Variable_Name, 0, IM_ARRAYSIZE(Variable_Name));

				memset(Value, 0, IM_ARRAYSIZE(Value));

				Status = 0;
			}
			else
			{
				ColoredConMsg(Color(RandomInt(10, 200), RandomInt(10, 200), 255), "Cvar %s was not found\n", Variable_Name);
			}
		}

	Label:

		if (DesiredConVarsValue.size())
		{
			ImGui::TextColored(ImColor(20, 20, 255), "Added ConVars:");

			for (int i = 0; i < DesiredConVarsValue.size(); i++)
			{
				auto&& pCvar = DesiredConVarsValue[i];

				ImGui::Text("%s -> Prescribed Value: %s Existance: %s\n", pCvar.Name, pCvar.Value, pCvar.Status ? "No" : "Yes");

				ImGui::SameLine(438);

				ImGui::PushID(__LINE__ + i);

				if (ImGui::Button("Remove", ImVec2(60, 22)))
				{
					DesiredConVarsValue.erase(DesiredConVarsValue.begin() + i);
				}

				Pop();
			}
		}

		ImGui::PopItemWidth();
	}

	auto UnlockConVars = [](int flagged_with)
	{
		ConCommandBase* Base;

		for (Base = Cvar->GetCommands(); Base; Base = Base->GetNext())
		{
			if (Base->IsFlagSet(flagged_with))
			{
				Base->RemoveFlags(flagged_with);

				ConMsg("Convar %s has been unlocked\n", Base->GetName());
			}
		}
	};

	Push() {

		static int Temp = 0;

		ImGui::TextColored(ImColor(255, 255, 20), "Unlock ConVars Flagged With: ");

		ImGui::SameLine();

		ImGui::Combo("", &Temp, FlagsName.data(), FlagsName.size());

		ImGui::SameLine();

		if (ImGui::Button("Unlock"))
		{
			UnlockConVars(FlagsValue[Temp]);
		}
	}

	Pop();
}

void Menu::Menu() noexcept
{
	ImGui::PushItemWidth(200.f);

	ImGui::TextUnformatted("Toggle Key");

	ImGui::SameLine();

	HotKey(Menu::Get.Menu.Key);

	ImGui::Combo("Font", &Menu::Get.Menu.FontID, MenuFont.data(), MenuFont.size());

	ImGui::SameLine(292.5f);

	if (auto Font = ImGui::GetIO().Fonts->Fonts[Menu::Get.Menu.FontID]; Font)
	{
		ImGui::SliderFloat("Font Size", &Font->FontSize, 1, 20.f);
	}

	ImGui::PopItemWidth();

	Push();

	ImGui::BeginChild("", ImVec2{ 538.5f , 585.2f });
	{
		ImGuiStyle& style = ImGui::GetStyle();
		for (int i = 0; i < ImGuiCol_COUNT; i++) {
			if (i && i % 2) ImGui::SameLine(292.5f);

			ColorPopupV2((std::array<float, 4>&)style.Colors[i], ImGui::GetStyleColorName(i), __LINE__ + i);
		}

	}ImGui::EndChild();

	Pop();
}


void Menu::RenderTabs() noexcept
{
	static float flRainbow;

	float flSpeed = 0.0003f;

	int curWidth = 1, y, size;

	ImVec2 curPos = ImGui::GetCursorPos();

	ImVec2 curWindowPos = ImGui::GetWindowPos();

	curPos.x += curWindowPos.x;

	curPos.y += curWindowPos.y;

	Engine->GetScreenSize(y, size);

	DrawRectRainbow(curPos.x - 10, curPos.y - 5, ImGui::GetWindowSize().x + size, curPos.y + -4, flSpeed, flRainbow);

	ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[0]);

#define PushF() ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[Menu::Get.Menu.FontID])

#define PopF()  ImGui::PopFont();

	if (ImGui::BeginTabBar("TabBar", ImGuiTabBarFlags_Reorderable | ImGuiTabBarFlags_FittingPolicyScroll))
	{
		if (ImGui::BeginTabItem("Zombie Escape"))
		{
			PushF(); ZE(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Aimbot"))
		{
			PushF(); Aimbot(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Triggerbot"))
		{
			PushF(); TriggerBot(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Glow"))
		{
			PushF(); RenderGlowTab(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Chams"))
		{
			PushF(); RenderChamsTab(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Visuals"))
		{
			PushF(); Visuals(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Miscellaneous"))
		{
			PushF(); Miscellaneous(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("ConVars"))
		{
			PushF(); ConVars(); PopF();

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Menu"))
		{
			PushF(); Menu(); PopF();

			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}

	ImGui::PopFont();
}

void Menu::Render() noexcept
{
	if (Menu::Get.Menu.On)
	{
		ImGui_ImplDX9_NewFrame();

		ImGui_ImplWin32_NewFrame();

		ImGui::NewFrame();

		//ImGui::SetNextWindowSize({ 510.f , 0.0f });

		ImGui::Begin("ZE Elite", nullptr, windowFlags);
		{
			RenderTabs();
		}

		ImGui::End();

		ImGui::EndFrame();

		ImGui::Render();

		ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
	}
}

void Menu::AssignVariables(bool IgnoreMenu) noexcept
{
	constexpr auto& Aimbot = Menu::Get.Aimbot;
	{
		Aimbot.LAC_Bypass.Init(false);
		Aimbot.Ragebot.Enabled.Init(false);
		Aimbot.Ragebot.AutoStop.Init(false);
		Aimbot.Ragebot.Predicted.Init(false);
		Aimbot.Ragebot.AutoShoot.Init(false);
		Aimbot.Ragebot.AutoCrouch.Init(false);
		Aimbot.Ragebot.FriendlyFire.Init(false);

		Aimbot.Ragebot.TargetSelection.Init(0);
		Aimbot.Ragebot.SmoothVal.Init(1.f);
		Aimbot.Ragebot.Timeout.Init(0);
		Aimbot.Ragebot.HitBox.Init(12);
		Aimbot.Ragebot.Silent.Init(0);
		Aimbot.Ragebot.Usage.Init(0);
		Aimbot.Ragebot.FOV.Init(180);
	}

	constexpr auto& General = Menu::Get.General;
	{
		General.ZeStuff.Filter.Init(2);
		General.ZeStuff.AutoLaser.Init(false);
		General.ZeStuff.Wireframe_lasers.Init(false);
		General.ZeStuff.Special_items_IgnoreZ.Init(false);

		General.Panic.Init(false);
		General.Unload.Init(false);
	}

	constexpr auto& Visuals = Menu::Get.Visuals;
	{
		Visuals.WorldTexture.Init(0);
		Visuals.ZE_LasersTexture.Init(0);
		Visuals.BackgroundTexture.Init(0);

		Visuals.Box.Init(false);
		Visuals.Name.Init(false);
		Visuals.Health.Init(false);
		Visuals.Weapon.Init(false);
		Visuals.Skeleton.Init(false);
		Visuals.SnapLine.Init(false);

		Visuals.BoxType.Init(0);
		Visuals.HealthType.Init(0);
		Visuals.NameFont.Init(0);
		Visuals.HealthFont.Init(0);
		Visuals.WeaponFont.Init(0);
		Visuals.HP_SpriteScale.Init(0.10f);
		Visuals.WeaponRenderingMode.Init(1);

		Visuals.KillEffect_Type.Init(0);
		Visuals.ScopeLen.Init(0);

		Visuals.SkyBoxIndex.Init(0);
		Visuals.NightVision.Init(0);
		Visuals.Hands.Init(0);
		Visuals.NightMode.Init(1);
		Visuals.MuzzleFlash.Init(0);
		Visuals.FieldOfView.Init(0);
		Visuals.ViewModelFov.Init(0);
		Visuals.FlashPercentage.Init(100);
		Visuals.DLights.Init(0);

		Visuals.FullBright.Init(false);
		Visuals.NoFog.Init(false);
		Visuals.Disable_Fire_Particles.Init(false);
		Visuals.Disable_Players_MuzzleFlash.Init(false);
		Visuals.NoFlashLight.Init(false);
		Visuals.NoSmoke.Init(false);
		Visuals.NadeTracer.Init(false);
		Visuals.NoVisualRecoil.Init(false);

		constexpr auto& Glow = Menu::Get.Visuals.Glow;
		{
			Glow.Enabled.Init(false);
		}

		constexpr auto& FOG = Menu::Get.Visuals.FOG;
		{
			FOG.Enabled.Init(false);

			FOG.end.Init(-1);
			FOG.start.Init(-1);
			FOG.maxdensity.Init(0.7f);
		}

		constexpr auto& Trails = Menu::Get.Visuals.Trails;
		{
			Trails.Enabled.Init(false);

			Trails.Width.Init(15.f);
			Trails.RenderMode.Init(5);
			Trails.EndWidth.Init(15.f);
			Trails.LifeTime.Init(0.65f);
			Trails.SpriteTexture.Init(0);
		}

		constexpr auto& Tracers = Menu::Get.Visuals.Tracers;
		{
			Tracers.Enabled.Init(false);

			Tracers.SpriteTexture.Init(0);
			Tracers.PointScale.Init(0.03f);
			Tracers.m_flLife.Init(0.8f);
			Tracers.m_flWidth.Init(0.7f);
			Tracers.m_flEndWidth.Init(0.5f);
			Tracers.m_flFadeLength.Init(55.f);
			Tracers.m_flAmplitude.Init(0.4f);
			Tracers.m_flSpeed.Init(0.2f);
			Tracers.m_nSegments.Init(4);
			Tracers.m_nFlags.Init(0);
		}

		constexpr auto& Weather = Menu::Get.Visuals.Weather;
		{
			Weather.Enabled.Init(false);

			Weather.Type.Init(1);
			Weather.Width.Init(3.f);
			Weather.Speed.Init(1000.f);
			Weather.Radius.Init(1000.f);
			Weather.Length.Init(0.1f);
		}

		constexpr auto& Debug = Menu::Get.Visuals.Debug;
		{
			Debug.Enabled.Init(false);

			Debug.INDEX.Init(false);
			Debug.Classname.Init(false);
			Debug.ModelName.Init(false);
			Debug.ModelIndex.Init(false);
			Debug.Velocity.Init(false);
			Debug.Origin.Init(false);
			Debug.Filter.Init(false);

			Debug.MaxDistance.Init(1500);
		}

		constexpr auto& Hitmarker = Menu::Get.Visuals.Hitmarker;
		{
			Hitmarker.Enabled.Init(false);

			Hitmarker.Time.Init(930);
			Hitmarker.Length.Init(7);
			Hitmarker.Gap.Init(2);
			Hitmarker.HitSound.Init(0);
			Hitmarker.Mode.Init(0);
			Hitmarker.Overlay.Init(0);
		}

		constexpr auto& DamageIndicator = Menu::Get.Visuals.DamageIndicator;
		{
			DamageIndicator.Enabled.Init(false);
		}

		constexpr auto& Chams = Menu::Get.Visuals.Chams;
		{
			constexpr auto& Player = Chams.Player;
			{
				Player.Enabled.Init(false);

				Player.Wireframe.Init(false);
				Player.Ignorez.Init(false);

				Player.Type.Init(1);
			}

			constexpr auto& Weapon = Chams.Weapon;
			{
				Weapon.Enabled.Init(false);

				Weapon.Wireframe.Init(false);
				Weapon.Ignorez.Init(false);

				Weapon.Type.Init(1);
			}
		}

		constexpr auto& ViewModel = Menu::Get.Visuals.ViewModel;
		{
			ViewModel.Enabled.Init(false);

			ViewModel.Origin.Init(0);
			ViewModel.Angles.Init(0);
		}

		constexpr auto& Crosshair = Menu::Get.Visuals.Crosshair;
		{
			Crosshair.Recoil.Init(false);

			constexpr auto& X_Shape = Crosshair.X_Shape;
			{
				X_Shape.Enabled.Init(false);

				X_Shape.Length.Init(10);
				X_Shape.Gap.Init(3);
			}

			constexpr auto& Plus_Shape = Crosshair.Plus_Shape;
			{
				Plus_Shape.Enabled.Init(false);

				Plus_Shape.Length.Init(10);
				Plus_Shape.Gap.Init(3);
			}

			constexpr auto& Circle_Shape = Crosshair.Circle_Shape;
			{
				Circle_Shape.Enabled.Init(false);

				Circle_Shape.Radius.Init(10);
				Circle_Shape.Segments.Init(10);
			}
		}
	}

	constexpr auto& Misc = Menu::Get.Misc;
	{
		Misc.NoMotd.Init(false);
		Misc.NoDrug.Init(false);
		Misc.NoShake.Init(false);
		Misc.MuteRadio.Init(false);
		Misc.NoRecoil.Init(false);
		Misc.NoSpread.Init(false);
		Misc.AutoPisol.Init(false);
		Misc.Faststop.Init(false);
		Misc.Disable_Game_Console_Warnings.Init(false);
		Misc.Fastrun.Init(false);
		Misc.AutoKevlar.Init(false);
		Misc.Bunnyhop.Init(false);
		Misc.Autostrafe.Init(false);
		Misc.CircleStrafe.Init(false);
		Misc.Edgejump.Init(false);
		Misc.FastLadder.Init(false);
		Misc.AntiAFKkick.Init(false);
		Misc.Sv_Pure_Bypass.Init(false);
		Misc.KillMessage.Init(false);

		Misc.Ragdoll_Force.Init(100);
		Misc.m_vecCameraOffset.Init(0);
		Misc.Bunnyhop_Perfect_Rate.Init(100);
		
		constexpr auto& StrafeOptimizer = Misc.StrafeOptimizer;
		{
			StrafeOptimizer.Enabled.Init(false);

			StrafeOptimizer.Desired_Gain.Init(0);
			StrafeOptimizer.Required_Speed.Init(0);
			StrafeOptimizer.Greatest_Possible_Strafe_Angle.Init(0);
		}

		constexpr auto& Triggerbot = Misc.Triggerbot;
		{
			Triggerbot.Enabled.Init(false);

			Triggerbot.Filter.Init(false);

			Triggerbot.Usage.Init(0);
		}

		constexpr auto& KnifeBot = Misc.KnifeBot;
		{
			KnifeBot.Enabled.Init(false);

			KnifeBot.Method.Init(0);
			KnifeBot.Usage.Init(1);
		}

		constexpr auto& ClanTag = Misc.ClanTag;
		{
			ClanTag.Changer.Init(false);
			ClanTag.Stealer.Init(false);

			ClanTag.Delay.Init(5.f);
		}

		constexpr auto& Sounds = Misc.Sounds;
		{
			Sounds.MusicVolume.Init(50);
			Sounds.WeaponsAudio.Init(50);
			Sounds.FootStepsVolume.Init(50);
		}

		constexpr auto& SteamIDSpoofer = Misc.SteamIDSpoofer;
		{
			SteamIDSpoofer.Enabled.Init(false);

			SteamIDSpoofer.Random.Init(false);

			SteamIDSpoofer.SteamID.Init(0);
		}

		if(IgnoreMenu == 0)
			Menu::Get.Menu.On.Init(false);

		Menu::Get.Menu.FontID.Init(0);

		Menu::Get.Menu.Key.Init(VK_F11);
	}

	constexpr auto& Colors = Menu::Get.Colors;
	{
		constexpr auto& PlayerEsp = Menu::Get.Colors.PlayerEsp;
		{
			PlayerEsp.Box = { Color::Blue(), Color::Red(), Color::White() };

			PlayerEsp.Name = { Color::White(), Color::White(), Color::White() };

			PlayerEsp.SnapLine = { Color::SkyBlue(), Color::Blue(), Color::White() };

			PlayerEsp.Health = { Color::Green(), Color::Yellow() };

			PlayerEsp.Weapon = { Color::Silver(), Color::Silver() };

			PlayerEsp.Skeleton = { Color::GoldenRod(), Color::Firebrick() };

			PlayerEsp.DLights = Color{ 0 , 40, 255 };
		}

		constexpr auto& Chams = Menu::Get.Colors.Chams;
		{
			Chams.Player = Chams.Weapon = Color(0, 0, 0, 255);
		}

		constexpr auto& Glow = Menu::Get.Colors.Glow;
		{
			Glow.Player = { Color::White(), Color::White() };
		}

		constexpr auto& General = Menu::Get.Colors.General;
		{
			General.SkyBox = Color::White();
			General.World = Color::White();
			General.DamageIndicator = Color::White();
			General.Nightvision = Color(0, 123, 255, 0);
			General.NadeTracer = Color(0, 123, 255, 0);
			General.BulletImpact = Color(255, 0, 0, 0);

			constexpr auto& Crosshair = Menu::Get.Colors.General.Crosshair;
			{
				Crosshair.X_Shape = Color(50, 255, 50, 200);
				Crosshair.Plus_Shape = Color(50, 255, 50, 200);
				Crosshair.Circle_Shape = Color(50, 255, 50, 200);
			}

			General.Hitmarker = Color::Yellow();
			General.Tracers = Color(135, 206, 235, 255);
			General.Fog = Color(135, 206, 235, 255);
			General.Trails = Color::White();
			General.HudColor = Color::SkyBlue();
			General.Net_graph = Color::White();
		}
	}

	constexpr auto& Keys = Menu::Get.Keys;
	{
		Keys.RageBot.Init(0);
		Keys.AirStuck.Init(0);
		Keys.Edgejump.Init(0);
		Keys.Bunnyhop.Init(0);
		Keys.Triggerbot.Init(0);
		Keys.Autostrafe.Init(0);
		Keys.CircleStrafe.Init(0);
		Keys.Unload.Init(VK_END);
		Keys.PanicKey.Init(VK_PAUSE);
		Keys.KnifeBot.Init(0);
	}
}