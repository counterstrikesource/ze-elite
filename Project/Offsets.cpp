#include "Vector.h"
#include "Offsets.h"
#include "SDK.h"

void Offsets()
{
	// Addresses

	GetDebugNameA = Tools::FindPattern("client.dll", "53 8B D9 C6");

	GetEffectName = Tools::FindPattern("client.dll", "8B 49 40 85 C9 74 0B");

	RemoveA = Tools::FindPattern("client.dll", "56 8B F1 83 BE ? ? ? ? ? 7E 1A");

	WpnDataA = Tools::FindPattern("client.dll", "0F B7 81 ? ? ? ? 50 E8 ? ? ? ? 83 C4 04 C3");

	SwingOrStab = Tools::FindPattern("client.dll", "80 7B 08 00 B8 ? ? ? ? BE ? ? ? ?") - 0x3B;

	CreateEntityByName = Tools::FindPattern("client.dll", "8B EC E8 ? ? ? ? FF 75 08 8B C8") - 1;

	LoadSkyBox = Tools::FindPattern("engine.dll", "55 8B EC 81 EC ? ? ? ? 8B 0D ? ? ? ? 53 56 57 8B 01");

	SetParentA = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 83 66 7C FE"));

	Audio_GetWaveDuration = Tools::FindPattern("engine.dll", "55 8B EC 81 EC ? ? ? ? 56 8B 75 08 8D 4D D4");

	GetGroundEntity = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? C6 45 FF 00"));

	GetOwnerEntityA = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 85 C0 75 EB"));

	UTIL_TraceLine = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 83 C4 18 8B 06"));

	SetModelByIndexA = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 83 7D 08 00 5B"));

	UpdateButtonStateA = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 8B 17 8D 43 0C"));

	EstimateAbsVelocityA = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? D9 45 F4 DC C8"));

	Voice_RecordStart = Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "E8 ? ? ? ? 83 C4 0C 5F 5E C3"));

	GetBulletTypeParameters = Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? F3 0F 10 43 ? 33 FF"));

	// ----------------------------------------------------------------------------------------------- \\

	MoveTypeA = *(uintptr_t*)(Tools::FindPattern("client.dll", "80 BE ? ? ? ? ? 75 2D") + 2);

	CreationTickA = *(uintptr_t*)(Tools::FindPattern("client.dll", "B8 ? ? ? ? 57 56") + 0x16);

	AddFifteen = *(std::float_t**)(Tools::FindPattern("client.dll", "74 11 F3 0F 58 05 ? ? ? ?") + 6);

	overlaycolor = (void*)(Tools::FindPattern("client.dll", "8B 01 FF 90 ? ? ? ? 83 F8 50 7C 1E") - 4);

	NightVisionAlphaA = *(uintptr_t*)(Tools::FindPattern("client.dll", "F3 0F 10 86 ? ? ? ? 74 11") + 4);

	g_pMoveData = *reinterpret_cast<CMoveData**>(Tools::FindPattern("client.dll", "FF 35 ? ? ? ? 57") + 2);

	LocalPlayerA = *reinterpret_cast<uintptr_t*>(Tools::FindPattern("client.dll", "39 05 ? ? ? ? 75 0B") + 2);

	m_pPredictionPlayer = *reinterpret_cast<CBaseEntity***>(Tools::FindPattern("client.dll", "89 3D ? ? ? ? F3 0F 2A 87") + 2);

	m_nPredictionRandomSeed = *reinterpret_cast<int**>(Tools::FindPattern("client.dll", "A3 ? ? ? ? 5D C3 55 8B EC 8B 45 08") + 1);

	// Materials

	HandsMat = MatSystemOther->FindMaterial("models/weapons/v_models/hands/v_hands", NULL);

	blueblacklargebeam = { MaterialActions::Update, MatSystemOther->FindMaterial("effects/blueblacklargebeam", NULL) };

	ItemsMat = { MaterialActions::Update, MatSystemOther->FindMaterial("particle/particle_glow_05_add_15ob_minsize", NULL) };

	// Byte Patching

	if (AddFifteen) {

		unsigned long Protection_Backup{ 0 };

		VirtualProtect(AddFifteen, 4, PAGE_EXECUTE_READWRITE, &Protection_Backup);

		*AddFifteen = 0;

		VirtualProtect(AddFifteen, 4, Protection_Backup, nullptr);
	}

	// ConVars

	fog_color = Cvar->FindVar("fog_color"); assert(fog_color);
	Fog_Enable = Cvar->FindVar("Fog_Enable"); assert(Fog_Enable);
	Fog_Override = Cvar->FindVar("Fog_Override"); assert(Fog_Override);
	fog_maxdensity = Cvar->FindVar("fog_maxdensity"); assert(fog_maxdensity);


	r_rainspeed = Cvar->FindVar("r_rainspeed"); assert(r_rainspeed);
	r_rainwidth = Cvar->FindVar("r_rainwidth"); assert(r_rainwidth);
	r_RainRadius = Cvar->FindVar("r_RainRadius"); assert(r_RainRadius);
	r_rainlength = Cvar->FindVar("r_rainlength"); assert(r_rainlength);
	
	sv_cheats = Cvar->FindVar("sv_cheats"); assert(sv_cheats);

	sv_skyname = Cvar->FindVar("sv_skyname"); assert(sv_skyname);

	CCCrosshair = Cvar->FindVar("crosshair"); assert(CCCrosshair);

	mat_fullbright = Cvar->FindVar("mat_fullbright"); assert(mat_fullbright);

	snd_musicvolume = Cvar->FindVar("snd_musicvolume"); assert(snd_musicvolume);

	cl_downloadfilter = Cvar->FindVar("cl_downloadfilter"); assert(cl_downloadfilter);
}

pMaterial HandsMat;

PVOID overlaycolor;

CMoveData* g_pMoveData;

std::float_t* AddFifteen;

int* m_nPredictionRandomSeed;

CBaseEntity** m_pPredictionPlayer;

std::pair<MaterialActions, pMaterial> blueblacklargebeam, ItemsMat;

pConVar
r_rainlength,
r_RainRadius,
r_rainspeed,
r_rainwidth,
snd_musicvolume,
sv_skyname,
Fog_Enable,
Fog_Override,
fog_color,
fog_maxdensity,
CCCrosshair,
cl_downloadfilter,
mat_fullbright,
sv_cheats;

uintptr_t
LoadSkyBox,
SwingOrStab,
GetEffectName,
UTIL_TraceLine,
GetGroundEntity,
Voice_RecordStart,
CreateEntityByName,
Audio_GetWaveDuration,
GetBulletTypeParameters,

//\\
CCSPlayer.cpp
//\\

RemoveA,
WpnDataA,
MoveTypeA,
SetParentA,
LocalPlayerA,
GetDebugNameA,
CreationTickA,
GetOwnerEntityA,
SetModelByIndexA,
NightVisionAlphaA,
UpdateButtonStateA,
EstimateAbsVelocityA;