#pragma once

#ifndef CAutoLaser_H
#define CAutoLaser_H

class CBaseEntity;
class CUserCmd;
class Entities;

namespace AutoLaser
{
	void Run(CBaseEntity*, CUserCmd*, bool&) noexcept;
}

#endif
