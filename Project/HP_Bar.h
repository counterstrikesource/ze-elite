#ifndef HP_Bar_H
#define HP_Bar_H

class CSprite;
class CBaseEntity;

namespace HP_Bar
{
	void UpdateSpritesScale() noexcept;

	void Process(CBaseEntity* pLocal) noexcept;

	bool IsLocalEntity(CSprite* pEntity) noexcept;

	void ClearSprites(bool Remove = true) noexcept;

	void CreateSpriteForEntity(CBaseEntity* pEntity, bool IsEnemy) noexcept;

	void UpdateSpriteModelIndex(std::pair<CSprite*, int> Sprite, int Health) noexcept;


	extern std::array<std::pair<CSprite*, int>, 65> Entities;
}

#endif
