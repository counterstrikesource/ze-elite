#pragma once

class CBaseEntity;
class CMoveData;

class IGameMovement
{
public:

	void ProcessMovement(CBaseEntity* pPlayer, CMoveData* pMove)
	{
		using Type = void(__thiscall*)(void*, void*, void*);

		GetVirtualMethod<Type>(this, 1)(this, pPlayer, pMove);
	}

	void StartTrackPredictionErrors(CBaseEntity* pPlayer)
	{
		using Type = void(__thiscall*)(void*, void*);

		GetVirtualMethod<Type>(this, 2)(this, pPlayer);
	}

	void FinishTrackPredictionErrors(CBaseEntity* pPlayer)
	{
		using Type = void(__thiscall*)(void*, void*);

		GetVirtualMethod<Type>(this, 3)(this, pPlayer);
	}
};