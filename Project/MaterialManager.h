#ifndef CMat_H
#define CMat_H

class IMaterial;
class Color;


typedef IMaterial* Matptr;

class CMat
{
public:
	Matptr Chams[4];

	void Initialize();

	void ResetMaterial();
};

extern CMat gMat;

void ForceMaterial(Color Color, Matptr Material, bool ForceColor = true, bool ForceMat = true);

#endif
