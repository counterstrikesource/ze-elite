#pragma once

#ifndef Esp_H
#define Esp_H

#include "Vector.h"

struct DynamicBoundingBox;
struct BoundingBox;
struct ImDrawList;
class CBaseEntity;

typedef unsigned long HFont;

extern bool ScreenTransform(const Vector& point, Vector& screen) noexcept;

extern bool WorldToScreen(const Vector& origin, Vector& screen) noexcept;

namespace Esp
{
	void Render() noexcept;

	void Initialize() noexcept;

	void SnapLine(CBaseEntity*, int) noexcept;

	void Skeleton(CBaseEntity*, bool) noexcept;

	void Box(DynamicBoundingBox, int) noexcept;

	void Debug(CBaseEntity*, CBaseEntity*) noexcept;

	void Name(BoundingBox, CBaseEntity*, int) noexcept;

	void Health(BoundingBox, CBaseEntity*, bool) noexcept;

	void Weapon(const Vector&, CBaseEntity*, bool) noexcept;

	extern HFont courier_new, Counter_Strike_Logo, Others[5];



	extern bool Fonts_Were_Initialized;
}

struct DamageIndicator_t {
	int iDamage;
	float flEraseTime;
};

namespace DamageIndicator
{
	void Paint(CBaseEntity* pLocal);

	extern DamageIndicator_t data;
}

#endif
