#pragma once

enum class MaterialActions;

class IBaseFileSystem;
class CCSWeaponInfo;
class CBaseEntity;
class KeyValues;
class IMaterial;
class CMoveData;
class ConVar;

using pConVar = ConVar*;
using pMaterial = IMaterial*;

void Offsets();

extern pMaterial HandsMat;

extern PVOID overlaycolor;

extern CMoveData* g_pMoveData;

extern std::float_t* AddFifteen;

extern int* m_nPredictionRandomSeed;

extern CBaseEntity** m_pPredictionPlayer;

extern std::pair<MaterialActions, pMaterial> blueblacklargebeam, ItemsMat;

extern pConVar 
r_rainlength,
r_RainRadius,
r_rainspeed,
r_rainwidth,
snd_musicvolume,
sv_skyname,
Fog_Enable,
Fog_Override,
fog_color,
fog_maxdensity,
CCCrosshair,
cl_downloadfilter,
mat_fullbright,
sv_cheats;

extern 
uintptr_t 
LoadSkyBox,
SwingOrStab,
GetEffectName,
UTIL_TraceLine,
GetGroundEntity,
Voice_RecordStart,
CreateEntityByName,
Audio_GetWaveDuration,
GetBulletTypeParameters,

//\\
CCSPlayer.cpp
//\\

RemoveA,
WpnDataA,
MoveTypeA,
SetParentA,
LocalPlayerA,
GetDebugNameA,
CreationTickA,
GetOwnerEntityA,
SetModelByIndexA,
NightVisionAlphaA,
UpdateButtonStateA,
EstimateAbsVelocityA;