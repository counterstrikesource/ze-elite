#include "SDK.h"
#include "MaterialManager.h"

CMat gMat;

inline constexpr std::array CMaterials =
{
	"NormalCh",
	"FlatCh",
	"debug/debugambientcube",
	"WaterCh"
};

void CMat::Initialize()
{
	for (auto i = 0; i < 4; i++) {

		auto& pMaterial = Chams[i];

		if (pMaterial = MatSystemOther->FindMaterial(CMaterials[i], 0); pMaterial) {

			pMaterial->AddRef();
		}
	}
}

void ForceMaterial(Color color, Matptr material, bool useColor, bool forceMaterial)
{
	if (useColor)
	{
		float blend[3] = { (float)color[0] / 255.f, (float)color[1] / 255.f, (float)color[2] / 255.f };
		float alpha = (float)color[3] / 255.f;

		if (RenderView->GetBlend() != alpha)
		{
			RenderView->SetBlend(alpha);
		}

		float ComparationColor[3];

		RenderView->GetColorModulation(ComparationColor);

		if (ComparationColor != blend)
		{
			RenderView->SetColorModulation(blend);
		}
	}

	if (forceMaterial)
		MdlRender->ForcedMaterialOverride(material);
}

void CMat::ResetMaterial()
{
	static const float flDefault[3] = { 1, 1, 1 };
	RenderView->SetBlend(1);
	RenderView->SetColorModulation(flDefault);
	MdlRender->ForcedMaterialOverride(nullptr);
}
