#include "SDK.h"
#include "WorldTextures.h"

constexpr float DefaultColor = 1.f;

MaterialActions World_Textures::NightModeModulateAction = MaterialActions::Update;
MaterialActions World_Textures::WorldColorModulateAction = MaterialActions::Update;
MaterialActions World_Textures::SkyBoxColorModulateAction = MaterialActions::Update;

auto World_Textures::NightModeModulate() noexcept -> void
{
	if (NightModeModulateAction == MaterialActions::None) return;

	float NightMod_Value = (100.f - float(Menu::Get.Visuals.NightMode)) / 100.f;

	for (auto i = MatSystemOther->FirstMaterial(); i != MatSystemOther->InvalidMaterial(); i = MatSystemOther->NextMaterial(i)) {

		auto pMaterial = MatSystemOther->GetMaterial(i);

		if (!pMaterial || !pMaterial->IsPrecached())
			continue;

		if (_strcmpi(pMaterial->GetTextureGroupName(), TEXTURE_GROUP_WORLD) == NULL) {

			pMaterial->ColorModulate(NightMod_Value, NightMod_Value, NightMod_Value);
		}
	}

	for (auto pMaterial : World.Textures) {

		if (pMaterial && pMaterial->IsPrecached()) {

			pMaterial->ColorModulate
			(
				NightMod_Value,
				NightMod_Value,
				NightMod_Value
			);
		}
	}

	NightModeModulateAction = MaterialActions::None;
}

auto World_Textures::WorldColorModulate() noexcept -> void
{
	if (WorldColorModulateAction == MaterialActions::None) return;

	static std::array<float, 4> WorldCurrentColor{};

	((Color&)Menu::Get.Colors.General.World.Get()).GetColor(WorldCurrentColor, 255.f);

	for (auto i = MatSystemOther->FirstMaterial(); i != MatSystemOther->InvalidMaterial(); i = MatSystemOther->NextMaterial(i)) {

		auto pMaterial = MatSystemOther->GetMaterial(i);

		if (!pMaterial || !pMaterial->IsPrecached())
			continue;

		if (_strcmpi(pMaterial->GetTextureGroupName(), TEXTURE_GROUP_WORLD) == NULL) {

			if (WorldColorModulateAction == MaterialActions::Unload)
			{
				pMaterial->ColorModulate(DefaultColor, DefaultColor, DefaultColor); pMaterial->AlphaModulate(DefaultColor);
			}
			else
			{
				pMaterial->ColorModulate(WorldCurrentColor[0], WorldCurrentColor[1], WorldCurrentColor[2]); pMaterial->AlphaModulate(WorldCurrentColor[3]);
			}
		}
	}

	for (auto pMaterial : World.Textures)
	{
		if (pMaterial && pMaterial->IsPrecached())
		{
			if (WorldColorModulateAction == MaterialActions::Unload)
			{
				pMaterial->ColorModulate(DefaultColor, DefaultColor, DefaultColor); pMaterial->AlphaModulate(DefaultColor);
			}
			else
			{
				pMaterial->ColorModulate(WorldCurrentColor[0], WorldCurrentColor[1], WorldCurrentColor[2]); pMaterial->AlphaModulate(WorldCurrentColor[3]);
			}
		}
	}

	WorldColorModulateAction = MaterialActions::None;
}

auto World_Textures::SkyBoxColorModulate() noexcept -> void
{
	if (SkyBoxColorModulateAction == MaterialActions::None) return;

	static std::array<float, 4> SkyBoxCurrentColor{};

	((Color&)Menu::Get.Colors.General.SkyBox.Get()).GetColor(SkyBoxCurrentColor, 255.f);

	for (auto i = MatSystemOther->FirstMaterial(); i != MatSystemOther->InvalidMaterial(); i = MatSystemOther->NextMaterial(i)) {

		auto pMaterial = MatSystemOther->GetMaterial(i);

		if (!pMaterial || !pMaterial->IsPrecached())
			continue;

		if (_strcmpi(pMaterial->GetTextureGroupName(), TEXTURE_GROUP_SKYBOX) == NULL) {

			if (SkyBoxColorModulateAction == MaterialActions::Unload)
			{
				pMaterial->ColorModulate(DefaultColor, DefaultColor, DefaultColor); pMaterial->AlphaModulate(DefaultColor);
			}
			else
			{
				pMaterial->ColorModulate(SkyBoxCurrentColor[0], SkyBoxCurrentColor[1], SkyBoxCurrentColor[2]); pMaterial->AlphaModulate(SkyBoxCurrentColor[3]);
			}
		}
	}

	SkyBoxColorModulateAction = MaterialActions::None;
}

auto SingleMaterialModulate(std::pair<MaterialActions, pMaterial>& Material, MaterialVarFlags flag, const bool& MenuVar) noexcept -> void
{
	auto& [Action, pMaterial] = Material;

	if (Action == MaterialActions::None || pMaterial == NULL)
		return;

	if (Action == MaterialActions::Update)
	{
		pMaterial->SetMaterialVarFlag(flag, MenuVar);
	}
	else
	{
		pMaterial->SetMaterialVarFlag(flag, false);
	}

	Action = MaterialActions::None;
}
