#pragma once

#ifndef MENU
#define MENU

#include "Json/single_include/nlohmann/json.hpp"

inline unsigned int CurrentID = 0;

template <typename Type, unsigned int _size = 1>
class CVariable
{
	enum { All = -1 };

public:

	constexpr CVariable(const char* m_szName)
	{
		ConfigID = CurrentID; CurrentID += size();

		m_szDebugName = new char[strlen(m_szName) + 1];

		strcpy(m_szDebugName, m_szName);
	}

	constexpr void Init(Type InitV, int index = All) noexcept
	{
		if (index == All)
			for (auto i = 0; i < size(); i++)
				data[i] = InitV;
		else
			data[index] = InitV;
	}

	constexpr void Save(nlohmann::json& j) noexcept
	{
		for (auto i = 0; i < size(); i++)
			j.emplace(std::to_string(ConfigID + i), data[i]);
	}

	constexpr void Load(const nlohmann::json& j) noexcept
	{
		for (auto i = 0; i < size(); i++)
			if (auto pData = j.find(std::to_string(ConfigID + i)); pData != j.end())
				data[i] = (*pData).get<Type>();
	}

	constexpr Type& Get(int index = 0) noexcept
	{
		return data[index];
	}

	constexpr const char* GetDebugName() const noexcept
	{
		return m_szDebugName;
	}

	constexpr unsigned int size() const noexcept
	{
		return _size;
	}

	// operators

	constexpr operator Type&()
	{
		return data[0];
	}

	constexpr operator Type() const
	{
		return data[0];
	}

	constexpr Type& operator[](int i)
	{
		return ((Type*)data)[i];
	}

	constexpr Type operator[](int i) const
	{
		return ((Type*)data)[i];
	}

	constexpr Type operator=(Type v)
	{
		return data[0] = v;
	}

	constexpr Color operator=(Color v)
	{
		return (Color&)data[0] = v;
	}

	constexpr auto operator=(std::array<Color, 2> v)
	{
		constexpr auto size = _size / sizeof(Color);

		assert(size == v.size());

		if (size == v.size())
		{
			for (auto i = 0; i < size; i++)
			{
				(Color&)data[i * sizeof(Color)] = v[i];
			}
		}

		return (Color*&)data[0];
	}

	constexpr auto operator=(std::tuple<Color, Color, Color> v)
	{
		constexpr auto size = _size / sizeof(Color);

		assert(size == 3);

		if (size == 3)
		{
			auto [Color1, Color2, Color3] = v;
			(Color&)data[0 * sizeof(Color)] = Color1;
			(Color&)data[1 * sizeof(Color)] = Color2;
			(Color&)data[2 * sizeof(Color)] = Color3;
		}

		return (Color*&)data[0];
	}

	constexpr Type* operator&()
	{
		return &data[0];
	}

	constexpr Type operator+(Type Other) const
	{
		return data[0] + Other;
	}

	constexpr Type operator-(Type Other) const
	{
		return data[0] - Other;
	}

	constexpr operator char*() const
	{
		return m_szDebugName;
	}

	constexpr operator Color() const
	{
		return ((Color&)data[0]);
	}

	constexpr operator Color&()
	{
		return ((Color&)data[0]);
	}

	constexpr operator Vector() const
	{
		return ((Vector&)data[0]);
	}

	constexpr operator Vector&()
	{
		return ((Vector&)data[0]);
	}

private:

	Type data[_size];

	char* m_szDebugName;

	unsigned int ConfigID;
};

namespace Menu
{
	struct Variables
	{
		struct
		{
			CVariable<bool>LAC_Bypass{ "Little Anti-Cheat Bypass" };

			struct
			{
				CVariable<bool> Enabled{ "Enabled" };

				CVariable<bool> AutoStop{ "Auto Stop" };

				CVariable<bool> AutoShoot{ "Auto Shoot" };

				CVariable<bool> AutoCrouch{ "Auto Crouch" };

				CVariable<bool> FriendlyFire{ "Friendly Fire" };

				CVariable<bool> Predicted{ "Predicted Postion" };

				CVariable<int> TargetSelection{ "Selection" };

				CVariable<float> SmoothVal{ "Smooth" };

				CVariable<int> Timeout{ "Timeout" };

				CVariable<int> HitBox{ "HitBox" };

				CVariable<int> Silent{ "Silent" };

				CVariable<int> Usage{ "Usage" };

				CVariable<int> FOV{ "FOV" };

			}Ragebot;

		}Aimbot;

		struct
		{
			struct 
			{
				CVariable<int> Filter{ "Filter" };

				CVariable<bool> AutoLaser{ "Auto Laser" };

				CVariable<bool> Wireframe_lasers{ "Wireframe Lasers" };

				CVariable<bool> Special_items_IgnoreZ{ "Special Items Ignore-Z" };

			} ZeStuff;

			CVariable<bool> Panic{ "Panic" };

			CVariable<bool> Unload{ "Unload" };

		}General;

		struct
		{
			// Textures

			CVariable<int> WorldTexture{ "" };

			CVariable<int> ZE_LasersTexture{ "Lasers Texture" };

			CVariable<int> BackgroundTexture{ "Background" };

			// Player Esp

			CVariable<bool, CategoriesCount> Box{ "Box" };

			CVariable<bool, CategoriesCount> Name{ "Name" };

			CVariable<int, CategoriesCount> BoxType{ "Type" };

			CVariable<bool, CategoriesCount> Skeleton{ "Skeleton" };

			CVariable<bool, CategoriesCount> SnapLine{ "SnapLine" };

			CVariable<bool, 2> Health{ "Health" };

			CVariable<bool, 2> Weapon{ "Weapon" };

			CVariable<int, 2> HealthType{ "Mode" };

			CVariable<int, CategoriesCount> NameFont{ "Font" };

			CVariable<int, 2> HealthFont{ "Font" };

			CVariable<int, 2> WeaponFont{ "Font" };

			CVariable<float, 2> HP_SpriteScale{ "Sprite Scale" };

			CVariable<int, 2> WeaponRenderingMode{ "Render Type" };

			// Assorted

			struct
			{
				CVariable<bool> INDEX{ "Index" };

				CVariable<bool> Enabled{ "Enabled" };

				CVariable<bool> Classname{ "Class name" };

				CVariable<bool> ModelName{ "Model name" };

				CVariable<bool> ModelIndex{ "Model Index" };

				CVariable<bool> Velocity{ "Velocity" };

				CVariable<bool, 4> Filter{ "Filter" };

				CVariable<bool> Origin{ "Origin" };

				CVariable<int> MaxDistance{ "" };

			}Debug;

			struct
			{
				CVariable<bool>  Enabled{ "Enabled" };

			}DamageIndicator;

			CVariable<int> KillEffect_Type{ "Kill Effect" };

			CVariable<int> ScopeLen{ "Scopes" };

			struct
			{
				CVariable<bool, 2> Enabled{ "Enabled" };
			}Glow;

			struct {

				CVariable<float> end{ "end" };

				CVariable<float> start{ "start" };

				CVariable<bool> Enabled{ "Enabled" };

				CVariable<float> maxdensity{ "Max density" };

			}FOG;

			struct {

				CVariable<bool> Enabled{ "Enabled" };

				CVariable<int> SpriteTexture{ "Texture" };

				CVariable<float> PointScale{ "Point Scale" };

				CVariable<float> m_flLife{ "Life" };

				CVariable<float> m_flWidth{ "Width" };

				CVariable<float> m_flEndWidth{ "EndWidth" };

				CVariable<float> m_flFadeLength{ "FadeLength" };

				CVariable<float> m_flAmplitude{ "Amplitude" };

				CVariable<float> m_flSpeed{ "Speed" };

				CVariable<int> m_nSegments{ "Segments" };

				CVariable<int> m_nFlags{ "Flags" };

			} Tracers;

			struct {

				CVariable<bool> Enabled{ "Enabled" };

				CVariable<int> SpriteTexture{ "Texture" };

				CVariable<int> RenderMode{ "Render Mode" };

				CVariable<float> Width{ "Width" }, EndWidth{ "EndWidth" }, LifeTime{ "Lifetime" };

			}Trails;

			struct
			{

				CVariable<bool> Enabled{ "Enabled" };

				CVariable<int> Time{ "Lifetime" };

				CVariable<int> Length{ "Length" };

				CVariable<int> Gap{ "gap" };

				CVariable<int> HitSound{ "Sound" };

				CVariable<int> Mode{ "Mode" };

				CVariable<int> Overlay{ "Overlay" };

			}Hitmarker;

			struct
			{
				CVariable<bool> Recoil{ "Recoil Crosshair" };

				struct
				{
					CVariable<bool> Enabled{ "Enabled" };

					CVariable<int> Length{ "Length" };

					CVariable<int> Gap{ "gap" };
				}X_Shape;

				struct
				{
					CVariable<bool> Enabled{ "Enabled" };

					CVariable<int> Length{ "Length" };

					CVariable<int> Gap{ "gap" };
				}Plus_Shape;

				struct
				{
					CVariable<bool> Enabled{ "Enabled" };

					CVariable<int> Radius{ "Radius" };

					CVariable<int> Segments{ "Segments" };

				}Circle_Shape;

			}Crosshair;

			CVariable<int> SkyBoxIndex{ "" };

			CVariable<int> NightVision{ "" };

			CVariable<int> Hands{ "Hands" };

			CVariable<bool> NoFog{ "Disable Fog" };

			CVariable<int> NightMode{ "Night mode" };

			CVariable<int> MuzzleFlash{ "MuzzleFlash" };

			CVariable<bool> FullBright{ "Full Bright" };

			CVariable<int> FieldOfView{ "Field of view" };

			CVariable<int> ViewModelFov{ "Viewmodel fov" };

			CVariable<int> FlashPercentage{ "Flash reduction" };

			CVariable<bool> Disable_Fire_Particles{ "Disable Fire Particles" };

			CVariable<bool> Disable_Players_MuzzleFlash{ "Disable Players MuzzleFlash" };

			CVariable<bool> NoFlashLight{ "Disable Players FlashLight" };

			CVariable<bool> NoVisualRecoil{ "Disable Visual Recoil" };

			CVariable<bool> NoTeammates{ "Hide Teammates Skins" };

			CVariable<bool> NadeTracer{ "Nade Tracer" };

			CVariable<bool> NoSmoke{ "Disable Smoke" };

			CVariable<int> DLights{ "" };

			struct
			{
				CVariable<bool> Enabled{ "Enabled" };

				CVariable<int> Type{ "Type" };

				CVariable<float> Width{ "Width" };

				CVariable<float> Speed{ "Speed" };

				CVariable<float> Radius{ "Radius" };

				CVariable<float> Length{ "Length" };


			}Weather;

			struct
			{
				struct
				{
					CVariable<bool> Enabled{ "Player" };

					CVariable<bool> Wireframe{ "Wireframe" };

					CVariable<bool> Ignorez{ "Ignore-Z" };

					CVariable<int> Type{ "Type" };

				}Player;

				struct
				{
					CVariable<bool> Enabled{ "Weapon" };

					CVariable<bool> Wireframe{ "Wireframe" };

					CVariable<bool> Ignorez{ "Ignore-Z" };

					CVariable<int> Type{ "Type" };

				}Weapon;

			}Chams;

			struct
			{
				CVariable<bool> Enabled{ "Enabled" };

				CVariable<float, 3>  Origin{ "" };

				CVariable<float, 3>  Angles{ "" };
			}ViewModel;

		}Visuals;


		struct
		{
			CVariable<int> Ragdoll_Force{ "" };

			CVariable<bool> Faststop{ "Fast Stop" };

			CVariable<bool> NoMotd{ "Disable MOTD" };

			CVariable<bool> AutoPisol{ "Auto Pisol" };

			CVariable<bool> MuteRadio{ "Disable Radio" };

			CVariable<bool> NoRecoil{ "Anti Bullet Recoil" };

			CVariable<bool> NoSpread{ "Anti Bullet Spread" };

			CVariable<bool> NoDrug{ "Disable Server Drug" };

			CVariable<bool> NoShake{ "Disable Screen Shake" };

			CVariable<bool> Disable_Game_Console_Warnings{ "Disable Game Console Warnings" };

			CVariable<bool> Fastrun{ "Fast Run" };

			CVariable<bool> AutoKevlar{ "Auto Kevlar" };

			CVariable<bool> Bunnyhop{ "Enabled" };

			CVariable<bool> Autostrafe{ "Auto Strafe" };

			CVariable<bool> CircleStrafe{ "Circle Strafe" };

			CVariable<bool> Edgejump{ "Edge Jump" };

			CVariable<bool> FastLadder{ "Fast Ladder Climb" };

			CVariable<bool> AntiAFKkick{ "Anti AFK Kick" };

			CVariable<bool> Sv_Pure_Bypass{ "Pure Bypass" };

			CVariable<int> m_vecCameraOffset{ "Distance" };

			CVariable<int> Bunnyhop_Perfect_Rate{ "" };

			CVariable<bool> KillMessage{ "Enabled" };

			struct
			{
				CVariable<int> MusicVolume{ "" }, FootStepsVolume{ "" }, WeaponsAudio{ "" };
			}Sounds;


			struct
			{
				CVariable<bool> Enabled{ "Enabled" };

				CVariable<float> Desired_Gain{ "Desired Gain" };

				CVariable<float> Required_Speed{ "Required Speed" };

				CVariable<float> Greatest_Possible_Strafe_Angle{ "Greatest Possible Strafe Angle" };

			}StrafeOptimizer;

			struct
			{
				CVariable<bool> Enabled{ "Enabled" };

				CVariable<int> Usage{ "Usage" };

				CVariable<bool, 6> Filter{ "Filter" };

			}Triggerbot;

			struct
			{
				CVariable<bool> Enabled{ "Enabled" };

				CVariable<bool> Random{ "Random" };

				CVariable<int> SteamID{ "" };

			}SteamIDSpoofer;

			struct
			{
				CVariable<bool> Enabled{ "Enabled" };

				CVariable<int> Method{ "Method" };

				CVariable<int> Usage{ "Usage" };

			}KnifeBot;

			struct
			{
				CVariable<bool> Changer{ "Changer" };

				CVariable<bool> Stealer{ "Stealer" };

				CVariable<float> Delay{ "Delay" };

			}ClanTag;

			CVariable<int> DownloadManagerFilter{ "" };

			char CustomizeableFiles[100];

		}Misc;

		struct
		{
			struct
			{
				CVariable<uint8_t, sizeof(Color) * CategoriesCount> Box{ "" };
				CVariable<uint8_t, sizeof(Color) * CategoriesCount> Name{ "" };
				CVariable<uint8_t, sizeof(Color) * CategoriesCount> SnapLine{ "" };
				CVariable<uint8_t, 8> Skeleton{ "" };
				CVariable<uint8_t, 8> Health{ "" };
				CVariable<uint8_t, 8> Weapon{ "" };
				CVariable<uint8_t, 4> DLights{ "" };

			}PlayerEsp;

			struct
			{
				CVariable<uint8_t, 4> Player{ "" };
				CVariable<uint8_t, 4> Weapon{ "" };
			}Chams;

			struct
			{
				CVariable<uint8_t, 8> Player{ "" };
			}Glow;

			struct
			{
				CVariable<uint8_t, 4> SkyBox{ "SkyBox" };
				CVariable<uint8_t, 4> World{ "World" };
				CVariable<uint8_t, 4> DamageIndicator{ "" };
				CVariable<uint8_t, 4> Nightvision{ "Nightvision" };
				CVariable<uint8_t, 4> NadeTracer{ "Nade Tracer" };
				CVariable<uint8_t, 4> BulletImpact{ "Bullet Impact" };

				struct
				{
					CVariable<uint8_t, 4> X_Shape{ "" };

					CVariable<uint8_t, 4> Plus_Shape{ "" };

					CVariable<uint8_t, 4> Circle_Shape{ "" };

				}Crosshair;

				CVariable<uint8_t, 4> Hitmarker{ "" };
				CVariable<uint8_t, 4> Tracers{ "" };
				CVariable<uint8_t, 4> Fog{ "" };
				CVariable<uint8_t, 4> Trails{ "" };
				CVariable<uint8_t, 4> HudColor{ "Hud" };
				CVariable<uint8_t, 4> Net_graph{ "Net graph" };

			}General;

		}Colors;

		struct
		{
			CVariable<bool> On{ "" };

			CVariable<int> FontID{ "Font" };

			CVariable<unsigned short> Key{ "Menu HotKey" };
		}Menu;

		struct
		{
			CVariable<unsigned short> RageBot{ "" };

			CVariable<unsigned short> AirStuck{ "" };

			CVariable<unsigned short> Edgejump{ "" };

			CVariable<unsigned short> Bunnyhop{ "" };

			CVariable<unsigned short> Triggerbot{ "" };

			CVariable<unsigned short> Autostrafe{ "" };

			CVariable<unsigned short> CircleStrafe{ "" };

			CVariable<unsigned short> Unload{ "" };

			CVariable<unsigned short> PanicKey{ "" };

			CVariable<unsigned short> KnifeBot{ "" };
		}Keys;
	};

	extern Variables Get;

	void ZE() noexcept;
	void Menu() noexcept;
	void Render() noexcept;
	void Aimbot() noexcept;
	void ConVars() noexcept;
	void Visuals() noexcept;
	void RenderTabs() noexcept;
	void TriggerBot() noexcept;
	void Miscellaneous() noexcept;


	//


	void AssignVariables(bool IgnoreMenu = 0) noexcept;
}

#endif
