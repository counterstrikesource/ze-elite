#ifndef Assorted_H
#define Assorted_H

#include <filesystem>

struct IDirect3DDevice9;

void ForceFullUpdate() noexcept;

void UpdateScopeLens() noexcept;

void Hit_Marker_On_Paint() noexcept;

void Prepare_Custom_Stuff() noexcept;

void SetClanTag(const char* tag) noexcept;

BOOL DirectoryExists(LPCTSTR szPath) noexcept;

auto PerformCopy(HMODULE DLL) noexcept -> void;

void UpdateNightVision(bool Unload = false) noexcept;

bool PrecacheModel(const  char* szModelName) noexcept;

void DrawBeamd(Vector src, Vector end, Color color) noexcept;

void Update_Net_Graph_Text_Colors(bool Unload = false) noexcept;

void NET_SetConVar(const char* cvar, const char* value) noexcept;

bool FindStringCIS(std::string data, std::string toSearch) noexcept;

bool IsGoodItem(int size, int& ID, bool reset_if_not_valid = true) noexcept;

void Set_DisConnection_Msg(const char* Message, bool Reset = false) noexcept;

void ProcessGameEventListeners(void* WriteListenEventList = nullptr) noexcept;

std::string ReplaceString(std::string subject, const std::string& search, const std::string& replace) noexcept;

auto AddFilesNameFromPathToArray(std::vector<std::string>& VectorOfString, const char* EndOfFile = ".vmt") noexcept -> void;

bool IntersectRayWithOBB(const Vector& vecRayStart, const Vector& vecRayDelta, const matrix3x4& matOBBToWorld, const Vector& vecOBBMins, const Vector& vecOBBMaxs) noexcept;

void UTIL_ClipTraceToPlayers
(
	const Vector& vecAbsStart,
	const Vector& vecAbsEnd,
	unsigned int mask,
	ITraceFilter* filter,
	trace_t* tr
) noexcept;

namespace Load
{
	void Once(HWND& hWindow, IDirect3DDevice9*& DirectXDevice);

	void Repeatedly();
}

namespace Unload
{
	void PreUnHook(void*);

	void PostUnHook();
}

extern char* TempPath;

extern float _flHurtTime;

extern bool Overlay_Triggered;

extern char DirectoryPath[MAX_PATH];

namespace FileSystem = std::filesystem;

extern std::vector<std::string> HitSounds;

extern CLC_ListenEvents* CLC_ListenEvents_Table;

#endif
