#pragma once

class CClientState
{
public:
	const char* GetLevelNameShort()
	{
		return (const char*)((unsigned int)this + 0x230);
	}

	DECLARE_OFFSET_FUNCTION(GetNetChannel, 0x10, CNetChan*);
	DECLARE_OFFSET_FUNCTION(GetDeltaTick, 0x1A0, int);
	DECLARE_OFFSET_FUNCTION(GetLastoutgoingcommand, 0x4B24, int);
	DECLARE_OFFSET_FUNCTION(GetChokedcommands, 0x4B28, int);
	DECLARE_OFFSET_FUNCTION(GetLast_command_ack, 0x4B2C, int);
};
