#include "SDK.h"
#include "Assorted.h"

void CBaseEntity::SetModelByIndex(int index) noexcept
{
	using Type = void(__thiscall*)(void*, int);

	reinterpret_cast<Type>(SetModelByIndexA)(this, index);
}

void CBaseEntity::SetParent(void* pParent, int attachment) noexcept
{
	using Type = void(__thiscall*)(void*, void*, int);

	reinterpret_cast<Type>(SetParentA)(this, pParent, attachment);
}

CBaseEntity* CBaseEntity::GetOwnerEntity() noexcept
{
	 using Type = CBaseEntity * (__thiscall*)(CBaseEntity*);

	return reinterpret_cast<Type>(GetOwnerEntityA)(this);
}

const char* CBaseEntity::GetDebugName() noexcept
{
	using Type = const char* (__thiscall*)(void*);

	return reinterpret_cast<Type>(GetDebugNameA)(this);
}

float& CBaseEntity::GetNightVisionAlpha() noexcept
{
	return *reinterpret_cast<float*>((uintptr_t)this + NightVisionAlphaA);
}

uintptr_t CBaseEntity::GetCreationTick() noexcept
{
	using Type = uintptr_t;

	return *reinterpret_cast<Type*>((Type)this + CreationTickA);
}

Vector& CBaseEntity::EstimateAbsVelocity() noexcept
{
	Vector Veloctiy;

	using Type = Vector & (__thiscall*)(void*, Vector&);

	reinterpret_cast<Type>(EstimateAbsVelocityA)(this, Veloctiy);

	return Veloctiy;
}

void CBaseEntity::UpdateButtonState(int nUserCmdButtonMask) noexcept
{
	using Type = void(__thiscall*)(void*, int);

	reinterpret_cast<Type>(UpdateButtonStateA)(this, nUserCmdButtonMask);
}

void CBaseEntity::Remove() noexcept
{
	using Type = void(__thiscall*)(void*);

	reinterpret_cast<Type>(RemoveA)(this);
}

char CBaseEntity::GetMoveType() noexcept
{
	using Type = uintptr_t;

	return *reinterpret_cast<char*>((uintptr_t)this + MoveTypeA);
}

Vector CBaseEntity::GetBonePos(int i) noexcept
{
	matrix3x4 boneMatrix[128];

	if (SetupBones(boneMatrix, 128, 0x00000100, GetTickCount64()))
	{
		return Vector(boneMatrix[i][0][3], boneMatrix[i][1][3], boneMatrix[i][2][3]);
	}
	return Vector(0, 0, 0);
}

const char* CBaseEntity::GetName() const noexcept
{
	player_info_t Info;

	if (Engine->GetPlayerInfo(GetIndex(), &Info))
	{
		return Info.name;
	}

	return "unnamed";
}

bool CBaseEntity::CanSeePlayer(CBaseEntity* player, const Vector& pos) noexcept
{
	CGameTrace tr;

	using Type = void(*)(const Vector&, const Vector&, unsigned int, const CBaseEntity*, int, trace_t*);

	reinterpret_cast<Type>(UTIL_TraceLine)(GetEyePosition(), pos, CS_MASK_SHOOT | CONTENTS_HITBOX, this, 0, &tr);

	return tr.m_pEnt == player || tr.fraction > 0.97f;
}

const Vector CBaseEntity::GetHitboxPosition(int iHitbox) noexcept
{
	auto model{ GetModel() };
	if (!model)
		return Vector();

	matrix3x4 matrix[128];
	if (!SetupBones(matrix, 128, 0x100, Globals->curtime))
		return Vector();

	auto pStudioHdr = ModelInfo->GetStudiomodel(model);
	if (!pStudioHdr)
		return Vector();

	auto pHitboxSet = pStudioHdr->GetHitboxSet(GetHitboxSet());
	if (!pHitboxSet)
		return Vector();

	mstudiobbox_t* box = pHitboxSet->GetHitbox(iHitbox);
	if (!box)
		return Vector();

	Vector vCenter = (box->bbmin + box->bbmax) * 0.5f;

	Vector vHitbox;

	Math::VectorTransform(vCenter, matrix[box->bone], vHitbox);

	return vHitbox;
}

CBaseEntity* CBaseEntity::GetLocalPlayer() noexcept
{
	using Type = CBaseEntity*;

	return *reinterpret_cast<Type*>(LocalPlayerA);
}

CCSWeaponInfo* CBaseCombatWeapon::GetWpnData() noexcept
{
	using Type = CCSWeaponInfo * (__thiscall*)(void*);

	return reinterpret_cast<Type>(WpnDataA)(this);
}

const char* C_PlayerResource::GetClanTag(size_t INDEX) noexcept
{
	static auto m_szClan = GetNetVarOffset("DT_CSPlayerResource", "m_szClan");

	return (const char*)(this - 0x518 + m_szClan + INDEX * 0X10);
}