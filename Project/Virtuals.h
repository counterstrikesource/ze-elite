#pragma once

#ifndef Virtuals_H
#define Virtuals_H

template <typename Type = void*>
Type GetVirtualMethod(void* pThis, size_t INDEX)
{
	return (*reinterpret_cast<Type**>(pThis))[INDEX];
}

#endif
