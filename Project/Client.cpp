#include "Client.h"

#include "include.h"
#include "SDK.h"
#include "Glow.h"
#include "Trails.h"
#include "ClientEntityListener.h"
#include "SkinChanger.h"
#include "Backtrack.h"
#include "VoiceChat.h"

extern bool Toggled_On;
extern void* RainNetworkable;
extern CBaseEntity* RainEntity;

bool Was_On_Ground_Backup{ 0 };

Shutdown_Type Original_Shutdown;
CreateMove_Type Original_CreateMove;
FrameStageNotify_Type Original_FrameStageNotify;
DispatchUserMessage_Type Original_DispatchUserMessage;

void AntiSmac(CBaseEntity* pLocal, CUserCmd* cmd) noexcept
{
	static auto Last_Angles{ cmd->viewangles };

	if (Menu::Get.Aimbot.LAC_Bypass && Menu::Get.Aimbot.Ragebot.Silent != 2)
	{
		bool ShouldProcess;

		constexpr auto MAX_ANGLE_DELTA = 4.f;

		ShouldProcess = (cmd->buttons & IN_JUMP) == 0 || !pLocal->IsOnGround();

		if (auto delta{ cmd->viewangles - Last_Angles }; ShouldProcess) {

			Math::NormalizeAngle(delta);

			if (delta.Normalized() >= MAX_ANGLE_DELTA)
			{
				cmd->viewangles = Last_Angles + delta * MAX_ANGLE_DELTA;

				Math::NormalizeAngle(cmd->viewangles);

				if (LAC_Bypass::NEXT_TICK != LAC_Bypass::Stages::NONE) {

					LAC_Bypass::NEXT_TICK = LAC_Bypass::Stages::NONE;

					ReleaseButton(IN_ATTACK);
				}
			}
		}
	}

	Last_Angles = cmd->viewangles;
}

void __stdcall LevelShutdown(void*, void*)
{
	RainEntity = nullptr;

	Trails::Entity = nullptr;

	RainNetworkable = nullptr;

	Glow::ClearObjects();

	EntitiesListener.clear();

	HP_Bar::ClearSprites(false);

	Toggled_On =
	_flHurtTime =
	Last_Logged_Time =
	VoiceChat::EndTime =
	Aimbot::LastProcessTime =
	DamageIndicator::data.flEraseTime = 0.f;

	for (auto& SoundInfo : VoiceChat::SoundList) 
		SoundInfo.KeyWasPressed = 0;

	return Original_Shutdown(BaseClientDLL);
}

void LAC_Bypass::PostCreateMove(CBaseEntity* pLocal, CUserCmd* cmd, bool& bSendPacket)
{
	const auto SideMove_Backup = cmd->sidemove;
	const auto ViewAngles_Backup = cmd->viewangles;
	const auto ForwardMove_Backup = cmd->forwardmove;

	if (NEXT_TICK != Stages::NONE)
	{
		bool DONT_SEND_PACKET = false;

		switch (NEXT_TICK)
		{
		case Stages::PRE_FIRE:

			if (Menu::Get.Aimbot.LAC_Bypass) {

				NEXT_TICK = Stages::FIRE;

				cmd->buttons &= ~IN_ATTACK;
			}
			else
			{
				NEXT_TICK = Stages::NONE;

				goto Label;
			}

			break;

		case Stages::FIRE:

			cmd->buttons |= IN_ATTACK;

			NEXT_TICK = Stages::POST_FIRE;

		Label:

			cmd->viewangles = AIM_ANGLES;

			if (Menu::Get.Misc.NoRecoil)
			{
				cmd->viewangles -= pLocal->GetPunchAngle() * 2.f;

				bSendPacket = false;
			}

			Math::ClampAngles(cmd->viewangles);

			if (Menu::Get.Misc.NoSpread)
			{
				const bool NoSpread(CBaseEntity * pLocal, CUserCmd * cmd, QAngle & Angle) noexcept;

				bSendPacket = bSendPacket || !NoSpread(pLocal, cmd, cmd->viewangles);
			}

			break;

		default:

			bSendPacket = false;

			NEXT_TICK = Stages::NONE;

			cmd->buttons |= IN_ATTACK;

			cmd->viewangles.y = AIM_ANGLES.y + 4.f;

			Math::NormalizeAngle(cmd->viewangles.y);

			break;
		}
	}

	AntiSmac(pLocal, cmd);

	Math::CorrectMovement(ViewAngles_Backup, cmd, ForwardMove_Backup, SideMove_Backup);
}

void __stdcall CreateMove(int sequence_number, float input_sample_frametime, bool active, bool& bSendPacket) noexcept
{
	CBaseEntity* pLocal{ CBaseEntity::GetLocalPlayer() };

	CUserCmd* cmd = GlobalVars::cmd = { &Input->Commands()[sequence_number % MULTIPLAYER_BACKUP] };

	CVerifiedUserCmd* verified{ &Input->VerifiedCommands()[sequence_number % MULTIPLAYER_BACKUP] };

	if (!cmd || !verified || !pLocal)
	{
		return;
	}

	QAngle ViewAngles_Pre_CreateMove{ cmd->viewangles };

	Original_CreateMove(sequence_number, input_sample_frametime, active);

	QAngle ViewAngles_Post_CreateMove{ cmd->viewangles };

	if (Menu::Get.General.Panic) goto End;

	if (Menu::Get.Aimbot.Ragebot.Silent && Aimbot::LoopCount >= 1 && ClientState->GetChokedcommands() < 10)
	{
		cmd->viewangles = ViewAngles_Pre_CreateMove;

		Math::CorrectMovement(ViewAngles_Post_CreateMove, cmd, cmd->forwardmove, cmd->sidemove);

		Aimbot::LoopCount--;
	}

	if (ClientState->GetChokedcommands() < 10)
	{
		Was_On_Ground_Backup = pLocal->IsOnGround();

		Miscellaneous::Run(pLocal, cmd, bSendPacket);

		AutoLaser::Run(pLocal, cmd, bSendPacket);

		Miscellaneous::EnginePrediction(pLocal, cmd);

		Aimbot::Run(pLocal, cmd, bSendPacket);

		void BunnyHop(CBaseEntity*, CUserCmd*) noexcept; BunnyHop(pLocal, cmd);

		void EdgeJump(CBaseEntity*, CUserCmd*) noexcept; EdgeJump(pLocal, cmd);

		void StrafeOptimizer(CBaseEntity*, CUserCmd*); StrafeOptimizer(pLocal, cmd);

		void Bullet_Accuracy(CBaseEntity*, CUserCmd*, bool&) noexcept; Bullet_Accuracy(pLocal, cmd, bSendPacket);
	}
	else
	{
		cmd->buttons &= ~IN_ATTACK;
	}

	LAC_Bypass::PostCreateMove(pLocal, cmd, bSendPacket);

End:

	verified->m_cmd = *cmd;

	verified->m_crc = cmd->GetChecksum();
}

void __stdcall FrameStageNotify(int curStage) noexcept
{
	if (CBaseEntity* MyPlayer{ CBaseEntity::GetLocalPlayer() }; MyPlayer) {

		if (curStage == FRAME_NET_UPDATE_POSTDATAUPDATE_START)
		{
			VoiceChat::ProcessVoiceOff();

			if (MyPlayer->IsAlive())
			{
				HP_Bar::Process(MyPlayer);

				SkinChanger::Run(MyPlayer);

				auto CurTickCount = TickCount;

				if (static auto LastUsedTick = CurTickCount; CurTickCount > LastUsedTick + 1500)
				{
					auto UpdateSlowly = [&]() -> void
					{
						if (Menu::Get.Visuals.Trails.Enabled)
						{
							Trails::Create(MyPlayer);
						}
						else if (Trails::Entity)
						{
							Trails::Entity->Remove();

							Trails::Entity = nullptr;
						}
					};

					UpdateSlowly();

					LastUsedTick = CurTickCount;
				}

				void Auto_Kevlar(CBaseEntity * MyPlayer); Auto_Kevlar(MyPlayer);
			}

		}

		if (curStage == FRAME_RENDER_START)
		{
			void Precipitation(); Precipitation();

			if (MyPlayer->IsAlive())
			{
				SkinChanger::Run(MyPlayer);

				MyPlayer->GetFlashMaxAlpha() = 255.0f - Menu::Get.Visuals.FlashPercentage * 2.55f;

				if (Menu::Get.Visuals.NoVisualRecoil)
				{
					auto Backup_AimPunch = MyPlayer->GetPunchAngle();

					MyPlayer->GetPunchAngle().Init();

					Original_FrameStageNotify(BaseClientDLL, curStage);

					MyPlayer->GetPunchAngle() = Backup_AimPunch;

					return;
				}
			}
		}
	}

	Original_FrameStageNotify(BaseClientDLL, curStage);
}

bool __stdcall DispatchUserMessage(int messageType, void*& msg_data) noexcept
{
	if (Menu::Get.Misc.NoMotd && messageType == CS_UM_VGUIMenu)
	{
		const char* VGUIMenu_Name = *(const char**)(&msg_data);

		if (!strcmp(VGUIMenu_Name, "info"))
		{
			Engine->ClientCmd_Unrestricted("joingame");

			return true;
		}
	}

	if (!messageType
		|| (messageType == CS_UM_Shake && Menu::Get.Misc.NoShake)
		|| (messageType == CS_UM_Fade && Menu::Get.Misc.NoDrug)
		|| ((messageType == CS_UM_RadioText || messageType == CS_UM_SendAudio) && Menu::Get.Misc.MuteRadio))
		return true;

	return Original_DispatchUserMessage(BaseClientDLL, messageType, msg_data);
}

void __declspec(naked) __stdcall Hooked_CreateMove(int sequence_number, float input_sample_frametime, bool active) noexcept
{
	__asm
	{
		LEA  EAX, [EBP - 01]
		PUSH EAX
		PUSH[ESP + 0x10]
		PUSH[ESP + 0x10]
		PUSH[ESP + 0x10]
		CALL CreateMove
		RET 0x0C
	}
}