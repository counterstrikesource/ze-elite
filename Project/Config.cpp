#include "SDK.h"
#include <Windows.h>
#include <fstream>
#include <iomanip>
#include "Config.h"
#include "Assorted.h"
#include "VoiceChat.h"
#include "SkinChanger.h"
#include "Json/Single_include/Nlohmann/Json.hpp"

nlohmann::json j;

#define Version "6.2"

BOOL DirectoryExists(LPCTSTR szPath) noexcept
{
	DWORD dwAttrib = GetFileAttributes(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

void Config::SaveConfig()
{
	if (!DirectoryExists("ZE"))
		CreateDirectory("ZE", nullptr);

	constexpr auto& Aimbot = Menu::Get.Aimbot;
	{
		Aimbot.LAC_Bypass.Save(j);
		Aimbot.Ragebot.Enabled.Save(j);
		Aimbot.Ragebot.AutoStop.Save(j);
		Aimbot.Ragebot.Predicted.Save(j);
		Aimbot.Ragebot.AutoShoot.Save(j);
		Aimbot.Ragebot.AutoCrouch.Save(j);
		Aimbot.Ragebot.FriendlyFire.Save(j);

		Aimbot.Ragebot.TargetSelection.Save(j);
		Aimbot.Ragebot.SmoothVal.Save(j);
		Aimbot.Ragebot.Timeout.Save(j);
		Aimbot.Ragebot.HitBox.Save(j);
		Aimbot.Ragebot.Silent.Save(j);
		Aimbot.Ragebot.Usage.Save(j);
		Aimbot.Ragebot.FOV.Save(j);
	}

	constexpr auto& General = Menu::Get.General;
	{
		General.ZeStuff.Filter.Save(j);

		General.ZeStuff.AutoLaser.Save(j);
		General.ZeStuff.Wireframe_lasers.Save(j);
		General.ZeStuff.Special_items_IgnoreZ.Save(j);
	}

	constexpr auto& Visuals = Menu::Get.Visuals;
	{
		Visuals.WorldTexture.Save(j);
		Visuals.ZE_LasersTexture.Save(j);
		Visuals.BackgroundTexture.Save(j);

		Visuals.Box.Save(j);
		Visuals.Name.Save(j);
		Visuals.Health.Save(j);
		Visuals.Weapon.Save(j);
		Visuals.Skeleton.Save(j);
		Visuals.SnapLine.Save(j);

		Visuals.BoxType.Save(j);
		Visuals.HealthType.Save(j);
		Visuals.NameFont.Save(j);
		Visuals.HealthFont.Save(j);
		Visuals.WeaponFont.Save(j);
		Visuals.HP_SpriteScale.Save(j);
		Visuals.WeaponRenderingMode.Save(j);

		Visuals.KillEffect_Type.Save(j);
		Visuals.ScopeLen.Save(j);

		Visuals.SkyBoxIndex.Save(j);
		Visuals.NightVision.Save(j);
		Visuals.Hands.Save(j);
		Visuals.NightMode.Save(j);
		Visuals.MuzzleFlash.Save(j);
		Visuals.FieldOfView.Save(j);
		Visuals.ViewModelFov.Save(j);
		Visuals.FlashPercentage.Save(j);
		Visuals.DLights.Save(j);

		Visuals.FullBright.Save(j);
		Visuals.NoFog.Save(j);
		Visuals.Disable_Fire_Particles.Save(j);
		Visuals.Disable_Players_MuzzleFlash.Save(j);
		Visuals.NoFlashLight.Save(j);
		Visuals.NoSmoke.Save(j);
		Visuals.NadeTracer.Save(j);
		Visuals.NoVisualRecoil.Save(j);

		constexpr auto& Glow = Menu::Get.Visuals.Glow;
		{
			Glow.Enabled.Save(j);
		}

		constexpr auto& FOG = Menu::Get.Visuals.FOG;
		{
			FOG.Enabled.Save(j);

			FOG.end.Save(j);
			FOG.start.Save(j);
			FOG.maxdensity.Save(j);
		}

		constexpr auto& Trails = Menu::Get.Visuals.Trails;
		{
			Trails.Enabled.Save(j);

			Trails.Width.Save(j);
			Trails.RenderMode.Save(j);
			Trails.EndWidth.Save(j);
			Trails.LifeTime.Save(j);
			Trails.SpriteTexture.Save(j);
		}

		constexpr auto& Tracers = Menu::Get.Visuals.Tracers;
		{
			Tracers.Enabled.Save(j);

			Tracers.SpriteTexture.Save(j);
			Tracers.PointScale.Save(j);
			Tracers.m_flLife.Save(j);
			Tracers.m_flWidth.Save(j);
			Tracers.m_flEndWidth.Save(j);
			Tracers.m_flFadeLength.Save(j);
			Tracers.m_flAmplitude.Save(j);
			Tracers.m_flSpeed.Save(j);
			Tracers.m_nSegments.Save(j);
			Tracers.m_nFlags.Save(j);
		}

		constexpr auto& Weather = Menu::Get.Visuals.Weather;
		{
			Weather.Enabled.Save(j);

			Weather.Type.Save(j);
			Weather.Width.Save(j);
			Weather.Speed.Save(j);
			Weather.Radius.Save(j);
			Weather.Length.Save(j);
		}

		constexpr auto& Debug = Menu::Get.Visuals.Debug;
		{
			Debug.Enabled.Save(j);

			Debug.INDEX.Save(j);
			Debug.Classname.Save(j);
			Debug.ModelName.Save(j);
			Debug.ModelIndex.Save(j);
			Debug.Velocity.Save(j);
			Debug.Origin.Save(j);
			Debug.Filter.Save(j);

			Debug.MaxDistance.Save(j);
		}

		constexpr auto& Hitmarker = Menu::Get.Visuals.Hitmarker;
		{
			Hitmarker.Enabled.Save(j);

			Hitmarker.Time.Save(j);
			Hitmarker.Length.Save(j);
			Hitmarker.Gap.Save(j);
			Hitmarker.HitSound.Save(j);
			Hitmarker.Mode.Save(j);
			Hitmarker.Overlay.Save(j);
		}

		constexpr auto& DamageIndicator = Menu::Get.Visuals.DamageIndicator;
		{
			DamageIndicator.Enabled.Save(j);
		}

		constexpr auto& Chams = Menu::Get.Visuals.Chams;
		{
			constexpr auto& Player = Chams.Player;
			{
				Player.Enabled.Save(j);

				Player.Wireframe.Save(j);
				Player.Ignorez.Save(j);

				Player.Type.Save(j);
			}

			constexpr auto& Weapon = Chams.Weapon;
			{
				Weapon.Enabled.Save(j);

				Weapon.Wireframe.Save(j);
				Weapon.Ignorez.Save(j);

				Weapon.Type.Save(j);
			}
		}

		constexpr auto& ViewModel = Menu::Get.Visuals.ViewModel;
		{
			ViewModel.Enabled.Save(j);

			ViewModel.Origin.Save(j);
			ViewModel.Angles.Save(j);
		}

		constexpr auto& Crosshair = Menu::Get.Visuals.Crosshair;
		{
			Crosshair.Recoil.Save(j);

			constexpr auto& X_Shape = Crosshair.X_Shape;
			{
				X_Shape.Enabled.Save(j);

				X_Shape.Length.Save(j);
				X_Shape.Gap.Save(j);
			}

			constexpr auto& Plus_Shape = Crosshair.Plus_Shape;
			{
				Plus_Shape.Enabled.Save(j);

				Plus_Shape.Length.Save(j);
				Plus_Shape.Gap.Save(j);
			}

			constexpr auto& Circle_Shape = Crosshair.Circle_Shape;
			{
				Circle_Shape.Enabled.Save(j);

				Circle_Shape.Radius.Save(j);
				Circle_Shape.Segments.Save(j);
			}
		}
	}

	constexpr auto& Misc = Menu::Get.Misc;
	{
		Misc.NoMotd.Save(j);
		Misc.NoDrug.Save(j);
		Misc.NoShake.Save(j);
		Misc.MuteRadio.Save(j);
		Misc.NoRecoil.Save(j);
		Misc.NoSpread.Save(j);
		Misc.AutoPisol.Save(j);
		Misc.Faststop.Save(j);
		Misc.Disable_Game_Console_Warnings.Save(j);
		Misc.Fastrun.Save(j);
		Misc.AutoKevlar.Save(j);
		Misc.Bunnyhop.Save(j);
		Misc.Autostrafe.Save(j);
		Misc.CircleStrafe.Save(j);
		Misc.Edgejump.Save(j);
		Misc.FastLadder.Save(j);
		Misc.AntiAFKkick.Save(j);
		Misc.Sv_Pure_Bypass.Save(j);
		Misc.KillMessage.Save(j);

		Misc.Ragdoll_Force.Save(j);
		Misc.m_vecCameraOffset.Save(j);
		Misc.Bunnyhop_Perfect_Rate.Save(j);

		constexpr auto& StrafeOptimizer = Misc.StrafeOptimizer;
		{
			StrafeOptimizer.Enabled.Save(j);

			StrafeOptimizer.Desired_Gain.Save(j);
			StrafeOptimizer.Required_Speed.Save(j);
			StrafeOptimizer.Greatest_Possible_Strafe_Angle.Save(j);
		}

		constexpr auto& Triggerbot = Misc.Triggerbot;
		{
			Triggerbot.Enabled.Save(j);

			Triggerbot.Filter.Save(j);

			Triggerbot.Usage.Save(j);
		}

		constexpr auto& KnifeBot = Misc.KnifeBot;
		{
			KnifeBot.Enabled.Save(j);

			KnifeBot.Method.Save(j);
			KnifeBot.Usage.Save(j);
		}

		constexpr auto& ClanTag = Misc.ClanTag;
		{
			ClanTag.Changer.Save(j);
			ClanTag.Stealer.Save(j);

			ClanTag.Delay.Save(j);
		}

		constexpr auto& Sounds = Misc.Sounds;
		{
			Sounds.MusicVolume.Save(j);
			Sounds.WeaponsAudio.Save(j);
			Sounds.FootStepsVolume.Save(j);
		}

		constexpr auto& SteamIDSpoofer = Misc.SteamIDSpoofer;
		{
			SteamIDSpoofer.Enabled.Save(j);

			SteamIDSpoofer.Random.Save(j);

			SteamIDSpoofer.SteamID.Save(j);
		}

		Menu::Get.Menu.FontID.Save(j);

		Menu::Get.Menu.Key.Save(j);
	}

	constexpr auto& Colors = Menu::Get.Colors;
	{
		constexpr auto& PlayerEsp = Menu::Get.Colors.PlayerEsp;
		{
			PlayerEsp.Box.Save(j);

			PlayerEsp.Name.Save(j);

			PlayerEsp.Health.Save(j);

			PlayerEsp.Weapon.Save(j);

			PlayerEsp.SnapLine.Save(j);

			PlayerEsp.Skeleton.Save(j);

			PlayerEsp.DLights.Save(j);
		}

		constexpr auto& Chams = Menu::Get.Colors.Chams;
		{
			Chams.Player.Save(j);

			Chams.Weapon.Save(j);
		}

		constexpr auto& Glow = Menu::Get.Colors.Glow;
		{
			Glow.Player.Save(j);
		}

		constexpr auto& General = Menu::Get.Colors.General;
		{

			General.SkyBox.Save(j);
			General.World.Save(j);
			General.DamageIndicator.Save(j);
			General.Nightvision.Save(j);
			General.NadeTracer.Save(j);
			General.BulletImpact.Save(j);

			constexpr auto& Crosshair = Menu::Get.Colors.General.Crosshair;
			{
				Crosshair.X_Shape.Save(j);
				Crosshair.Plus_Shape.Save(j);
				Crosshair.Circle_Shape.Save(j);
			}

			General.Hitmarker.Save(j);
			General.Tracers.Save(j);
			General.Fog.Save(j);
			General.Trails.Save(j);
			General.HudColor.Save(j);
			General.Net_graph.Save(j);
		}
	}

	constexpr auto& Keys = Menu::Get.Keys;
	{
		Keys.RageBot.Save(j);
		Keys.AirStuck.Save(j);
		Keys.Edgejump.Save(j);
		Keys.Bunnyhop.Save(j);
		Keys.Triggerbot.Save(j);
		Keys.Autostrafe.Save(j);
		Keys.CircleStrafe.Save(j);
		Keys.Unload.Save(j);
		Keys.PanicKey.Save(j);
		Keys.KnifeBot.Save(j);
	}

	for (auto&& WeaponInfo : SkinChanger::WeaponsInfo)
	{
		auto data = WeaponInfo.data;

		j["Skin Changer"].emplace(data.first, data.second);
	}

	for (auto&& data : VoiceChat::SoundList)
	{
		j["VoiceChat"].emplace(data.Soundname, data.KeyBind);
	}

	ImGuiStyle& style = ImGui::GetStyle();

	for (int i = 0; i < ImGuiCol_COUNT; i++)
		for (auto t = 0; t < 4; t++)
			j["Menu"].emplace(ImGui::GetStyleColorName(i) + std::to_string(t), ((std::array<float, 4>&)style.Colors[i])[t]);

	for (auto& Font : ImGui::GetIO().Fonts->Fonts)
	{
		j["FontsSize"].emplace(Font->GetDebugName(), Font->FontSize);
	}

	Menu::Get.Menu.FontID.Save(j);

	std::ofstream o(std::string("ZE\\ZE.") + Version + ".json");

	o << std::setw(4) << j << std::endl;

	j.clear();
}

void Config::LoadConfig()
{
	std::ifstream input_file = std::ifstream(std::string("ZE\\ZE.") + Version + ".json");

	if (!input_file.good())
		return;

	try
	{
		input_file >> j;
	}
	catch (...)
	{
		input_file.close();
		return;
	}

	constexpr auto& Aimbot = Menu::Get.Aimbot;
	{
		Aimbot.LAC_Bypass.Load(j);
		Aimbot.Ragebot.Enabled.Load(j);
		Aimbot.Ragebot.AutoStop.Load(j);
		Aimbot.Ragebot.Predicted.Load(j);
		Aimbot.Ragebot.AutoShoot.Load(j);
		Aimbot.Ragebot.AutoCrouch.Load(j);
		Aimbot.Ragebot.FriendlyFire.Load(j);

		Aimbot.Ragebot.TargetSelection.Load(j);
		Aimbot.Ragebot.SmoothVal.Load(j);
		Aimbot.Ragebot.Timeout.Load(j);
		Aimbot.Ragebot.HitBox.Load(j);
		Aimbot.Ragebot.Silent.Load(j);
		Aimbot.Ragebot.Usage.Load(j);
		Aimbot.Ragebot.FOV.Load(j);
	}

	constexpr auto& General = Menu::Get.General;
	{
		General.ZeStuff.Filter.Load(j);

		General.ZeStuff.AutoLaser.Load(j);
		General.ZeStuff.Wireframe_lasers.Load(j);
		General.ZeStuff.Special_items_IgnoreZ.Load(j);
	}

	constexpr auto& Visuals = Menu::Get.Visuals;
	{
		Visuals.WorldTexture.Load(j);
		Visuals.ZE_LasersTexture.Load(j);
		Visuals.BackgroundTexture.Load(j);

		Visuals.Box.Load(j);
		Visuals.Name.Load(j);
		Visuals.Health.Load(j);
		Visuals.Weapon.Load(j);
		Visuals.Skeleton.Load(j);
		Visuals.SnapLine.Load(j);

		Visuals.BoxType.Load(j);
		Visuals.HealthType.Load(j);
		Visuals.NameFont.Load(j);
		Visuals.HealthFont.Load(j);
		Visuals.WeaponFont.Load(j);
		Visuals.HP_SpriteScale.Load(j);
		Visuals.WeaponRenderingMode.Load(j);

		Visuals.KillEffect_Type.Load(j);
		Visuals.ScopeLen.Load(j);

		Visuals.SkyBoxIndex.Load(j);
		Visuals.NightVision.Load(j);
		Visuals.Hands.Load(j);
		Visuals.NightMode.Load(1);
		Visuals.MuzzleFlash.Load(j);
		Visuals.FieldOfView.Load(j);
		Visuals.ViewModelFov.Load(j);
		Visuals.FlashPercentage.Load(j);
		Visuals.DLights.Load(j);

		Visuals.FullBright.Load(j);
		Visuals.NoFog.Load(j);
		Visuals.Disable_Fire_Particles.Load(j);
		Visuals.Disable_Players_MuzzleFlash.Load(j);
		Visuals.NoFlashLight.Load(j);
		Visuals.NoSmoke.Load(j);
		Visuals.NadeTracer.Load(j);
		Visuals.NoVisualRecoil.Load(j);

		constexpr auto& Glow = Menu::Get.Visuals.Glow;
		{
			Glow.Enabled.Load(j);
		}

		constexpr auto& FOG = Menu::Get.Visuals.FOG;
		{
			FOG.Enabled.Load(j);

			FOG.end.Load(j);
			FOG.start.Load(j);
			FOG.maxdensity.Load(j);
		}

		constexpr auto& Trails = Menu::Get.Visuals.Trails;
		{
			Trails.Enabled.Load(j);

			Trails.Width.Load(j);
			Trails.RenderMode.Load(j);
			Trails.EndWidth.Load(j);
			Trails.LifeTime.Load(j);
			Trails.SpriteTexture.Load(j);
		}

		constexpr auto& Tracers = Menu::Get.Visuals.Tracers;
		{
			Tracers.Enabled.Load(j);

			Tracers.SpriteTexture.Load(j);
			Tracers.PointScale.Load(j);
			Tracers.m_flLife.Load(j);
			Tracers.m_flWidth.Load(j);
			Tracers.m_flEndWidth.Load(j);
			Tracers.m_flFadeLength.Load(j);
			Tracers.m_flAmplitude.Load(j);
			Tracers.m_flSpeed.Load(j);
			Tracers.m_nSegments.Load(j);
			Tracers.m_nFlags.Load(j);
		}

		constexpr auto& Weather = Menu::Get.Visuals.Weather;
		{
			Weather.Enabled.Load(j);

			Weather.Type.Load(j);
			Weather.Width.Load(j);
			Weather.Speed.Load(j);
			Weather.Radius.Load(j);
			Weather.Length.Load(j);
		}

		constexpr auto& Debug = Menu::Get.Visuals.Debug;
		{
			Debug.Enabled.Load(j);

			Debug.INDEX.Load(j);
			Debug.Classname.Load(j);
			Debug.ModelName.Load(j);
			Debug.ModelIndex.Load(j);
			Debug.Velocity.Load(j);
			Debug.Origin.Load(j);
			Debug.Filter.Load(j);

			Debug.MaxDistance.Load(j);
		}

		constexpr auto& Hitmarker = Menu::Get.Visuals.Hitmarker;
		{
			Hitmarker.Enabled.Load(j);

			Hitmarker.Time.Load(j);
			Hitmarker.Length.Load(j);
			Hitmarker.Gap.Load(j);
			Hitmarker.HitSound.Load(j);
			Hitmarker.Mode.Load(j);
			Hitmarker.Overlay.Load(j);
		}

		constexpr auto& DamageIndicator = Menu::Get.Visuals.DamageIndicator;
		{
			DamageIndicator.Enabled.Load(j);
		}

		constexpr auto& Chams = Menu::Get.Visuals.Chams;
		{
			constexpr auto& Player = Chams.Player;
			{
				Player.Enabled.Load(j);

				Player.Wireframe.Load(j);
				Player.Ignorez.Load(j);

				Player.Type.Load(j);
			}

			constexpr auto& Weapon = Chams.Weapon;
			{
				Weapon.Enabled.Load(j);

				Weapon.Wireframe.Load(j);
				Weapon.Ignorez.Load(j);

				Weapon.Type.Load(j);
			}
		}

		constexpr auto& ViewModel = Menu::Get.Visuals.ViewModel;
		{
			ViewModel.Enabled.Load(j);

			ViewModel.Origin.Load(j);
			ViewModel.Angles.Load(j);
		}

		constexpr auto& Crosshair = Menu::Get.Visuals.Crosshair;
		{
			Crosshair.Recoil.Load(j);

			constexpr auto& X_Shape = Crosshair.X_Shape;
			{
				X_Shape.Enabled.Load(j);

				X_Shape.Length.Load(j);
				X_Shape.Gap.Load(j);
			}

			constexpr auto& Plus_Shape = Crosshair.Plus_Shape;
			{
				Plus_Shape.Enabled.Load(j);

				Plus_Shape.Length.Load(j);
				Plus_Shape.Gap.Load(j);
			}

			constexpr auto& Circle_Shape = Crosshair.Circle_Shape;
			{
				Circle_Shape.Enabled.Load(j);

				Circle_Shape.Radius.Load(j);
				Circle_Shape.Segments.Load(j);
			}
		}
	}

	constexpr auto& Misc = Menu::Get.Misc;
	{
		Misc.NoMotd.Load(j);
		Misc.NoDrug.Load(j);
		Misc.NoShake.Load(j);
		Misc.MuteRadio.Load(j);
		Misc.NoRecoil.Load(j);
		Misc.NoSpread.Load(j);
		Misc.AutoPisol.Load(j);
		Misc.Faststop.Load(j);
		Misc.Disable_Game_Console_Warnings.Load(j);
		Misc.Fastrun.Load(j);
		Misc.AutoKevlar.Load(j);
		Misc.Bunnyhop.Load(j);
		Misc.Autostrafe.Load(j);
		Misc.CircleStrafe.Load(j);
		Misc.Edgejump.Load(j);
		Misc.FastLadder.Load(j);
		Misc.AntiAFKkick.Load(j);
		Misc.Sv_Pure_Bypass.Load(j);
		Misc.KillMessage.Load(j);

		Misc.Ragdoll_Force.Load(j);
		Misc.m_vecCameraOffset.Load(j);
		Misc.Bunnyhop_Perfect_Rate.Load(j);

		constexpr auto& StrafeOptimizer = Misc.StrafeOptimizer;
		{
			StrafeOptimizer.Enabled.Load(j);

			StrafeOptimizer.Desired_Gain.Load(j);
			StrafeOptimizer.Required_Speed.Load(j);
			StrafeOptimizer.Greatest_Possible_Strafe_Angle.Load(j);
		}

		constexpr auto& Triggerbot = Misc.Triggerbot;
		{
			Triggerbot.Enabled.Load(j);

			Triggerbot.Filter.Load(j);

			Triggerbot.Usage.Load(j);
		}

		constexpr auto& KnifeBot = Misc.KnifeBot;
		{
			KnifeBot.Enabled.Load(j);

			KnifeBot.Method.Load(j);
			KnifeBot.Usage.Load(j);
		}

		constexpr auto& ClanTag = Misc.ClanTag;
		{
			ClanTag.Changer.Load(j);
			ClanTag.Stealer.Load(j);

			ClanTag.Delay.Load(j);
		}

		constexpr auto& Sounds = Misc.Sounds;
		{
			Sounds.MusicVolume.Load(j);
			Sounds.WeaponsAudio.Load(j);
			Sounds.FootStepsVolume.Load(j);
		}

		constexpr auto& SteamIDSpoofer = Misc.SteamIDSpoofer;
		{
			SteamIDSpoofer.Enabled.Load(j);

			SteamIDSpoofer.Random.Load(j);

			SteamIDSpoofer.SteamID.Load(j);
		}

		Menu::Get.Menu.FontID.Load(j);

		Menu::Get.Menu.Key.Load(j);
	}

	constexpr auto& Colors = Menu::Get.Colors;
	{
		constexpr auto& PlayerEsp = Menu::Get.Colors.PlayerEsp;
		{
			PlayerEsp.Box.Load(j);

			PlayerEsp.Name.Load(j);

			PlayerEsp.Health.Load(j);

			PlayerEsp.Weapon.Load(j); 

			PlayerEsp.SnapLine.Load(j);

			PlayerEsp.Skeleton.Load(j);

			PlayerEsp.DLights.Load(j);
		}

		constexpr auto& Chams = Menu::Get.Colors.Chams;
		{
			Chams.Player.Load(j);

			Chams.Weapon.Load(j);
		}

		constexpr auto& Glow = Menu::Get.Colors.Glow;
		{
			Glow.Player.Load(j);
		}

		constexpr auto& General = Menu::Get.Colors.General;
		{
			General.SkyBox.Load(j);
			General.World.Load(j);
			General.DamageIndicator.Load(j);
			General.Nightvision.Load(j);
			General.NadeTracer.Load(j);
			General.BulletImpact.Load(j);

			constexpr auto& Crosshair = Menu::Get.Colors.General.Crosshair;
			{
				Crosshair.X_Shape.Load(j);
				Crosshair.Plus_Shape.Load(j);
				Crosshair.Circle_Shape.Load(j);
			}

			General.Hitmarker.Load(j);
			General.Tracers.Load(j);
			General.Fog.Load(j);
			General.Trails.Load(j);
			General.HudColor.Load(j);
			General.Net_graph.Load(j);
		}
	}

	constexpr auto& Keys = Menu::Get.Keys;
	{
		Keys.RageBot.Load(j);
		Keys.AirStuck.Load(j);
		Keys.Edgejump.Load(j);
		Keys.Bunnyhop.Load(j);
		Keys.Triggerbot.Load(j);
		Keys.Autostrafe.Load(j);
		Keys.CircleStrafe.Load(j);
		Keys.Unload.Load(j);
		Keys.PanicKey.Load(j);
		Keys.KnifeBot.Load(j);
	}

	if (auto SkinChanger = j["Skin Changer"]; SkinChanger.empty() == NULL)
	{
		for (auto&& WeaponInfo : SkinChanger::WeaponsInfo)
		{
			auto& data = WeaponInfo.data;

			if (auto pData = SkinChanger.find(data.first); pData != SkinChanger.end())
			{
				int Value = (*pData).get<int>();

				if (IsGoodItem(WeaponInfo.models.size(), Value, 0))
					data.second = Value;
			}
		}
	}

	if (auto Menu = j["Menu"]; Menu.empty() == NULL)
	{
		ImGuiStyle& style = ImGui::GetStyle();

		for (int i = 0; i < ImGuiCol_COUNT; i++)
		{
			for (auto t = 0; t < 4; t++)
			{
				if (auto pData = Menu.find(ImGui::GetStyleColorName(i) + std::to_string(t)); pData != Menu.end())
					((std::array<float, 4>&)style.Colors[i])[t] = (*pData).get<float>();
			}
		}
	}

	if (auto FontsSize = j["FontsSize"]; FontsSize.empty() == NULL)
	{
		for (auto& Font : ImGui::GetIO().Fonts->Fonts)
		{
			if (auto pData = FontsSize.find(Font->GetDebugName()); pData != FontsSize.end())
				Font->FontSize = (*pData).get<float>();
		}
	}

	if (auto VoiceChatJson = j["VoiceChat"]; VoiceChatJson.empty() == NULL)
	{
		for (auto&& data : VoiceChat::SoundList)
		{
			if (auto pData = VoiceChatJson.find(data.Soundname); pData != VoiceChatJson.end())
			{
				data.KeyBind = (*pData).get<int>();
			}
		}
	}

	Menu::Get.Menu.FontID.Load(j);

	input_file.close();
	
	j.clear();

	IsGoodItem(SkyBoxes.size(), Menu::Get.Visuals.SkyBoxIndex);
	IsGoodItem(ScopeOverlays.size(), Menu::Get.Visuals.ScopeLen);
	IsGoodItem(HitSounds.size(), Menu::Get.Visuals.Hitmarker.HitSound);
	IsGoodItem(HitmarkerOverlays.size(), Menu::Get.Visuals.Hitmarker.Overlay);
	IsGoodItem(TrailMaterials.first.size(), Menu::Get.Visuals.Trails.SpriteTexture);
}

