#include "SDK.h"
#include "Assorted.h"
#include "SkinChanger.h"

std::vector<SkinChanger::SC_ModelInfo>SkinChanger::WeaponsInfo;

void SkinChanger::Run(CBaseEntity* pLocal) noexcept
{
	auto ViewModel = EntityList->GetClientEntityFromHandle(pLocal->GetViewModel());
	if (!ViewModel) return;

	auto ActiveWeapon = pLocal->GetActiveWeapon();
	if (!ActiveWeapon) return;

	auto ActiveWeaponModel = ActiveWeapon->GetModel();
	if (!ActiveWeaponModel) return;

	auto ActiveWeaponModelPath = ActiveWeaponModel->GetName();

	for (auto& WeaponInfo : WeaponsInfo) {

		auto& data = WeaponInfo.data;

		auto& Models = WeaponInfo.models;

		if (strstr(ActiveWeaponModelPath, data.first.data()) && Models.size() > 1) {

			if (auto Index = data.second; Index > 0)
			{
				auto ModelIndex = Models[Index].second;

				if (ModelIndex != -1 && ViewModel->GetModel() != ModelInfo->GetModel(ModelIndex))
				{
					ViewModel->SetModelByIndex(ModelIndex);
				}
			}

			break;
		}
	}
}

void SkinChanger::PrecacheModels() noexcept
{
	char TempPath[MAX_PATH];

	for (auto& WeaponInfo : WeaponsInfo)
	{
		auto& data = WeaponInfo.data;

		auto& Models = WeaponInfo.models;

		for (auto i = 0; i < Models.size(); i++)
		{
			auto& Model = Models[i];

			strcpy(TempPath, "models/weapons/");

			if (i == 0) continue;

			strcat(TempPath, data.first.data());

			strcat(TempPath, "/");

			strcat(TempPath, Model.first.data());

			strcat(TempPath, ".mdl");

			PrecacheModel(TempPath);

			Model.second = ModelInfo->GetModelIndex(TempPath);
		}
	}
}

void SkinChanger::PrepareFiles() noexcept
{
	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "models\\weapons");

	if (DirectoryExists(TempPath)) {

		for (const auto& entry : FileSystem::directory_iterator(TempPath))
		{
			size_t Found;

			auto FilePath = entry.path().string();

			if (FileSystem::status(FilePath).type() == FileSystem::file_type::directory)
			{
				if (Found = FilePath.find_last_of("\\"); Found != std::string::npos)
				{
					strcpy(TempPath, (const char*)&FilePath[Found + 1]);

					SkinChanger::WeaponsInfo.push_back(SkinChanger::SC_ModelInfo(TempPath));

					SkinChanger::WeaponsInfo[SkinChanger::WeaponsInfo.size() - 1].models.push_back({ "Default", -1 });
				}

				for (const auto& entry : FileSystem::directory_iterator(FilePath))
				{
					auto FilePath = entry.path().string();

					if (Found = FilePath.find_last_of("\\"); Found != std::string::npos)
					{
						strcpy(TempPath, (const char*)&FilePath[Found + 1]);

						if (Found = std::string(TempPath).find(".mdl"); Found != std::string::npos)
						{
							TempPath[Found] = 0;

							SkinChanger::WeaponsInfo[SkinChanger::WeaponsInfo.size() - 1].models.push_back({ TempPath, -1 });
						}
					}
				}
			}
		}
	}
}
