#pragma once

#ifndef CAimbot_H
#define CAimbot_H

class CBaseEntity;
class CUserCmd;

namespace Aimbot
{
	extern int Action;

	extern int LoopCount;

	extern float LastProcessTime;

	extern CBaseEntity* BestTarget;

	extern bool NoSpreadWasCalled, NoRecoilWasCalled;

	CBaseEntity* GetClosestEnemy(CBaseEntity* pLocal) noexcept;

	void Run(CBaseEntity* pLocal, CUserCmd* cmd, bool& bSendPacket) noexcept;

	enum LAC_AIMBOT_BYPASS_ACTIONS
	{
		None,
		SetViewAngles,
		SetViewAngles2,
		ForceAttackOnNextTick
	};
};

Vector Predicted_Position(CBaseEntity* Entity, bool pLocal = false) noexcept;

void GetMaterialParameters(int iMaterial, float& flPenetrationModifier, float& flDamageModifier);

float ScaleDamage(float damage, int hitgroup, int armor_value, bool has_helmet, float weapon_armor_ratio);

bool TraceToExit(Vector& start, Vector& dir, Vector& end, float flStepSize, float flMaxDistance);

bool CanPenetratePoint(CBaseEntity* pLocal, CBaseEntity* target_player, Vector shootAngles);

namespace LAC_Bypass
{
	enum class Stages
	{
		NONE,
		PRE_FIRE,
		FIRE,
		POST_FIRE
	};

	extern Stages NEXT_TICK;

	extern QAngle AIM_ANGLES;

	extern void PostCreateMove(
		CBaseEntity* pLocal,
		CUserCmd* cmd,
		bool& bSendPacket
	);
}

#endif
