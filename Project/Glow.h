#ifndef GLOW
#define GLOW

namespace Glow
{
	void RenderEffect() noexcept;
	void ClearObjects() noexcept;
};

#endif
