#include <Windows.h>
#include "Config.h"
#include <d3d9.h>

#include "SDK.h"
#include "Hooks.h"
#include "Client.h"
#include "Interfaces.h"
#include "DirectXDevice.hpp"

VMTHook* Hooks::Panel_Table;
VMTHook* Hooks::Engine_Table;
VMTHook* Hooks::Client_Table;
VMTHook* Hooks::Surface_Table;
VMTHook* Hooks::ClientMod_Table;
VMTHook* Hooks::D3Ddevice_Table;
VMTHook* Hooks::NetChannel_Table;
VMTHook* Hooks::RenderView_Table;
VMTHook* Hooks::EngineSound_Table;
VMTHook* Hooks::ModelRender_Table;
VMTHook* Hooks::GameMovement_Table;
VMTHook* Hooks::BSPTreeQuery_Table;
VMTHook* Hooks::MatSystemOther_Table;
VMTHook* Hooks::GameEventManager_Table;

IDirect3DDevice9* DirectXDevice{ nullptr };

#include "General.h"
#include "NetvarHooks.h"

void Initialize_Loactions() noexcept {

	if (auto r_shadows(Cvar->FindVar("r_shadows")); r_shadows)
		r_shadows->SetValue(0);

	if (auto r_3dsky(Cvar->FindVar("r_3dsky")); r_3dsky)
		r_3dsky->SetValue(0);

	m_fEffects = new NetVarHookInfo("DT_BaseEntity", "m_fEffects", m_fEffectsHook); assert(m_fEffects->Original_Fn);

	m_VecForceProxy = new NetVarHookInfo("DT_CSRagdoll", "m_vecForce", VecForce); assert(m_VecForceProxy->Original_Fn);

	m_iHealthProxy = new NetVarHookInfo("DT_BasePlayer", "m_iHealth", m_iHealthHook); assert(m_iHealthProxy->Original_Fn);

	CLC_ListenEvents_Table = *(CLC_ListenEvents**)(Tools::FindPattern("engine.dll", "C7 01 ? ? ? ? 85 C0") + 0x2);

	Warning = new DetourHookInfo(uintptr_t(GetProcAddress(GetModuleHandle("tier0.dll"), "Warning")), Hooked_Warning, 2);

	DrawModel = new DetourHookInfo(Tools::FindPattern("client.dll", "55 8B EC 83 EC 30 53 33 D2"), Hooked_DrawModel, 0);

	CLC_RespondCvarValue_Table = *(CLC_RespondCvarValue**)(Tools::FindPattern("engine.dll", "8B 01 52 8B 40 34") - 0x1E);

	SetViewModel = new DetourHookInfo(Tools::FindPattern("client.dll", "C2 04 00 56 57 8B F9") + 0x3, Hooked_SetViewModel, 4);

	ReadWavFile = new DetourHookInfo(Tools::FindPattern("engine.dll", "51 56 68 ? ? ? ? FF 75 08") - 0x3, Hooked_ReadWavFile, 4);

	CheckWhitelist = new DetourHookInfo(Tools::FindPattern("engine.dll", "55 8B EC 83 3D ? ? ? ? ? 7E 5E"), Hooked_CheckWhitelist, 4);

	CalcViewModelView = new DetourHookInfo(Tools::FindPattern("client.dll", "55 8B EC 83 EC 24 8B 55 10"), Hooked_CalcViewModelView, 0);

	R_LevelInit = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "6A 13 8B C8") - 0xA), Hooked_R_LevelInit, 1);

	ConsistencyCheck = new DetourHookInfo(Tools::FindPattern("engine.dll", "81 EC A0 02 ? ? 53 8B D9") - 0x3, Hooked_ConsistencyCheck, 3);

	FireEventIntern = new DetourHookInfo(Tools::FindPattern("engine.dll", "55 8B EC 83 EC 34 53 8B 5D 08 57"), Hooked_FireEventIntern, 0);

	ProcessStringCmd = new DetourHookInfo(Tools::FindPattern("engine.dll", "55 8B EC 80 B9 ? ? ? ? ? 74 2F"), Hooked_ProcessStringCmd, 4);

	GetPMaterial = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 8D 5F F8")), Hooked_GetPMaterial, 0);

	SimulateEntities = new DetourHookInfo(Tools::FindPattern("client.dll", "83 EC 10 8B 0D ? ? ? ? 53 56") - 0x3, Hooked_SimulateEntities, 0);

	GetCvarValue = new DetourHookInfo(Tools::FindPattern("engine.dll", "55 8B EC 81 EC ? ? ? ? 56 57 8B 7D 08 89 4D FC"), Hooked_GetCvarValue, 3);

	CheckCRCs = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "E8 ? ? ? ? 83 C4 04 84 C0 75 21")), Hooked_CheckCRCs, 0);

	GetFgColor = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 8B 45 08 5D C2 04 00")), Hooked_GetFgColor, 0);

	ProcessFixAngle = new DetourHookInfo(Tools::FindPattern("engine.dll", "55 8B EC 8B 45 08 83 EC 08 F3 0F 10 15 ? ? ? ?"), Hooked_ProcessFixAngle, 0);

	C_ParticleSmokeGrenade = new DetourHookInfo(Tools::FindPattern("client.dll", "55 8B EC 81 EC 00 01 ? ? 56 57 8B F9"), Hooked_SmokeGrenade_Start, 3);

	ProcessMuzzleFlashEvent = new DetourHookInfo(Tools::FindPattern("client.dll", "55 8B EC 83 EC 68 53 56 57 8B F9"), Hooked_ProcessMuzzleFlashEvent, 0);

	DrawSpriteModel = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 83 C4 38 83 FE 03")), Hooked_DrawSpriteModel, 2);

	OnScreenSizeChanged = new DetourHookInfo(Tools::FindPattern("vguimatsurface.dll", "50 64 89 25 ? ? ? ? 83 EC 10 56") - 0x10, Hooked_OnScreenSizeChanged, 4);

	CL_QueueDownload = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "E8 ? ? ? ? 83 C4 04 8B 8B ? ? ? ?")), Hooked_CL_QueueDownload, 7);

	CSteam3Client_InitiateConnection = new DetourHookInfo(Tools::FindPattern("engine.dll", "55 8B EC 83 EC 10 8B 45 1C"), Hooked_CSteam3Client_InitiateConnection, 0);

	WriteListenEventList = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "E8 ? ? ? ? 8B 4E 10 8D 55 A4")), Hooked_WriteListenEventList, 0);

	C_BaseAnimating_DrawModel = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 8D 5E FC")), Hooked_C_BaseAnimating_DrawModel, 0);

	CCSMapOverview_CanPlayerBeSeen = new DetourHookInfo(Tools::FindPattern("client.dll", "55 8B EC 51 53 56 8B D9 E8 ? ? ? ?"), Hooked_CCSMapOverview_CanPlayerBeSeen, 0);

	ClientModeShared_FireGameEvent = new DetourHookInfo(Tools::FindPattern("client.dll", "55 8B EC 81 EC ? ? ? ? 56 57 89 4D FC"), Hooked_ClientModeShared_FireGameEvent, 3);

	CTraceFilterSimple_ShouldHitEntity = new DetourHookInfo(Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 84 C0 74 3A 8B 06")), Hooked_CTraceFilterSimple_ShouldHitEntity, 1);

	CSpriteTrail_GetRenderOrigin = new DetourHookInfo(Tools::FindPattern("client.dll", "55 8B EC 57 FF 75 08 8B F9 E8 ? ? ? ? 83 7D 08 00 75 1B") - 0xD0, Hooked_CSpriteTrail_GetRenderOrigin, 2);

	ProcessGameEventListeners();
}

void Hooks::Initialize(HMODULE DLL) noexcept
{
	PerformCopy(DLL);

	Load_Other_Stuff = NULL;

	CMatRenderContext_Bind = new DetourHookInfo
	(
		Tools::FindPattern
		(
			"materialsystem.dll",
			"55 8B EC 53 57 8B F9 8B 4D 08"
		),
		Hooked_CMatRenderContext_Bind, 1
	);

	while (Load_Other_Stuff == NULL)
	{
		Sleep(10U);
	}

	Load::Once(hWindow, DirectXDevice);
	
	Config::LoadConfig(); Load::Repeatedly(); Initialize_Loactions();

	Client_Table = new VMTHook(BaseClientDLL);
	{
		Original_Shutdown = Client_Table->Hook<Shutdown_Type>(7, LevelShutdown); assert(Original_Shutdown);

		Original_CreateMove = Client_Table->Hook<CreateMove_Type>(21, Hooked_CreateMove); assert(Original_CreateMove);

		Original_FrameStageNotify = Client_Table->Hook<FrameStageNotify_Type>(35, FrameStageNotify); assert(Original_FrameStageNotify);

		Original_DispatchUserMessage = Client_Table->Hook<DispatchUserMessage_Type>(36, DispatchUserMessage); assert(Original_DispatchUserMessage);
	}

	ClientMod_Table = new VMTHook(ClientMode);
	{
		Original_OverrideView = ClientMod_Table->Hook<OverrideView_Type>(16, OverrideView); assert(Original_OverrideView);

		Original_GetViewModelFov = ClientMod_Table->Hook<GetViewModelFov_Type>(32, GetViewModelFov); assert(Original_GetViewModelFov);

		Original_DoPostScreenSpaceEffects = ClientMod_Table->Hook<DoPostScreenSpaceEffects_Type>(39, DoPostScreenSpaceEffects); assert(Original_DoPostScreenSpaceEffects);
	}

	D3Ddevice_Table = new VMTHook(DirectXDevice);
	{
		Original_Reset = D3Ddevice_Table->Hook<Reset_Fn>(16, Reset); assert(Original_Reset);

		Original_EndScane = D3Ddevice_Table->Hook<EndScane_Fn>(42, EndScane); assert(Original_EndScane);
	}

	Panel_Table = new VMTHook(Panel);
	{
		Original_PaintTraverse = Panel_Table->Hook<PaintTraverse_Type>(41, PaintTraverse); assert(Original_PaintTraverse);
	}

	ModelRender_Table = new VMTHook(MdlRender);
	{
		Original_DrawModelExecute = ModelRender_Table->Hook<DrawModelExecute_Type>(19, DrawModelExecute); assert(Original_DrawModelExecute);
	}

	EngineSound_Table = new VMTHook(EngineSound);
	{
		Original_EmitSound = EngineSound_Table->Hook<EmitSound_Type>(4, Hooked_EmitSound);
	}

	if (hWindow) Original_Wnd_Proc = WNDPROC((SetWindowLongPtr(hWindow, GWLP_WNDPROC, LONG_PTR(Wnd_Proc)))); assert(Original_Wnd_Proc);
}

void Hooks::Uninitialize() noexcept
{
	Unload::PreUnHook(WriteListenEventList->Original);

	// Netvars \\

	delete m_fEffects;

	delete m_iHealthProxy;

	delete m_VecForceProxy;

	// Hooks

	delete Warning;

	delete DrawModel;

	delete EmitSound;

	delete CheckCRCs;

	delete GetFgColor;

	delete R_LevelInit;

	delete ReadWavFile;

	delete GetCvarValue;

	delete GetPMaterial;

	delete SetViewModel;

	delete CheckWhitelist;

	delete DrawSpriteModel;

	delete FireEventIntern;

	delete ProcessFixAngle;

	delete ProcessStringCmd;

	delete SimulateEntities;

	delete ConsistencyCheck;

	delete CL_QueueDownload;

	delete CalcViewModelView;

	delete OnScreenSizeChanged;

	delete WriteListenEventList;

	delete C_ParticleSmokeGrenade;

	delete CMatRenderContext_Bind;

	delete ProcessMuzzleFlashEvent;

	delete C_BaseAnimating_DrawModel;

	delete CSpriteTrail_GetRenderOrigin;

	delete CCSMapOverview_CanPlayerBeSeen;

	delete ClientModeShared_FireGameEvent;

	delete CSteam3Client_InitiateConnection;

	delete CTraceFilterSimple_ShouldHitEntity;

	delete Panel_Table;

	delete Engine_Table;

	delete Client_Table;

	delete Surface_Table;

	delete ClientMod_Table;

	delete D3Ddevice_Table;

	delete RenderView_Table;

	delete EngineSound_Table;

	delete NetChannel_Table;

	delete ModelRender_Table;

	delete GameMovement_Table;

	delete BSPTreeQuery_Table;

	delete MatSystemOther_Table;

	delete GameEventManager_Table;

	if (hWindow) SetWindowLongPtr(hWindow, GWLP_WNDPROC, LONG_PTR(Original_Wnd_Proc));

	Unload::PostUnHook();
}

void mmcopy(void* address, const void* value, size_t bytes)
{
	static unsigned long Protection_Backup{ 0 };

	VirtualProtect(address, bytes, PAGE_EXECUTE_READWRITE, &Protection_Backup);

	memcpy(address, value, bytes);

	VirtualProtect(address, bytes, Protection_Backup, nullptr);
}
