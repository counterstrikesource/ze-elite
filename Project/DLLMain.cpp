#include "SDK.h"
#include <string>
#include <fstream>
#include <intrin.h>
#include "include.h"

using namespace std;

CUserCmd* GlobalVars::cmd{ nullptr };

DWORD WINAPI Main_Function(void* DLL)
{
	//AllocConsole();

	//SetConsoleTitleA("ZE-Elite");

	//freopen("CONIN$"), "r"), stdin);

	//freopen("CONOUT$"), "w"), stdout);

	while (!GetModuleHandleA("serverbrowser.dll"))
		Sleep(200);

	Menu::AssignVariables(); Interfaces::Initialize(); Hooks::Initialize((HMODULE)DLL);

	static bool PressedPanicKey{ false };

	while (1)
	{
		auto IsPanicKeyPressed = IsVirtualKeyPressed(Menu::Get.Keys.PanicKey);

		if (IsPanicKeyPressed && !PressedPanicKey)
		{
			Menu::Get.General.Panic = !Menu::Get.General.Panic;

			PressedPanicKey = !PressedPanicKey;
		}
		else if (!IsPanicKeyPressed && PressedPanicKey)
		{
			PressedPanicKey = !PressedPanicKey;
		}

		if (IsVirtualKeyPressed(Menu::Get.Keys.Unload) || Menu::Get.General.Unload)
		{
			Menu::Get.General.Unload = true;

			Sleep(50U);

			break;
		}

		Sleep(100U);
	}

	Sleep(100U);

	Hooks::Uninitialize(); 

	//FreeConsole();

	Sleep(2000U);

	//SendMessageA(FindWindowA(NULL, "ZE-Elite"), WM_CLOSE, NULL, NULL);

	FreeLibraryAndExitThread((HMODULE)DLL, EXIT_SUCCESS);
}

BOOL __stdcall DllMain
(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD     fdwReason,
	_In_ LPVOID    lpvReserved
)
{
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		if (hinstDLL)
		{
			DisableThreadLibraryCalls(hinstDLL);
		}

		CreateThread(NULL, NULL, Main_Function, hinstDLL, NULL, NULL);
	}

	return TRUE;
}
