#pragma once

enum modtype_t
{
	mod_bad = 0,
	mod_brush,
	mod_sprite,
	mod_studio
};

class IVModelInfo
{
public:

	void* GetModel(int index)
	{
		using Type = void * (__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(this, 1)(this, index);
	}

	int	GetModelIndex(const char* name)
	{
		using Type = int(__thiscall*)(void*, const char*);

		return GetVirtualMethod<Type>(this, 2)(this, name);
	}

	const char* GetMdlName(const void* model)
	{
		using Type = const char* (__thiscall*)(void*, const void*);

		return GetVirtualMethod<Type>(this, 3)(this, model);
	}

	studiohdr_t* GetStudiomodel(const void* model)
	{
		using Type = studiohdr_t * (__thiscall*)(void*, const void*);

		return GetVirtualMethod<Type>(this, 28)(this, model);
	}
};