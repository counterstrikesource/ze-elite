#include "SDK.h"
#include "CvarQuery.h"

//Credits to TraitCore <3
#define Bits_32
#include "\
Redirection_Manager/\
Redirection_Manager.hpp"

#include "General.h"

#include "Esp.h"
#include "Glow.h"
#include "HP_Bar.h"
#include "Trails.h"
#include "Assorted.h"
#include "VoiceChat.h"
#include "SkinChanger.h"
#include "WorldTextures.h"
#include "MaterialManager.h"
#include "SteamID/steamid.h"
#include "glow_outline_effect.h"

HWND hWindow;

char Load_Other_Stuff;

WNDPROC Original_Wnd_Proc;

CLC_ListenEvents* CLC_ListenEvents_Table;

CLC_RespondCvarValue* CLC_RespondCvarValue_Table;

std::vector<DesiredConVarsValueInfo> DesiredConVarsValue;

EmitSound_Type Original_EmitSound;
OverrideView_Type Original_OverrideView;
PaintTraverse_Type Original_PaintTraverse;
GetViewModelFov_Type Original_GetViewModelFov;
DrawModelExecute_Type Original_DrawModelExecute;
DoPostScreenSpaceEffects_Type Original_DoPostScreenSpaceEffects;

extern void mmcopy
(
	void* address,
	const void* value,
	size_t bytes
);

IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND, UINT, WPARAM, LPARAM);

DetourHookInfo::DetourHookInfo(uintptr_t Addr, PVOID Fn, size_t offset) : Location(PVOID(Addr)) {

	Original = nullptr;

	if (Addr) Redirection_Manager::Redirect_Function(Original, offset, Location, 1, Fn);
}

DetourHookInfo::~DetourHookInfo() {

	if (Original) Redirection_Manager::Restore_Function(1, Location, Original, 0);
}

void __fastcall PaintTraverse(PVOID pPanels, int edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce) noexcept
{
	Original_PaintTraverse(pPanels, vguiPanel, forceRepaint, allowForce);

	static bool WasEnabled{ false }, Unload_Process_WasDone{ false };

	static unsigned int vguiFocusOverlayPanel(0);

	if (vguiFocusOverlayPanel == NULL)
	{
		const char* szName = Panel->GetName(vguiPanel);

		if (szName[0] == 'F' && szName[5] == 'O' && szName[12] == 'P')
		{
			vguiFocusOverlayPanel = vguiPanel;

			gMat.Initialize();
		}
	}


	if (vguiFocusOverlayPanel == vguiPanel)
	{
		if (!Esp::Fonts_Were_Initialized)
		{
			Esp::Initialize();
		}

		Esp::Render();

		if (Menu::Get.General.Unload)
		{
			if (!Unload_Process_WasDone)
			{
				Panel->SetMouseInputEnabled(vguiPanel, false);

				Unload_Process_WasDone = !Unload_Process_WasDone;
			}
		}
		else
		{
			if (!WasEnabled && Menu::Get.Menu.On)
			{
				Panel->SetMouseInputEnabled(vguiPanel, true);

				WasEnabled = !WasEnabled;
			}
			else if (WasEnabled && !Menu::Get.Menu.On)
			{
				Panel->SetMouseInputEnabled(vguiPanel, false);

				WasEnabled = !WasEnabled;
			}
		}
	}
}

unsigned __int32 __stdcall Wnd_Proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) noexcept
{
	static auto Previous_Menu_Key_Pressed = false;

	auto Menu_Key_Pressed = wParam == Menu::Get.Menu.Key && uMsg == WM_KEYDOWN;

	if (!Previous_Menu_Key_Pressed && Menu_Key_Pressed)
	{
		Menu::Get.Menu.On = !Menu::Get.Menu.On;

		Previous_Menu_Key_Pressed = !Previous_Menu_Key_Pressed;
	}
	else if (Previous_Menu_Key_Pressed && !Menu_Key_Pressed)
	{
		Previous_Menu_Key_Pressed = !Previous_Menu_Key_Pressed;
	}

	if (Menu::Get.Menu.On)
	{
		ImGui_ImplWin32_WndProcHandler(hWnd, uMsg, wParam, lParam);

		return WM_CREATE;
	}

	auto Original = CallWindowProc(Original_Wnd_Proc, hWnd, uMsg, wParam, lParam);

	VoiceChat::Run();

	return Original;
}

void __stdcall DrawModelExecute(const void*& state, const ModelRenderInfo_t& pInfo, matrix3x4* pCustomBoneToWorld) noexcept
{
	auto LocalPlayer{ CBaseEntity::GetLocalPlayer() };
	if (!LocalPlayer)
		return;

	std::string_view ModelName = const_cast<model_t*>(pInfo.pModel)->GetName();

	if (auto pModelEntity{ GetEntity(pInfo.entity_index) }; pModelEntity && LocalPlayer->IsAlive())
	{
		if (Menu::Get.Visuals.Chams.Player.Enabled && ModelName.starts_with("models/player"))
		{
			if (pModelEntity->IsAlive() && pModelEntity->IsPlayer() && pModelEntity->IsEnemy(LocalPlayer))
			{
				if (auto Player_Material = gMat.Chams[Menu::Get.Visuals.Chams.Player.Type]; Player_Material)
				{
					if (Player_Material->GetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_WIREFRAME) != Menu::Get.Visuals.Chams.Player.Wireframe)
						Player_Material->SetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_WIREFRAME, Menu::Get.Visuals.Chams.Player.Wireframe);

					if (Player_Material->GetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_IGNOREZ) != Menu::Get.Visuals.Chams.Player.Ignorez)
						Player_Material->SetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_IGNOREZ, Menu::Get.Visuals.Chams.Player.Ignorez);

					ForceMaterial(Menu::Get.Colors.Chams.Player, Player_Material);

					Original_DrawModelExecute(MdlRender, state, pInfo, pCustomBoneToWorld);

					gMat.ResetMaterial();

					return;
				}
			}
		}

		if (Menu::Get.Visuals.Chams.Weapon.Enabled && ModelName.starts_with("models/weapons"))
		{
			if (pModelEntity->GetOwnerEntity() == nullptr && /* -> Never indicates that local player is the owner */ pModelEntity->GetOwner() != 255)  /* -> Ignore dropped weapons */ {

				if (auto Weapon_Material = gMat.Chams[Menu::Get.Visuals.Chams.Weapon.Type]; Weapon_Material)
				{
					if (Weapon_Material->GetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_WIREFRAME) != Menu::Get.Visuals.Chams.Weapon.Wireframe)
						Weapon_Material->SetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_WIREFRAME, Menu::Get.Visuals.Chams.Weapon.Wireframe);

					if (Weapon_Material->GetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_IGNOREZ) != Menu::Get.Visuals.Chams.Weapon.Ignorez)
						Weapon_Material->SetMaterialVarFlag(MaterialVarFlags::MATERIAL_VAR_IGNOREZ, Menu::Get.Visuals.Chams.Weapon.Ignorez);

					ForceMaterial(Menu::Get.Colors.Chams.Weapon, Weapon_Material);

					Original_DrawModelExecute(MdlRender, state, pInfo, pCustomBoneToWorld);

					gMat.ResetMaterial();

					return;
				}
			}
		}

	}

	Original_DrawModelExecute(MdlRender, state, pInfo, pCustomBoneToWorld);
}

void __fastcall OverrideView(void* _this, void* _edx, CViewSetup* pSetup) noexcept
{
	Original_OverrideView(_this, pSetup);

	const auto current_Fov = pSetup->GetFOV();

	if (Menu::Get.Visuals.FieldOfView NotEquals 0
		&& current_Fov NotEquals 40.0f
		&& current_Fov NotEquals 10.0f
		&& current_Fov NotEquals 15.0f)
	{
		pSetup->GetFOV() = current_Fov + Menu::Get.Visuals.FieldOfView;
	}
}

float __fastcall GetViewModelFov() noexcept
{
	return Original_GetViewModelFov() + Menu::Get.Visuals.ViewModelFov;
}

bool __stdcall DoPostScreenSpaceEffects(const CViewSetup* pSetup) noexcept
{
	Glow::RenderEffect();

	g_GlowObjectManager.RenderGlowEffects(const_cast<CViewSetup*>(pSetup), -1);

	return Original_DoPostScreenSpaceEffects(ClientMode, pSetup);
}

void __fastcall Hooked_EmitSound(void* ECX, void* EDX, void*& filter, int iEntIndex, int iChannel, const char* pSample,
	float flVolume, float flAttenuation, int iFlags, int iPitch, int iSpecialDSP,
	const Vector* pOrigin, const Vector* pDirection, CUtlVector< Vector >* pUtlVecOrigins, bool bUpdatePositions, float soundtime, int speakerentity)
{
	if (strstr(pSample, "weapons/")) { flVolume = Menu::Get.Misc.Sounds.WeaponsAudio * 0.01f; }
	else
		if (strstr(pSample, "footsteps")) { flVolume = Menu::Get.Misc.Sounds.FootStepsVolume * 0.01f; }

	Original_EmitSound(ECX, EDX, filter, iEntIndex, iChannel, pSample, flVolume, flAttenuation, iFlags, iPitch, iSpecialDSP, pOrigin, pDirection, pUtlVecOrigins, bUpdatePositions, soundtime, speakerentity);
}

// Detours \\

DetourHookInfo* Warning;
DetourHookInfo* CheckCRCs;
DetourHookInfo* DrawModel;
DetourHookInfo* EmitSound;
DetourHookInfo* GetFgColor;
DetourHookInfo* ReadWavFile;
DetourHookInfo* R_LevelInit;
DetourHookInfo* GetCvarValue;
DetourHookInfo* GetPMaterial;
DetourHookInfo* SetViewModel;
DetourHookInfo* CheckWhitelist;
DetourHookInfo* ProcessFixAngle;
DetourHookInfo* DrawSpriteModel;
DetourHookInfo* FireEventIntern;
DetourHookInfo* CL_QueueDownload;
DetourHookInfo* SimulateEntities;
DetourHookInfo* ProcessStringCmd;
DetourHookInfo* ConsistencyCheck;
DetourHookInfo* CalcViewModelView;
DetourHookInfo* OnScreenSizeChanged;
DetourHookInfo* WriteListenEventList;
DetourHookInfo* CMatRenderContext_Bind;
DetourHookInfo* C_ParticleSmokeGrenade;
DetourHookInfo* ProcessMuzzleFlashEvent;
DetourHookInfo* C_BaseAnimating_DrawModel;
DetourHookInfo* CSpriteTrail_GetRenderOrigin;
DetourHookInfo* ClientModeShared_FireGameEvent;
DetourHookInfo* CCSMapOverview_CanPlayerBeSeen;
DetourHookInfo* CSteam3Client_InitiateConnection;
DetourHookInfo* CTraceFilterSimple_ShouldHitEntity;

void Hooked_R_LevelInit()
{
	Load::Repeatedly();

	using Type = decltype(&Hooked_R_LevelInit);

	reinterpret_cast<Type>(R_LevelInit->Original)();
}

void __fastcall Hooked_CMatRenderContext_Bind(void* Self, void*, IMaterial* iMaterial, void* proxyData)
{
	if (Load_Other_Stuff == NULL) {

		Offsets();

		Prepare_Custom_Stuff();

		Assign_Glow_Textures();

		Load_Other_Stuff = 1;
	}

	SingleMaterialModulate
	(
		blueblacklargebeam,
		MaterialVarFlags::MATERIAL_VAR_WIREFRAME,
		Menu::Get.General.ZeStuff.Wireframe_lasers
	);

	SingleMaterialModulate
	(
		ItemsMat,
		MaterialVarFlags::MATERIAL_VAR_IGNOREZ,
		Menu::Get.General.ZeStuff.Special_items_IgnoreZ
	);

	World_Textures::NightModeModulate();
	World_Textures::WorldColorModulate();
	World_Textures::SkyBoxColorModulate();

	if (!iMaterial)
		goto End;

	if (Engine->IsInGame() == 0)
	{
		if (Menu::Get.Visuals.BackgroundTexture)
		{
			static IMaterial* Default_Background = nullptr;

			if (!Default_Background)
			{
				std::string_view Name = iMaterial->GetName();

				if (Name.starts_with("console/background01"))
				{
					Default_Background = iMaterial;
				}
			}

			if (iMaterial == Default_Background)
			{
				Background.AssignTexture(iMaterial, Menu::Get.Visuals.BackgroundTexture);

				goto End;
			}

		}
	}
	else
	{
		if (iMaterial == blueblacklargebeam.second - 0x90)
		{
			if (Menu::Get.Visuals.ZE_LasersTexture)
			{
				ListOfEffects.first.AssignTexture(iMaterial, Menu::Get.Visuals.ZE_LasersTexture);

				goto End;

			}
		}
		else if (Menu::Get.Visuals.Hands)
		{
			if (iMaterial == HandsMat)
			{
				Hands.AssignTexture(iMaterial, Menu::Get.Visuals.Hands);

				goto End;
			}
		}

		if (Menu::Get.Visuals.WorldTexture)
		{
			std::string_view TextureGroupName = iMaterial->GetTextureGroupName();

			if (TextureGroupName.starts_with(TEXTURE_GROUP_WORLD))
			{
				const char* szName = iMaterial->GetName();

				if (!strstr(szName, "floor") && !strstr(szName, "ground"))
					for (auto Blocked : BlockList)
						if (strstr(szName, Blocked))
							goto End;

				World.AssignTexture(iMaterial, Menu::Get.Visuals.WorldTexture);
			}
		}
	}


End:

	using Type = void(__thiscall*)(void*, IMaterial*, void*);

	reinterpret_cast<Type>(CMatRenderContext_Bind->Original)(Self, iMaterial, proxyData);
}

int __fastcall Hooked_C_BaseAnimating_DrawModel(CBaseEntity* ECX, void*, int flags)
{
	if (Menu::Get.Visuals.NoTeammates)
	{
		if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal && pLocal->IsAlive())
		{
			auto pEntity = (ECX - 4);

			if (!pEntity->IsEnemy(pLocal) && pLocal != pEntity)
				return 0;
		}
	}

	using Type = int(__thiscall*)(void*, int);

	return reinterpret_cast<Type>(C_BaseAnimating_DrawModel->Original)(ECX, flags);
}

const Vector& __fastcall Hooked_CSpriteTrail_GetRenderOrigin(void* ECX)
{
	using Type = const Vector& (__thiscall*)(void*);

	const auto& Origin = reinterpret_cast<Type>(CSpriteTrail_GetRenderOrigin->Original)(ECX);

	const_cast<Vector&>(Origin).z += 10.0f;

	return Origin;
}

void __fastcall Hooked_ProcessMuzzleFlashEvent(void* ECX)
{
	using Type = void(__thiscall*)(void*);

	if (Menu::Get.Visuals.Disable_Players_MuzzleFlash) return;

	reinterpret_cast<Type>(ProcessMuzzleFlashEvent->Original)(ECX);
}

void __fastcall Hooked_SetViewModel(void* ECX)
{
	using Type = void(__thiscall*)(void*);

	reinterpret_cast<Type>(SetViewModel->Original)(ECX);

	if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal)
	{
		if (auto ActiveWeapon = pLocal->GetActiveWeapon(); ECX == ActiveWeapon)
		{
			SkinChanger::Run(pLocal);
		}
	}
}

void Hooked_DrawSpriteModel
(
	CSprite* baseentity,
	void* psprite,
	const Vector& origin,
	float fscale,
	float frame,
	int rendermode,
	int r, int g, int b, int a,
	const Vector& forward,
	const Vector& right,
	const Vector& up,
	float flHDRColorScale
)
{
	using Type = decltype(&Hooked_DrawSpriteModel);

	if (HP_Bar::IsLocalEntity(baseentity)) const_cast<Vector&>(origin).z += 80.f;

	reinterpret_cast<Type>(DrawSpriteModel->Original)(baseentity, psprite, origin,
		fscale, frame, rendermode, r, g, b, a, forward, right, up, flHDRColorScale);
}

Color& __fastcall Hooked_GetFgColor(void* Self, void* EDX, void* UnknownArg)
{
	using Type = const char* (__thiscall*)(void*);

	static auto GePanelName = GetVirtualMethod<Type>(Self, *reinterpret_cast<BYTE*>(
		Tools::FindPattern("client.dll", "E8 ? ? ? ? EB 94") + 0x15) / 4);

	if (Engine->IsInGame())
	{
		if (strstr(GePanelName(Self), "Hud"))
		{
			reinterpret_cast<Color*>(Self)[34] = Menu::Get.Colors.General.HudColor;
		}
	}

	using Type1 = Color & (__thiscall*)(void*, void*);

	return reinterpret_cast<Type1>(GetFgColor->Original)(Self, UnknownArg);
}

bool __cdecl Hooked_ReadWavFile
(
	const char* pFilename,
	char*& pData,
	int& nDataBytes,
	int& wBitsPerSample,
	int& nChannels,
	int& nSamplesPerSec
)
{
	std::vector<unsigned char>Temp;

	unsigned char x;

	std::ifstream input(pFilename, std::ios::in | std::ios::binary);

	input >> std::noskipws;

	while (input >> x) {

		Temp.push_back(x);
	}

	if (auto size = Temp.size(); size)
	{
		nDataBytes = size;

		pData = new char[size];

		mmcopy(pData, Temp.data(), size);
	}

	return true;
}

void __stdcall Hooked_OnScreenSizeChanged(int OldWidht, int OldHeight) noexcept
{
	using Type = void(__thiscall*)(void*, int, int);

	reinterpret_cast<Type>(OnScreenSizeChanged->Original)(Surface, OldWidht, OldHeight);

	Esp::Initialize();
}

void __cdecl Hooked_SimulateEntities()
{
	using Type = decltype(&Hooked_SimulateEntities);

	if (Trails::Entity) Trails::Entity->ClientThink();

	reinterpret_cast<Type>(SimulateEntities->Original)();
}

bool __fastcall Hooked_CheckCRCs(void*, void*, const char* szMap) noexcept
{
	if (Menu::Get.Misc.Sv_Pure_Bypass)
	{
		return true;
	}

	using Type = bool(__cdecl*)(const char*);

	return reinterpret_cast<Type>(CheckCRCs->Original)(szMap);
}

void __fastcall Hooked_CheckWhitelist(void*, void*, int iUnknown) noexcept
{
	if (Menu::Get.Misc.Sv_Pure_Bypass)
	{
		return;
	}

	using Type = void(__cdecl*)(int);

	reinterpret_cast<Type>(CheckWhitelist->Original)(iUnknown);
}

void __fastcall Hooked_ConsistencyCheck(void* pThis, void*, bool bChanged) noexcept
{
	if (Menu::Get.Misc.Sv_Pure_Bypass)
	{
		return;
	}

	using Type = void(__thiscall*)(void*, int);

	reinterpret_cast<Type>(ConsistencyCheck->Original)(pThis, bChanged);
}

bool __fastcall Hooked_ProcessFixAngle(void* ECX, void* EDX, SVC_FixAngle* msg)
{
	using Type = bool(__thiscall*)(void*, SVC_FixAngle*);

	if (msg->m_Angle.IsValid() /*Server might crash you ?*/ && (!Menu::Get.Misc.NoDrug || msg->m_bRelative))
		return reinterpret_cast<Type>(ProcessFixAngle->Original)(ECX, msg);

	return true;
}

void __fastcall Hooked_SmokeGrenade_Start(CBaseEntity* ecx, void* edx, void* pParticleMgr, void* pArgs)
{
	if (Menu::Get.Visuals.NoSmoke) {

		return;
	}

	using Type = void(__thiscall*)(void*, void*, void*);

	reinterpret_cast<Type>(C_ParticleSmokeGrenade->Original)(ecx, pParticleMgr, pArgs);
}

void __stdcall Hooked_ClientModeShared_FireGameEvent(IGameEvent* event)
{
	auto EventName = event->GetName();

	if (_stricmp(EventName, "player_connect_client") == 0)
	{
		ChatElement->Printf("\x04Player \x03%s \x04has joined the game", event->GetString("name"));

		return;
	}

	if (_stricmp(EventName, "player_disconnect") == 0)
	{
		ChatElement->Printf("\x04Player \x03%s \x04left the game (\x03%s)", event->GetString("name"), event->GetString("reason"));

		return;
	}

	using Type = void(__stdcall*)(void*);

	reinterpret_cast<Type>(ClientModeShared_FireGameEvent->Original)(event);
}


void __fastcall Hooked_CalcViewModelView(CBaseEntity* pThis, void*, CBaseEntity* owner, const Vector& eyePosition, const QAngle& eyeAngles)
{
	QAngle vmangles = eyeAngles;

	Vector vmorigin = eyePosition;

	Vector vecRight, vecUp, vecForward;

	Math::AngleVectors(vmangles, &vecForward, &vecRight, &vecUp);

	vmangles += (Vector&)Menu::Get.Visuals.ViewModel.Angles;

	vmorigin += (vecForward * Menu::Get.Visuals.ViewModel.Origin[0]) +
		(vecRight * Menu::Get.Visuals.ViewModel.Origin[1]) +
		(vecUp * Menu::Get.Visuals.ViewModel.Origin[2]);

	using Type = void(__thiscall*)(void*, void*, const Vector&, const Vector&);

	reinterpret_cast<Type>(CalcViewModelView->Original)(pThis, owner, vmorigin, vmangles);
}

void __cdecl Hooked_Warning(char* msg, ...)
{
	if (Menu::Get.Misc.Disable_Game_Console_Warnings) return void();
	{
		va_list va_alist;

		char buf[1000];

		va_start(va_alist, msg);

		_vsnprintf(buf, sizeof(buf), msg, va_alist);

		va_end(va_alist);

		using Type = void(__cdecl*)(char* msg, ...);

		reinterpret_cast<Type>(Warning->Original)(buf);
	}
}

void __cdecl Hooked_CL_QueueDownload(const char* Name) noexcept
{
	constexpr auto& Filter = Menu::Get.Misc.DownloadManagerFilter;

	enum { All, NoSounds, MapsOnly, None, CustomFileExtension };

	if (Filter == None
		|| (Filter == MapsOnly && !strstr(Name, ".bsp"))
		|| (Filter == NoSounds && (strstr(Name, ".wav") || strstr(Name, ".mp3")))
		|| (Filter == CustomFileExtension && !FindStringCIS(std::string(Name), std::string(Menu::Get.Misc.CustomizeableFiles))))
		return;

	using Type = void(__cdecl*)(const char*);

	reinterpret_cast<Type>(CL_QueueDownload->Original)(Name);
}

bool __stdcall Hooked_ProcessStringCmd(NET_StringCmd* msg)
{
	// Don't let server execute commands in your console.

	return true;
}

int __fastcall Hooked_DrawModel(CBaseEntity* pThis, void*, int flags)
{
	if (pThis) {

		using Type = const char* (__thiscall*)(void*);

		auto Name{ reinterpret_cast<Type>(GetEffectName)(PVOID(UINT(pThis) + 8)) };

		if (Name && ((Menu::Get.Visuals.Disable_Fire_Particles && strstr(Name, "burning_character")) || strstr(Name, "explosion_huge"))) {

			return 0;
		}

		using Type1 = int(__thiscall*)(CBaseEntity*, int);

		return reinterpret_cast<Type1>(DrawModel->Original)(pThis, flags);
	}

	return 0;
}

bool __fastcall Hooked_CCSMapOverview_CanPlayerBeSeen(void* Self, void* EDX, void* player)
{
	if (Menu::Get.Visuals.NoTeammates && Engine->GetLocalPlayer() != *reinterpret_cast<std::int32_t*>(player) + 1) // Radar
	{
		if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal)
		{
			if (pLocal->GetTeamNum() == *reinterpret_cast<std::int32_t*>(std::uint32_t(player) + 48))
				return false;
		}
	}

	using Type = bool(__thiscall*)(void*, void*);

	return reinterpret_cast<Type>(CCSMapOverview_CanPlayerBeSeen->Original)(Self, player);
}

bool __fastcall Hooked_CTraceFilterSimple_ShouldHitEntity(void* Self, void*, CBaseEntity* pEntity, int contentsMask)
{
	if (Menu::Get.Visuals.NoTeammates) // Target ID
	{
		if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal)
		{
			if (pEntity->GetTeamNum() == pLocal->GetTeamNum())
			{
				return false;
			}
		}
	}

	using Type = bool(__thiscall*)(void*, void*, int);

	return reinterpret_cast<Type>(CTraceFilterSimple_ShouldHitEntity->Original)(Self, pEntity, contentsMask);
}

void* __fastcall Hooked_GetPMaterial(void* pThis, void* EDX, const char* MaterialName)
{
	using Type = void* (__thiscall*)(void*, const char*);

	[[maybe_unused]] static const auto Once = [pThis]()
	{
		for (auto i = 1; i < ListOfEffects.first.m_szNames.size(); i++)
		{
			auto PathToMaterial = std::string("effects/") + ListOfEffects.first.m_szNames.at(i);

			ListOfEffects.second.at(i - 1) = reinterpret_cast<Type>(GetPMaterial->Original)(pThis, PathToMaterial.c_str());
		}

		return 1;
	}();

	if (Menu::Get.Visuals.MuzzleFlash > 0 && strstr(MaterialName, "muzzleflash"))
	{
		auto Return = ListOfEffects.second.at(Menu::Get.Visuals.MuzzleFlash - 1);

		if (Return) return Return;
	}

	return reinterpret_cast<Type>(GetPMaterial->Original)(pThis, MaterialName);
}

bool __fastcall Hooked_FireEventIntern(void* pThis, void* EDX, IGameEvent* event, bool bServerOnly, bool bClientOnly)
{
	if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal && event && !Menu::Get.General.Panic) {

		auto EventName = event->GetName();

		if (strstr(EventName, "player_spawn"))
		{
			int Spawned_INDEX = Engine->GetPlayerForUserID(event->GetInt("userid"));

			if (Spawned_INDEX == Engine->GetLocalPlayer()) {

				Global_Jumps = Perfect_Jumps = 0;

				pLocal->GetNightVisionAlpha() = pLocal->IsNightVisionOn() = Menu::Get.Colors.General.Nightvision[Color::Elements::Alpha];
			}

			if (auto Entity = GetEntity(Spawned_INDEX); Entity)
			{
				if (Spawned_INDEX > 0 && Spawned_INDEX < HP_Bar::Entities.size()) {

					auto& [pEntity, MaxHealth] = HP_Bar::Entities[Spawned_INDEX];

					MaxHealth = Entity->GetHealth();

					if (pLocal->IsAlive())
					{
						auto IsEnemy = Entity->IsEnemy(pLocal);

						if (Menu::Get.Visuals.Health && Menu::Get.Visuals.HealthType[IsEnemy] == 2 && !pEntity)
						{
							HP_Bar::CreateSpriteForEntity(Entity, IsEnemy);
						}
					}

					HP_Bar::UpdateSpriteModelIndex({ pEntity, MaxHealth }, Entity->GetHealth());
				}
			}
		}

		if (strstr(EventName, "bullet_impact"))
		{
			if (auto PlayerIndex = Engine->GetPlayerForUserID(event->GetInt("userid")); PlayerIndex == Engine->GetLocalPlayer()) {

				Vector Position(event->GetFloat("x"), event->GetFloat("y"), event->GetFloat("z"));

				constexpr auto& Bullet_Impact{ Menu::Get.Colors.General.BulletImpact };

				if (Bullet_Impact[Color::Elements::Alpha]) {

					DebugOverlay->AddBoxOverlay(Position, Vector(-2, -2, -2), Vector(2, 2, 2), QAngle(0, 0, 0), Bullet_Impact[Color::Elements::Red], Bullet_Impact[Color::Elements::Green], Bullet_Impact[Color::Elements::Blue], Bullet_Impact[Color::Elements::Alpha], 4);
				}

				if (Menu::Get.Visuals.Tracers.Enabled) {

					DrawBeamd(pLocal->GetEyePosition() - Vector(0, 0, Menu::Get.Visuals.Tracers.PointScale), Position, Menu::Get.Colors.General.Tracers);
				}
			}
		}


		if (strstr(EventName, "player_hurt"))
		{
			int attacker = event->GetInt("attacker");

			int dmg_health = event->GetInt("dmg_health");

			int Hurted_INDEX = Engine->GetPlayerForUserID(event->GetInt("userid"));

			if (Engine->GetPlayerForUserID(attacker) == Engine->GetLocalPlayer())
			{
				if (Menu::Get.Visuals.DamageIndicator.Enabled)
				{
					DamageIndicator::data = DamageIndicator_t{ dmg_health, pLocal->GetTickBase() * Globals->interval_per_tick + 1.f };
				}

				if (Menu::Get.Visuals.Hitmarker.Enabled)
				{
					if (Menu::Get.Visuals.Hitmarker.HitSound)
					{
						auto Temp = HitSounds.size() - 2;

						Engine->ClientCmd_Unrestricted_Formatted("play \"HitSounds\\%s\"", HitSounds[Menu::Get.Visuals.Hitmarker.HitSound.Get() >= Temp ? RandomInt(1, Temp) : Menu::Get.Visuals.Hitmarker.HitSound.Get()].c_str());
					}

					if (Menu::Get.Visuals.Hitmarker.Mode)
					{
						Engine->ClientCmd_Unrestricted_Formatted("r_screenoverlay \"overlays\\hitmarkers\\hitmarker%i\"", Menu::Get.Visuals.Hitmarker.Overlay.Get());

						Overlay_Triggered = true;
					}

					_flHurtTime = Globals->curtime;
				}
			}
		}

		if (strstr(EventName, "player_death"))
		{
			int DIED_INDEX = Engine->GetPlayerForUserID(event->GetInt("userid")), KILLER_INDEX = Engine->GetPlayerForUserID(event->GetInt("attacker"));

			if (DIED_INDEX == Engine->GetLocalPlayer())
			{
				Engine->ClientCmd_Unrestricted_Formatted("r_screenoverlay \"\"");

				HP_Bar::ClearSprites();

				if (Trails::Entity)
				{
					Trails::Entity->Remove();

					Trails::Entity = nullptr;
				}
			}
			else
			{
				if (DIED_INDEX > 0 && DIED_INDEX < 65)
				{
					if (auto& Ent = HP_Bar::Entities[DIED_INDEX].first; Ent)
					{
						Ent->Remove();

						Ent = nullptr;
					}
				}
			}

			CBaseEntity* Died_Player = GetEntity(DIED_INDEX), * Killer = GetEntity(KILLER_INDEX);

			if (Died_Player && Killer && Killer == pLocal) {

				if (Menu::Get.Misc.KillMessage && Killmessages.size())
				{
					std::string message = Killmessages[std::rand() % Killmessages.size()];

					if (strstr(message.c_str(), "$nick"))
					{
						std::string dead_player_name = std::string(Died_Player->GetName());

						dead_player_name.erase(std::remove(dead_player_name.begin(), dead_player_name.end(), ';'), dead_player_name.end());
						dead_player_name.erase(std::remove(dead_player_name.begin(), dead_player_name.end(), '"'), dead_player_name.end());
						dead_player_name.erase(std::remove(dead_player_name.begin(), dead_player_name.end(), '\n'), dead_player_name.end());

						message = ReplaceString(message, "$nick", dead_player_name);
					}

					Engine->ClientCmd_Unrestricted_Formatted("say \"%s\"", message.c_str());
				}

				if (Menu::Get.Visuals.KillEffect_Type > 0) {

					auto HeadHitBox(Died_Player->GetHitboxPosition(12));

					const auto& Angles = Died_Player->GetAbsAngles();

					static CTeslaInfo teslaInfo;

					static CEffectData data;

					[[maybe_unused]] static const auto Once = []()
					{
						data.m_fFlags = 6;
						data.m_vNormal.Init(-1.f, 1.f, -2.f); //some shit I don't care about
						data.m_flScale = 2.f;
						data.m_nColor = 3; //not actual color, but what kind of blood spray that would be (headshot, knife impact, etc)

						teslaInfo.m_pszSpriteName = "sprites/physbeam.vmt";

						teslaInfo.m_flBeamWidth = 5.f;

						teslaInfo.m_flRadius = 100.f;

						teslaInfo.m_vColor.Init(1.f, 1.f, 1.f); //your value up to 255 / 255.f

						teslaInfo.m_flTimeVisible = 0.75f;

						teslaInfo.m_nBeams = 12;

						return 0;
					}();

					data.m_nEntIndex = teslaInfo.m_nEntIndex = DIED_INDEX;

					data.m_vAngles = teslaInfo.m_vAngles = Angles;

					data.m_vOrigin = teslaInfo.m_vPos = HeadHitBox;

					static auto DispatchEffect = reinterpret_cast<void(*)(const char*, const CEffectData&)>(
						Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 83 C4 08 EB 15")));

					if (Menu::Get.Visuals.KillEffect_Type == 1)
					{
						static auto FX_Tesla = reinterpret_cast<void(__cdecl*)(const  CTeslaInfo&)>(Tools::FindPattern("client.dll", "8B 7B 08 FF 77 18") - 0x24);

						FX_Tesla(teslaInfo);

						DispatchEffect("csblood", data);
					}
					else
					{
						DispatchEffect(KillEffects[Menu::Get.Visuals.KillEffect_Type], data);
					}

					Surface->Play_Sound("ambient/energy/zap3.wav");
				}
			}
		}
	}

	if (sv_cheats)
		sv_cheats->SetValue(1); // Memory Modifiction Only // reuqired for third person and hitmarker overlays.

	if (Fog_Override) // Some maps does reset this var, so fog features will break ^^
		Fog_Override->SetValue(
			Menu::Get.Visuals.FOG.Enabled || Menu::Get.Visuals.NoFog);

	using Type = bool(__thiscall*)(void*, void*, bool, bool);

	return reinterpret_cast<Type>(FireEventIntern->Original)(EventManager, event, bServerOnly, bClientOnly);
}

bool __fastcall Hooked_GetCvarValue(void* ECX, void* EDX, SVC_GetCvarValue* msg)
{
	// Prepare the response.
	CLC_RespondCvarValue returnMsg; *(void**)&returnMsg = CLC_RespondCvarValue_Table;

	returnMsg.m_iCookie = msg->m_iCookie;
	returnMsg.m_szCvarName = msg->m_szCvarName;
	returnMsg.m_szCvarValue = "";
	returnMsg.m_eStatusCode = eQueryCvarValueStatus_CvarNotFound;

	char tempValue[256];

	// Does any ConCommand exist with this name?

	const auto pVar = Cvar->FindVar(msg->m_szCvarName);

	if (pVar)
	{
		if (pVar->IsFlagSet(SERVER_CANNOT_QUERY))
		{
			// The server isn't allowed to query this.
			returnMsg.m_eStatusCode = eQueryCvarValueStatus_CvarProtected;
		}
		else
		{
			returnMsg.m_eStatusCode = eQueryCvarValueStatus_ValueIntact;

			if (pVar->IsFlagSet(NEVER_AS_STRING))
			{
				// The cvar won't store a string, so we have to come up with a string for it ourselves.
				if (fabs(pVar->GetFloat() - pVar->GetInt()) < 0.001f)
				{
					snprintf(tempValue, sizeof(tempValue), "%d", pVar->GetInt());
				}
				else
				{
					snprintf(tempValue, sizeof(tempValue), "%f", pVar->GetFloat());
				}

				returnMsg.m_szCvarValue = tempValue;
			}
			else
			{
				// The easy case..
				returnMsg.m_szCvarValue = pVar->GetString();
			}
		}
	}
	else
	{
		if (Cvar->FindCommand(msg->m_szCvarName))
			returnMsg.m_eStatusCode = eQueryCvarValueStatus_NotACvar; // It's a command, not a cvar.
		else
			returnMsg.m_eStatusCode = eQueryCvarValueStatus_CvarNotFound;
	}

#ifdef SafetyMode
	bool HasDesiredValue = false;
#endif

	for (int i = 0; i < DesiredConVarsValue.size(); i++)
	{
		if (strcmp(returnMsg.m_szCvarName, DesiredConVarsValue.at(i).Name) == 0)
		{
			returnMsg.m_szCvarValue = DesiredConVarsValue.at(i).Value;

			returnMsg.m_eStatusCode = (EQueryCvarValueStatus)DesiredConVarsValue.at(i).Status;

#ifdef SafetyMode
			HasDesiredValue = true;
#endif
			break;
		}
	}

#ifdef SafetyMode
	if (!HasDesiredValue && pVar)
	{
		returnMsg.m_szCvarValue = pVar->GetDefault();
	}
#endif

	// Send back.
	if (auto NetChan = ClientState->GetNetChannel(); NetChan)
		NetChan->SendNetMsg((void*&)returnMsg);

	return true;
}

void __stdcall Hooked_WriteListenEventList(void* msg) noexcept
{
	using Type = void(__thiscall*)(PVOID, PVOID);

	reinterpret_cast<Type>(WriteListenEventList->Original)(EventManager, msg);

	bool BulletImpact_Options = ((Color&)Menu::Get.Colors.General.BulletImpact.Get()).a() || Menu::Get.Visuals.Tracers.Enabled;

	bool PlayerHurt_Options = Menu::Get.Visuals.Hitmarker.Enabled || Menu::Get.Visuals.DamageIndicator.Enabled;

	static auto m_EventArray_Set = [](PVOID msg, int eventid) { *(unsigned long*)((unsigned int)msg + 4 * (eventid >> 5) + 16) |= 1 << (eventid & 0x1F); };

	if (PlayerHurt_Options) {

		m_EventArray_Set(msg, 24);
	}

	if (BulletImpact_Options) {

		m_EventArray_Set(msg, 98);
	}
}

void __stdcall Hooked_CSteam3Client_InitiateConnection(void* data, int nMaxData, int& nOutSize, long nIP, short nPort, unsigned long long nGSSteamID, bool bSecure)
{
	auto pdata = (long*)data;

	using Type = void(__stdcall*)(void*, int, int&, long, short, unsigned long long, bool);

	reinterpret_cast<Type>(CSteam3Client_InitiateConnection->Original)(pdata, nMaxData, nOutSize, nIP, nPort, nGSSteamID, bSecure);

	if (Menu::Get.Misc.SteamIDSpoofer.Enabled)
	{
		uint32_t nSteamID = Menu::Get.Misc.SteamIDSpoofer.Random ? (__rdtsc() % INT_MAX) : Menu::Get.Misc.SteamIDSpoofer.SteamID;

		uint32_t revHash;

		nOutSize = GenerateRevEmu2013(Transpose(data, 8), nSteamID >> 1, &revHash);

		if (!nOutSize) return;

		revHash = revHash << 1;

		pdata[0] = revHash | (nSteamID & 1);
	}
}