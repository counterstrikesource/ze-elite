#include "SDK.h"
#include "glow_outline_effect.h"

typedef ITexture* pTexture;
typedef IMaterial* pMaterial;

pTexture pRtFullFrame, pRtQuarterSize1;
pMaterial pMatGlowColor, pMatHaloAddToScreen;

CGlowObjectManager g_GlowObjectManager;

int __fastcall Hooked_C_BaseAnimating_DrawModel
(
	CBaseEntity* ECX,
	void*, int flags
);

auto Assign_Glow_Textures() -> void
{
	pRtFullFrame = MatSystemOther->FindTexture("_rt_FullFrameFB", 0);

	pRtQuarterSize1 = MatSystemOther->FindTexture("_rt_SmallFB1", 0);

	pMatGlowColor = MatSystemOther->FindMaterial("dev/glow_color", 0, true); pMatGlowColor->AddRef();

	pMatHaloAddToScreen = MatSystemOther->FindMaterial("dev/halo_add_to_screen", 0, true); pMatHaloAddToScreen->AddRef();

	if (!pMatHaloAddToScreen->IsErrorMaterial())
	{
		IMaterialVar* pDimVar = pMatHaloAddToScreen->FindVar("$C0_X", NULL);

		if (pDimVar) pDimVar->SetFloatValue(1.f);
	}
}

struct ShaderStencilState_t
{
	bool m_bEnable;
	StencilOperation_t m_FailOp;
	StencilOperation_t m_ZFailOp;
	StencilOperation_t m_PassOp;
	StencilComparisonFunction_t m_CompareFunc;
	int m_nReferenceValue;
	uintptr_t m_nTestMask;
	uintptr_t m_nWriteMask;

	ShaderStencilState_t()
	{
		m_bEnable = false;
		m_PassOp = m_FailOp = m_ZFailOp = STENCILOPERATION_KEEP;
		m_CompareFunc = STENCILCOMPARISONFUNCTION_ALWAYS;
		m_nReferenceValue = 0;
		m_nTestMask = m_nWriteMask = 0xFFFFFFFF;
	}

	void SetStencilState(IMatRenderContext* pRenderContext)
	{
		pRenderContext->SetStencilEnable(m_bEnable);
		pRenderContext->SetStencilFailOperation(m_FailOp);
		pRenderContext->SetStencilZFailOperation(m_ZFailOp);
		pRenderContext->SetStencilPassOperation(m_PassOp);
		pRenderContext->SetStencilCompareFunction(m_CompareFunc);
		pRenderContext->SetStencilReferenceValue(m_nReferenceValue);
		pRenderContext->SetStencilTestMask(m_nTestMask);
		pRenderContext->SetStencilWriteMask(m_nWriteMask);
	}
};

void CGlowObjectManager::RenderGlowEffects(CViewSetup* pSetup, int nSplitScreenSlot)
{
	if (Menu::Get.Visuals.Glow.Enabled[0] || Menu::Get.Visuals.Glow.Enabled[1])
	{
		IMatRenderContext* pRenderContext = MatSystemOther->GetRenderContext();

		int nX, nY, nWidth, nHeight;

		pRenderContext->GetViewport(nX, nY, nWidth, nHeight);

		pRenderContext->BeginPIXEvent(0xFFF5940F, "EntityGlowEffects");

		ApplyEntityGlowEffects(pSetup, nSplitScreenSlot, pRenderContext, nX, nY, nWidth, nHeight);

		pRenderContext->EndPIXEvent();
	}
}

static void SetRenderTargetAndViewPort(IMatRenderContext* pRenderContext, ITexture* rt, int w, int h)
{
	pRenderContext->SetRenderTarget(rt);
	pRenderContext->Viewport(0, 0, w, h);
}

void CGlowObjectManager::RenderGlowModels(CBaseEntity* pLocal, CViewSetup* pSetup, int nSplitScreenSlot, IMatRenderContext* pRenderContext)
{
	pRenderContext->PushRenderTargetAndViewport();

	Vector vOrigColor;

	RenderView->GetColorModulation(vOrigColor.Base());

	float flOrigBlend = RenderView->GetBlend();

	SetRenderTargetAndViewPort(pRenderContext, pRtFullFrame, pSetup->GetWidth(), pSetup->GetHeight());

	pRenderContext->ClearColor3ub(0, 0, 0);

	pRenderContext->ClearBuffers(true, false, false);

	MdlRender->ForcedMaterialOverride(pMatGlowColor);

	ShaderStencilState_t stencilState;
	stencilState.m_bEnable = false;
	stencilState.m_nReferenceValue = 0;
	stencilState.m_nTestMask = 0xFF;
	stencilState.m_CompareFunc = STENCILCOMPARISONFUNCTION_ALWAYS;
	stencilState.m_PassOp = STENCILOPERATION_KEEP;
	stencilState.m_FailOp = STENCILOPERATION_KEEP;
	stencilState.m_ZFailOp = STENCILOPERATION_KEEP;

	stencilState.SetStencilState(pRenderContext);

	for (int i = 0; i < m_GlowObjectDefinitions.size(); ++i)
	{
		if (!m_GlowObjectDefinitions[i].ShouldDraw(nSplitScreenSlot))
			continue;

		Color color = (Color&)Menu::Get.Colors.Glow.Player.Get(m_GlowObjectDefinitions[i].pEntity->IsEnemy(pLocal) * sizeof(Color));

		auto Alpha = color.a() / 255.f;

		RenderView->SetBlend(Alpha);

		Vector vGlowColor = color.GetVector(255.f) * Alpha;

		RenderView->SetColorModulation(&vGlowColor[0]);

		m_GlowObjectDefinitions[i].DrawModel();
	}

	MdlRender->ForcedMaterialOverride(NULL);
	RenderView->SetColorModulation(vOrigColor.Base());
	RenderView->SetBlend(flOrigBlend);

	ShaderStencilState_t stencilStateDisable;
	stencilStateDisable.m_bEnable = false;
	stencilStateDisable.SetStencilState(pRenderContext);

	pRenderContext->PopRenderTargetAndViewport();
}

void CGlowObjectManager::ApplyEntityGlowEffects(CViewSetup* pSetup, int nSplitScreenSlot, IMatRenderContext* pRenderContext, int x, int y, int w, int h)
{
	auto LocalPlayer = CBaseEntity::GetLocalPlayer(); if (!LocalPlayer) return;

	MdlRender->ForcedMaterialOverride(pMatGlowColor);
	ShaderStencilState_t stencilStateDisable;
	stencilStateDisable.m_bEnable = false;
	float flSavedBlend = RenderView->GetBlend();

	RenderView->SetBlend(0.0f);

	pRenderContext->OverrideDepthEnable(true, false);

	int iNumGlowObjects = 0;

	for (int i = 0; i < m_GlowObjectDefinitions.size(); ++i)
	{
		if (!m_GlowObjectDefinitions[i].ShouldDraw(nSplitScreenSlot))
			continue;

		if (Menu::Get.Visuals.Glow.Enabled[m_GlowObjectDefinitions[i].pEntity->IsEnemy(LocalPlayer)])
		{
			ShaderStencilState_t stencilState;
			stencilState.m_bEnable = true;
			stencilState.m_nReferenceValue = 1;
			stencilState.m_CompareFunc = STENCILCOMPARISONFUNCTION_ALWAYS;
			stencilState.m_PassOp = STENCILOPERATION_KEEP;
			stencilState.m_FailOp = STENCILOPERATION_KEEP;
			stencilState.m_ZFailOp = STENCILOPERATION_REPLACE;

			stencilState.SetStencilState(pRenderContext);

			m_GlowObjectDefinitions[i].DrawModel();
		}

		iNumGlowObjects++;
	}

	for (int i = 0; i < m_GlowObjectDefinitions.size(); ++i)
	{
		if (!m_GlowObjectDefinitions[i].ShouldDraw(nSplitScreenSlot))
			continue;

		if (Menu::Get.Visuals.Glow.Enabled[m_GlowObjectDefinitions[i].pEntity->IsEnemy(LocalPlayer)])
		{
			ShaderStencilState_t stencilState;
			stencilState.m_bEnable = true;
			stencilState.m_nReferenceValue = 2;
			stencilState.m_CompareFunc = STENCILCOMPARISONFUNCTION_ALWAYS;
			stencilState.m_PassOp = STENCILOPERATION_REPLACE;
			stencilState.m_FailOp = STENCILOPERATION_KEEP;
			stencilState.m_ZFailOp = STENCILOPERATION_KEEP;
			stencilState.SetStencilState(pRenderContext);

			m_GlowObjectDefinitions[i].DrawModel();
		}
	}

	pRenderContext->OverrideDepthEnable(false, false);
	RenderView->SetBlend(flSavedBlend);
	stencilStateDisable.SetStencilState(pRenderContext);
	MdlRender->ForcedMaterialOverride(NULL);

	if (iNumGlowObjects <= 0)
		return;


	pRenderContext->BeginPIXEvent(0xFFF5940F, "RenderGlowModels");
	RenderGlowModels(LocalPlayer, pSetup, nSplitScreenSlot, pRenderContext);

	int nSrcWidth = pSetup->GetWidth();
	int nSrcHeight = pSetup->GetHeight();

	ShaderStencilState_t stencilState;
	stencilState.m_bEnable = true;
	stencilState.m_nWriteMask = 0x0;
	stencilState.m_nTestMask = 0xFF;
	stencilState.m_nReferenceValue = 0x0;
	stencilState.m_CompareFunc = STENCILCOMPARISONFUNCTION_EQUAL;
	stencilState.m_PassOp = STENCILOPERATION_KEEP;
	stencilState.m_FailOp = STENCILOPERATION_KEEP;
	stencilState.m_ZFailOp = STENCILOPERATION_KEEP;
	stencilState.SetStencilState(pRenderContext);

	pRenderContext->DrawScreenSpaceRectangle(pMatHaloAddToScreen, 0, 0, w, h,
		0.0f, -0.5f, nSrcWidth / 4 - 1, nSrcHeight / 4 - 1,
		pRtQuarterSize1->GetActualWidth(),
		pRtQuarterSize1->GetActualHeight());

	stencilStateDisable.SetStencilState(pRenderContext);

	pRenderContext->EndPIXEvent();
}

void CGlowObjectManager::GlowObjectDefinition_t::DrawModel()
{
	if (pEntity)
	{
		static auto m_bReadyToDraw_Offset = *reinterpret_cast<BYTE*>(Tools::FindPattern("client.dll", "8A 83 ? ? ? ? 8D 4D FE") + 0x30);

		if (*reinterpret_cast<bool*>(std::uint32_t(pEntity) + m_bReadyToDraw_Offset))
		{
			using Type = int(__thiscall*)(void*, int);

			reinterpret_cast<Type>(&Hooked_C_BaseAnimating_DrawModel)(pEntity + 4, 0x1);
		}
	}
}
