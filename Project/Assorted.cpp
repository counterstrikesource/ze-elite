#include "SDK.h"
#include <fstream>
#include "Trails.h"
#include "HP_Bar.h"
#include "VoiceChat.h"
#include "Assorted.h"
#include <filesystem>
#include "SkinChanger.h"
#include "WorldTextures.h"
#include "ClientEntityListener.h"
#include "ImGui/imgui_impl_dx9.h"
#include "ImGui/imgui_impl_win32.h"

char* TempPath;

float _flHurtTime{ 0.f };

char DirectoryPath[MAX_PATH];

bool Overlay_Triggered{ false };

std::vector<std::string> HitSounds{ "Off" };

static char cl_downloadfilter_backup_value[64];

void mmcopy
(
	void* address,
	const void* value,
	size_t bytes
);

void DrawBeamd(Vector src, Vector end, Color color) noexcept
{
	BeamInfo_t beamInfo;

	constexpr auto& TracersVars = Menu::Get.Visuals.Tracers;

	auto ModelPath = std::string("sprites/") + TracerSprites[TracersVars.SpriteTexture] + ".vmt";

	beamInfo.m_nType = 0;
	beamInfo.m_pszModelName = ModelPath.data();
	beamInfo.m_flLife = TracersVars.m_flLife;
	beamInfo.m_flWidth = TracersVars.m_flWidth;
	beamInfo.m_flEndWidth = TracersVars.m_flEndWidth;
	beamInfo.m_flFadeLength = TracersVars.m_flFadeLength;
	beamInfo.m_flAmplitude = TracersVars.m_flAmplitude;
	beamInfo.m_nSegments = TracersVars.m_nSegments;
	beamInfo.m_flSpeed = TracersVars.m_flSpeed;
	beamInfo.m_nFlags = TracersVars.m_nFlags;
	beamInfo.m_nStartFrame = 0;
	beamInfo.m_flFrameRate = 1;
	beamInfo.m_flRed = color.r();
	beamInfo.m_flGreen = color.g();
	beamInfo.m_flBlue = color.b();
	beamInfo.m_flBrightness = color.a();
	beamInfo.m_bRenderable = true;
	beamInfo.m_vecStart = src;
	beamInfo.m_vecEnd = end;

	Beams->CreateBeamPoints(beamInfo);
}

void Hit_Marker_On_Paint() noexcept
{
	if (Menu::Get.General.Panic || !Menu::Get.Visuals.Hitmarker.Enabled)
	{
		return;
	}
	
	auto curtime = Globals->curtime;

	constexpr int& Gap = Menu::Get.Visuals.Hitmarker.Gap;

	constexpr int& Length = Menu::Get.Visuals.Hitmarker.Length;

	if (_flHurtTime + static_cast<float>(Menu::Get.Visuals.Hitmarker.Time / 1000.f) >= curtime)
	{
		if (!Menu::Get.Visuals.Hitmarker.Mode)
		{
			int screenSizeX, screenSizeY;

			Engine->GetScreenSize(screenSizeX, screenSizeY);

			screenSizeX /= 2, screenSizeY /= 2;

			Surface->DrawSetColor(Menu::Get.Colors.General.Hitmarker);

			Surface->DrawLine(screenSizeX - (Length + Gap), screenSizeY - (Length + Gap), screenSizeX - Gap, screenSizeY - Gap);

			Surface->DrawLine(screenSizeX - (Length + Gap), screenSizeY + (Length + Gap), screenSizeX - Gap, screenSizeY + Gap);

			Surface->DrawLine(screenSizeX + (Length + Gap), screenSizeY + (Length + Gap), screenSizeX + Gap, screenSizeY + Gap);

			Surface->DrawLine(screenSizeX + (Length + Gap), screenSizeY - (Length + Gap), screenSizeX + Gap, screenSizeY - Gap);
		}

	}
	else
	{
		if (Menu::Get.Visuals.Hitmarker.Mode && Overlay_Triggered) {

			Engine->ClientCmd_Unrestricted("r_screenoverlay \"\"");

			Overlay_Triggered = !Overlay_Triggered;
		}
	}
}

bool PrecacheModel(const char* szModelName) noexcept
{
	static auto ClientStringTableContainer = Interfaces::GetInterface("engine.dll", "VEngineClientStringTable001"); assert(ClientStringTableContainer);

	static auto FindTable = GetVirtualMethod<void* (__thiscall*)(void*, const char*) >(ClientStringTableContainer, 3);

	if (void* m_pModelPrecacheTable = FindTable(ClientStringTableContainer, "modelprecache"); m_pModelPrecacheTable)
	{
		static auto AddString = GetVirtualMethod<int(__thiscall*)(void*, bool, const char*, int, const void*) >(m_pModelPrecacheTable, 8);

		AddString(m_pModelPrecacheTable, false, szModelName, -1, 0);
	}

	return true;
}

int FindAndReturnV(std::vector<std::string>& vector, const char* item)
{
	for (auto i = 0; i < vector.size(); i++)
	{
		if (!strcmp(vector[i].c_str(), item))
			return i;
	}

	return 0;
}

auto AddFilesNameFromPathToArray(std::vector<std::string>& VectorOfString, const char* EndOfFile) noexcept -> void {

	if (DirectoryExists(TempPath)) {

		for (const auto& entry : FileSystem::directory_iterator(TempPath))
		{
			auto FilePath = entry.path().string();

			if (auto Found = FilePath.find_last_of("\\"); Found != std::string::npos)
			{
				strcpy(TempPath, (const char*)&FilePath[Found + 1]);

				if (Found = std::string(TempPath).find(EndOfFile); Found != std::string::npos)
				{
					TempPath[Found] = 0;

					VectorOfString.push_back(TempPath);
				}
			}
		}
	}
}

auto PerformCopy(HMODULE DLL) noexcept -> void
{
	TempPath = new char[MAX_PATH + 1];

	GetCurrentDirectory(MAX_PATH, DirectoryPath);

	GetModuleFileName(DLL, TempPath, MAX_PATH);

	for (auto i = strlen(TempPath); i > 0; i--)
	{
		if (TempPath[i] == '\\' || TempPath[i] == '/')
		{
			TempPath[i] = 0;

			strcat(TempPath, "\\cstrike");

			if (DirectoryExists(TempPath))
			{
				FileSystem::path SourceFile = TempPath;

				FileSystem::path TargetParent = DirectoryPath;

				auto target = TargetParent / SourceFile.filename();

				FileSystem::copy
				(
					SourceFile, target,
					FileSystem::copy_options::skip_existing |
					FileSystem::copy_options::recursive
				);
			}
			else
			{
				ColoredConMsg(Color::Red(), "[ERROR]: cstrike is not in same direction\n");
			}

			return;
		}
	}
}

void Prepare_Custom_Stuff() noexcept
{
	strcat
	(
		DirectoryPath,
		"\\cstrike\\"
	);

	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "materials\\skybox");

	AddFilesNameFromPathToArray(SkyBoxes, "bk.vmt");

	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "materials\\overlays\\Scopes");

	AddFilesNameFromPathToArray(ScopeOverlays);

	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "materials\\overlays\\hitmarkers");

	AddFilesNameFromPathToArray(HitmarkerOverlays);

	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "sound\\HitSounds");

	AddFilesNameFromPathToArray(HitSounds, ".wav");

	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "materials\\effects\\nightvisions");

	AddFilesNameFromPathToArray(NightVisions);

	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "materials\\sprites\\trails");
	{
		auto& [Names, ModelIndexes] = TrailMaterials;

		AddFilesNameFromPathToArray(Names);

		for (auto Temp : Names) ModelIndexes.push_back(-1);
	}

	ProcessEffectsList();

	VoiceChat::PrepareFiles();

	SkinChanger::PrepareFiles();

	Hands.PrepareFiles("hands");
	World.PrepareFiles("ground");
	Background.PrepareFiles("console");

	HitSounds.push_back(std::string("Random"));

	Menu::Get.Visuals.Hitmarker.HitSound = HitSounds.size() - 1;

	Menu::Get.Visuals.Hitmarker.Overlay = FindAndReturnV(HitmarkerOverlays, "hitmarker3");

	delete TempPath; // Last
}

char NEW_Disconnect_Msg[1000];

void Set_DisConnection_Msg(const char* Message, bool Reset) noexcept
{
	static auto Location = reinterpret_cast<BYTE*>(
		Tools::FindPattern("engine.dll", "B9 ? ? ? ? 8B 02") + 1);

	static BYTE Backup_Bytes[4] = { Location[0], Location[1], Location[2], Location[3] };

	[[maybe_unused]] static bool Once = []() {

		static unsigned long Protection_Backup{ 0 };

		VirtualProtect(Location, sizeof(void*), PAGE_EXECUTE_READWRITE, &Protection_Backup);

		*(void**)Location = &NEW_Disconnect_Msg;

		VirtualProtect(Location, sizeof(void*), Protection_Backup, nullptr);

		return 1;
	}();

	if (Reset)
	{
		mmcopy(Location, Backup_Bytes, 4);
	}
	else
	{
		strcpy(NEW_Disconnect_Msg, Message);
	}
}

void Update_Net_Graph_Text_Colors(bool Unload) noexcept
{
#define GRAPH_RED	(0.9 * 255)
#define GRAPH_GREEN (0.9 * 255)
#define GRAPH_BLUE	(0.7 * 255)

	static auto BaseAddress = Tools::FindPattern("client.dll", "51 F3 0F 2C C8");

	static auto IgnoreForwardBytes = [](void* Address, int Bytes)
	{
		return reinterpret_cast<void*>(std::uint32_t(Address) + Bytes);
	};

	static std::array<void*, 4>ListOfAlpha =
	{
		PVOID(BaseAddress + 40),
		PVOID(BaseAddress + 256),
		PVOID(BaseAddress + 715),
		PVOID(BaseAddress + 490),
	};

	static std::array<void*, 4> ListOf_RGB =
	{
		IgnoreForwardBytes(ListOfAlpha[0], 7),
		IgnoreForwardBytes(ListOfAlpha[1], 5),
		IgnoreForwardBytes(ListOfAlpha[2], 7),
		nullptr,
	};

	static unsigned long Protection_Backup{ 0 };

	auto& NET_graph_Color = (Color&)Menu::Get.Colors.General.Net_graph.Get();

	for (auto& Alpha : ListOfAlpha)
	{
		VirtualProtect(Alpha, 4, PAGE_EXECUTE_READWRITE, &Protection_Backup);

		if (Unload == 0)
		{
			*reinterpret_cast<std::int32_t*>(Alpha) = NET_graph_Color.a();
		}
		else
		{
			*reinterpret_cast<std::int32_t*>(Alpha) = 255;
		}

		VirtualProtect(Alpha, 4, Protection_Backup, nullptr);
	}

	static auto SharedSetColors = [](void* Base, Color& color, bool Unload = false, int Offset = 5)
	{
		VirtualProtect(Base, 4, PAGE_EXECUTE_READWRITE, &Protection_Backup);

		if (Unload == 0)
		{
			*reinterpret_cast<std::int32_t*>(Base) = color.b();

			*reinterpret_cast<std::int32_t*>(std::int32_t(Base) + Offset) = color.g();

			*reinterpret_cast<std::int32_t*>(std::int32_t(Base) + Offset * 2) = color.r();
		}
		else
		{
			*reinterpret_cast<std::int32_t*>(Base) = GRAPH_BLUE;

			*reinterpret_cast<std::int32_t*>(std::int32_t(Base) + Offset) = GRAPH_GREEN;

			*reinterpret_cast<std::int32_t*>(std::int32_t(Base) + Offset * 2) = GRAPH_RED;
		}

		VirtualProtect(Base, 4, Protection_Backup, nullptr);
	};

	for (auto& RGB : ListOf_RGB)
	{
		if (RGB == nullptr)
		{
			constexpr int Offset = 0x92;

			auto NRGB = ListOfAlpha[ListOfAlpha.size() - 1];

			static auto& r = *reinterpret_cast<std::uint32_t*>(std::uint32_t(NRGB) - 28);

			static auto& b = *reinterpret_cast<std::uint32_t*>(std::uint32_t(NRGB) - Offset);

			static auto& g = *reinterpret_cast<std::uint32_t*>(std::uint32_t(NRGB) - Offset - 7);

			VirtualProtect(&r, 4, PAGE_EXECUTE_READWRITE, &Protection_Backup);

			if (Unload == 0)
			{
				r = NET_graph_Color.r();
				g = NET_graph_Color.g();
				b = NET_graph_Color.b();
			}
			else
			{
				r = GRAPH_RED;
				g = GRAPH_GREEN;
				b = GRAPH_BLUE;
			}

			VirtualProtect(&r, 4, Protection_Backup, nullptr);
		}
		else
		{
			SharedSetColors(RGB, Menu::Get.Colors.General.Net_graph, Unload);
		}
	}
}

void SetClanTag(const char* tag) noexcept
{
	static auto Kv_Init_Name = reinterpret_cast<void* (__thiscall*)(void*, const char*)>(
		Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "E8 ? ? ? ? 8B F8 EB 02 33 FF 53")));

	static auto Kv_SetString = reinterpret_cast<void* (__thiscall*)(void*, const char*, const char*)>(
		Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "E8 ? ? ? ? 8B 8F ? ? ? ? FF 77 0C")));

	static auto Kv_Alloc = reinterpret_cast<void* (_cdecl*)(int)>(
		Tools::CallableFromRelative(Tools::FindPattern("engine.dll", "E8 ? ? ? ? 83 C4 04 85 C0 0F 84 ? ? ? ? 56")));

	const auto v9 = Kv_Alloc(32);

	if (v9)
	{
		const auto v10 = Kv_Init_Name(v9, "ClanTagChanged");

		if (v10)
		{
			Kv_SetString(v10, "tag", tag);

			GetVirtualMethod<void(__thiscall*)(void*, void*)>(Engine, 127)(Engine, v10);
		}
	}
}


std::string ReplaceString(std::string subject, const std::string& search, const std::string& replace) noexcept
{
	size_t pos = 0;

	while ((pos = subject.find(search, pos)) != std::string::npos)
	{
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}

	return subject;
}

bool IntersectRayWithOBB(const Vector& vecRayStart, const Vector& vecRayDelta, const matrix3x4& matOBBToWorld, const Vector& vecOBBMins, const Vector& vecOBBMaxs) noexcept
{


	// OPTIMIZE: Store this in the box instead of computing it here
	// compute center in local space
	Vector vecBoxExtents = (vecOBBMins + vecOBBMaxs) * 0.5;
	Vector vecBoxCenter;


	// transform to world space
	Math::VectorTransform(vecBoxExtents, matOBBToWorld, vecBoxCenter);


	// calc extents from local center
	vecBoxExtents = vecOBBMaxs - vecBoxExtents;


	// OPTIMIZE: This is optimized for world space.  If the transform is fast enough, it may make more
	// sense to just xform and call UTIL_ClipToBox() instead.  MEASURE THIS.


	// save the extents of the ray along 
	Vector extent, uextent;
	Vector segmentCenter = vecRayStart + vecRayDelta - vecBoxCenter;


	extent.x = extent.y = extent.z = 0.0f;


	// check box axes for separation
	for (int j = 0; j < 3; j++)
	{
		extent[j] = vecRayDelta.x * matOBBToWorld[0][j] + vecRayDelta.y * matOBBToWorld[1][j] + vecRayDelta.z * matOBBToWorld[2][j];
		uextent[j] = fabsf(extent[j]);
		float coord = segmentCenter.x * matOBBToWorld[0][j] + segmentCenter.y * matOBBToWorld[1][j] + segmentCenter.z * matOBBToWorld[2][j];
		coord = fabsf(coord);


		if (coord > (vecBoxExtents[j] + uextent[j]))
			return false;
	}


	// now check cross axes for separation

	float tmp, cextent;

	Vector cross;

	cross.Cross(&vecRayDelta, &segmentCenter);


	cextent = cross.x * matOBBToWorld[0][0] + cross.y * matOBBToWorld[1][0] + cross.z * matOBBToWorld[2][0];
	cextent = fabsf(cextent);
	tmp = vecBoxExtents[1] * uextent[2] + vecBoxExtents[2] * uextent[1];
	if (cextent > tmp)
		return false;


	cextent = cross.x * matOBBToWorld[0][1] + cross.y * matOBBToWorld[1][1] + cross.z * matOBBToWorld[2][1];
	cextent = fabsf(cextent);
	tmp = vecBoxExtents[0] * uextent[2] + vecBoxExtents[2] * uextent[0];
	if (cextent > tmp)
		return false;


	cextent = cross.x * matOBBToWorld[0][2] + cross.y * matOBBToWorld[1][2] + cross.z * matOBBToWorld[2][2];
	cextent = fabsf(cextent);
	tmp = vecBoxExtents[0] * uextent[1] + vecBoxExtents[1] * uextent[0];
	if (cextent > tmp)
		return false;


	return true;
}

bool FindStringCIS(std::string data, std::string toSearch) noexcept
{
	std::transform(data.begin(), data.end(), data.begin(), ::tolower);

	std::transform(toSearch.begin(), toSearch.end(), toSearch.begin(), ::tolower);

	return strstr(data.data(), toSearch.data());
}

char NewScopePath[MAX_PATH]; 

char Backup_Of_Scope_Len_Bytes[4]; 

void UpdateScopeLens() noexcept
{
	static auto CHudScope_Init = reinterpret_cast<void(__thiscall*)(void*)>(
		Tools::FindPattern("client.dll", "56 8B F1 8B 0D ? ? ? ? 6A 00 8B 01"));

	static auto Scope_Len_path = reinterpret_cast<BYTE*>(
		Tools::FindPattern("client.dll", "89 46 3C 8B 0D ? ? ? ?") + 0xE);

	if (Scope_Len_path && CHudScope_Init) {

		[[maybe_unused]] static auto Once = []() -> bool
		{
			mmcopy(Backup_Of_Scope_Len_Bytes, Scope_Len_path, 4);

			return true;
		}();

		if (Menu::Get.Visuals.ScopeLen == 0)
		{
			mmcopy(Scope_Len_path, Backup_Of_Scope_Len_Bytes, 4);
		}
		else
		{
			strcpy(NewScopePath, "overlays/Scopes/");
			
			strcat(NewScopePath, ScopeOverlays[Menu::Get.Visuals.ScopeLen].data());

			static unsigned long Protection_Backup{ 0 };

			VirtualProtect(Scope_Len_path, sizeof(void*), PAGE_EXECUTE_READWRITE, &Protection_Backup);

			*(void**)Scope_Len_path = &NewScopePath;

			VirtualProtect(Scope_Len_path, sizeof(void*), Protection_Backup, nullptr);
		}

		if (HudScope) CHudScope_Init(HudScope);

		mmcopy(Scope_Len_path, Backup_Of_Scope_Len_Bytes, 4); // Restore
	}
}

char NewNightVisionPath[MAX_PATH]; 

char Backup_Of_NightVision_Bytes[4]; 

void UpdateNightVision(bool Unload) noexcept
{
	static auto NightVision_path = reinterpret_cast<BYTE*>(
		Tools::FindPattern("client.dll", "68 ? ? ? ? 68 ? ? ? ? FF 90 ? ? ? ? 8B D8 85 DB") + 0x6);

	if (NightVision_path) {

		[[maybe_unused]] static auto Once = []() -> bool
		{
			mmcopy(Backup_Of_NightVision_Bytes, NightVision_path, 4);

			mmcopy(overlaycolor, (Color&)Menu::Get.Colors.General.Nightvision.Get(), 4);

			if (auto pLocal = CBaseEntity::GetLocalPlayer(); pLocal)
			{
				pLocal->IsNightVisionOn() = pLocal->GetNightVisionAlpha() = Menu::Get.Colors.General.Nightvision[Color::Elements::Alpha];
			}

			return true;
		}();

		if (Menu::Get.Visuals.NightVision == 0 || Unload)
		{
			mmcopy(NightVision_path, Backup_Of_NightVision_Bytes, 4);
		}
		else
		{
			strcpy(NewNightVisionPath, "effects/nightvisions/");
			
			strcat(NewNightVisionPath, NightVisions[Menu::Get.Visuals.NightVision].data());

			static unsigned long Protection_Backup{ 0 };

			VirtualProtect(NightVision_path, sizeof(void*), PAGE_EXECUTE_READWRITE, &Protection_Backup);

			*(void**)NightVision_path = &NewNightVisionPath;

			VirtualProtect(NightVision_path, sizeof(void*), Protection_Backup, nullptr);
		}
	}
}

void ForceFullUpdate() noexcept
{
	static auto ForceFullUpdate = reinterpret_cast<void(__thiscall*)(void*)>(
		Tools::FindPattern("engine.dll", "56 8B F1 83 BE A0 01"));

	ForceFullUpdate(ClientState);
}

void ProcessGameEventListeners(void* WriteListenEventList) noexcept
{
	void __stdcall Hooked_WriteListenEventList(void*);

	if (auto NetChannel = ClientState->GetNetChannel(); NetChannel) {

		CLC_ListenEvents msg;

		*(void**)&msg = CLC_ListenEvents_Table;

		if (!WriteListenEventList) {

			Hooked_WriteListenEventList(&msg);
		}
		else {

			reinterpret_cast<void(__thiscall*)(void*, void*)>(
				WriteListenEventList)(EventManager, &msg);
		}

		NetChannel->SendNetMsg((void*&)msg);
	}
}

__declspec(naked) void __cdecl Invoke_NET_SetConVar(void* pfn, const char* cvar, const char* value) noexcept
{
	__asm
	{
		push    ebp
		mov     ebp, esp
		sub     esp, 2Ch
		push    esi
		push    edi
		mov     edi, cvar
		mov     esi, value
		jmp     pfn
	}
}

void NET_SetConVar(const char* cvar, const char* value) noexcept
{
	static void* pvSetConVar = (void*)(Tools::FindPattern("engine.dll", "56 57 8D 4D D4"));

	Invoke_NET_SetConVar(pvSetConVar, cvar, value);
}

void UpdateThirdPerson() noexcept
{
	static auto& m_vecCameraOffset = **(Vector**)(Tools::FindPattern("client.dll", "D8 05 ? ? ? ? D9 5F 04") + 2);

	Input->IsCameraInThirdPerson() = Menu::Get.Misc.m_vecCameraOffset;

	m_vecCameraOffset.z = static_cast<float>(Menu::Get.Misc.m_vecCameraOffset);
}

static auto GameResources() -> C_PlayerResource*
{
	static auto GameResources = reinterpret_cast<C_PlayerResource * (__cdecl*)()>(
		Tools::CallableFromRelative(Tools::FindPattern("client.dll", "E8 ? ? ? ? 8B D8 85 DB 75 07 32 C0")));

	return GameResources();
}

bool IsGoodItem(int size, int& ID, bool reset_if_not_valid) noexcept
{
	auto RetV = ID > -1 && ID < size;

	if (!RetV && reset_if_not_valid) ID = 0;

	return RetV;
}

void Load::Once(HWND& hWindow, IDirect3DDevice9*& DirectXDevice)
{
	DirectXDevice = **(IDirect3DDevice9***)(Tools::FindPattern("shaderapidx9.dll", "8B 0D ? ? ? ? F7 DF") + 2);

	hWindow = FindWindowA("Valve001", nullptr); assert(hWindow);

	void Initialize_GUI(IDirect3DDevice9*); Initialize_GUI(DirectXDevice);

	ClientEntityListner::Singleton()->Initialize();

	if (cl_downloadfilter)
	{
		strcpy(cl_downloadfilter_backup_value, cl_downloadfilter->GetString());

		cl_downloadfilter->SetValue("all");
	}
}

void Load::Repeatedly()
{
	if (Engine->IsConnected() == 0) 
		return;

	if (fog_color)
	{
		char Buffer[64];

		snprintf(Buffer, sizeof Buffer, "%i %i %i",
			Menu::Get.Colors.General.Fog[0],
			Menu::Get.Colors.General.Fog[1],
			Menu::Get.Colors.General.Fog[2]);

		fog_color->SetValue(Buffer);
	}

	if (Fog_Enable)
		Fog_Enable->SetValueAndFlags(!Menu::Get.Visuals.NoFog);

	if (Fog_Override)
		Fog_Override->SetValueAndFlags(Menu::Get.Visuals.NoFog);

	if (r_rainwidth)
		r_rainwidth->SetValueAndFlags(Menu::Get.Visuals.Weather.Width);

	if (r_rainspeed)
		r_rainspeed->SetValueAndFlags(Menu::Get.Visuals.Weather.Speed);

	if (r_RainRadius)
		r_RainRadius->SetValueAndFlags(Menu::Get.Visuals.Weather.Radius);

	if (r_rainlength)
		r_rainlength->SetValueAndFlags(Menu::Get.Visuals.Weather.Length);

	if (fog_maxdensity)
		fog_maxdensity->SetValueAndFlags(Menu::Get.Visuals.FOG.maxdensity);

	if (snd_musicvolume)
		snd_musicvolume->SetValueAndFlags(Menu::Get.Misc.Sounds.MusicVolume * 0.01f);

	if (Menu::Get.Visuals.SkyBoxIndex.Get())
	{
		using Type = bool(__cdecl*)(const char*);

		reinterpret_cast<Type>(LoadSkyBox)(SkyBoxes[Menu::Get.Visuals.SkyBoxIndex.Get()].c_str());
	}

	UpdateScopeLens();

	Trails::Precache();

	UpdateNightVision();

	UpdateThirdPerson();

	SkinChanger::PrecacheModels();

	Update_Net_Graph_Text_Colors();

	PlayerResource = GameResources();
	
	World_Textures::NightModeModulateAction = MaterialActions::Update;

	World_Textures::SkyBoxColorModulateAction = MaterialActions::Update;

	blueblacklargebeam.first = ItemsMat.first = MaterialActions::Update;
}

void Unload::PreUnHook(void* WriteListenEventList)
{
	ProcessGameEventListeners(WriteListenEventList);

	Menu::Get.Colors.General.HudColor = Color{ 255, 176, 0, 120 };

	World_Textures::WorldColorModulateAction = MaterialActions::Unload;

	World_Textures::SkyBoxColorModulateAction = MaterialActions::Unload;

	blueblacklargebeam.first = ItemsMat.first = MaterialActions::Unload;

	Sleep(100U);
}

void Unload::PostUnHook()
{
	if (sv_skyname)
	{
		using Type = bool(__cdecl*)(const char*);

		reinterpret_cast<Type>(LoadSkyBox)(sv_skyname->GetString());
	}

	if (cl_downloadfilter)
		cl_downloadfilter->SetValue(cl_downloadfilter_backup_value);

	if (CCCrosshair)
		CCCrosshair->SetValueAndFlags(1);

	if (auto Entity = EntityList->GetClientEntity(MAX_EDICTS - 1); Entity)
		Entity->Remove();

	if (Trails::Entity) {

		Trails::Entity->Remove();
	}

	HP_Bar::ClearSprites();

	if (auto pLocal = GetLocalPlayerBase; pLocal)
	{
		pLocal->GetNightVisionAlpha() = pLocal->IsNightVisionOn() = false;
	}

	constexpr char Original_Bytes[] = { 0, 255, 0, 255 };

	if (overlaycolor) mmcopy(overlaycolor, Original_Bytes, 4);

	if (AddFifteen) {

		unsigned long Protection_Backup{ 0 };

		VirtualProtect(AddFifteen, 4, PAGE_EXECUTE_READWRITE, &Protection_Backup);

		*AddFifteen = 15.0f;

		VirtualProtect(AddFifteen, 4, Protection_Backup, nullptr);
	}

	UpdateNightVision(true);

	Set_DisConnection_Msg(NULL, true);

	Update_Net_Graph_Text_Colors(true);

	ClientEntityListner::Singleton()->End();

	void ForceFullUpdate(); ForceFullUpdate();

	ImGui_ImplDX9_Shutdown(); ImGui_ImplWin32_Shutdown(); ImGui::DestroyContext();
}
