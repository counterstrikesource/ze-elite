#pragma once

class ConVar;
class IConVar;
class CCommand;
class ConCommand;

#include "Defintions.h"

class ConCommandBase
{
	DECLARE_OFFSET_FUNCTION(GetFlags, 0x14, int);
public:

	void SetFlags(int flags)
	{
		if (flags != -1) {

			GetFlags() = flags;
		}
	}

	void RemoveFlags(int flags)
	{
		GetFlags() &= ~(flags);
	}

	bool IsFlagSet(int flag)
	{
		return (GetFlags() & flag) != 0;
	}

	DECLARE_OFFSET_FUNCTION(GetName, 0xC, const char*);
	DECLARE_OFFSET_FUNCTION(GetNext, 0x4, ConCommandBase*);
};

class ConCommand : public ConCommandBase
{
public:
	
};

class ConVar : public ConCommandBase
{

public:

	static void Create
	(
		void* ECX,
		const char* Name,
		const char* DefaultValue, int Flags,
		const char* Description, void* Fn = nullptr
	);

	void SetValue(const char* value)
	{
		using Type = void(__thiscall*)(void*, const char*);

		GetVirtualMethod<Type>(PVOID((unsigned int)this + 24), 2)(PVOID((unsigned int)this + 24), value);
	}

	void SetValue(float value)
	{
		if (GetFloat() != value) {
			GetFloat() = value;
		}
	}

	void SetValue(int value)
	{
		if (GetInt() != value) {
			GetInt() = value;
		}
	}

public:

	void Revert(void);

	int& GetInt(void) const;

	float& GetFloat(void) const;

	const char* GetDefault(void) const;

	const char* GetString(void) const;

	void SetValueAndFlags(float Value, const int Flags = -1);
};

enum CvarFlags
{
	NONE = 0,
	DEVELOPMENTONLY = (1 << 1),
	CLIENTDLL = (1 << 3),
	HIDDEN = (1 << 4),
	NOTIFY = (1 << 8),
	CHEAT = (1 << 14),
	NEVER_AS_STRING = (1 << 12),
	REPLICATED = (1 << 13),
	NOT_CONNECTED = (1 << 22),
	SERVER_CANNOT_QUERY = (1 << 29)
};

inline constexpr std::array FlagsName
{
	"CHEAT",
	"HIDDEN",
	"REPLICATED",
	"NOT_CONNECTED",
	"DEVELOPMENTONLY"
};

inline constexpr std::array FlagsValue
{
	CHEAT,
	HIDDEN,
	REPLICATED,
	NOT_CONNECTED,
	DEVELOPMENTONLY,
};

static_assert(FlagsName.size() == FlagsValue.size());