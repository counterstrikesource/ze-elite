#include "steamid.h"

static int iInputLen;

static unsigned int uTreasure;

#define astrlen(x) _countof(x) - 1

static char s_szDictionary[] = { "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" };

static void CreateRandomString(char* pszDest, int nLength) noexcept
{
	const char c_szAlphaNum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	for (int i = 0; i < nLength; ++i)
		pszDest[i] = c_szAlphaNum[rand() % (sizeof(c_szAlphaNum) - 1)];

	pszDest[nLength] = '\0';
}

static bool ScanLast3(char* pszInput, unsigned int uPrevHash) noexcept
{
	unsigned int h1, h2, h3, hh;
	for (int i1 = 0; i1 < astrlen(s_szDictionary); i1++)
	{
		h1 = uPrevHash ^ ((uPrevHash >> 2) + (uPrevHash << 5) + s_szDictionary[i1]);
		hh = h1 ^ ((h1 >> 2) + (h1 << 5));
		hh = hh ^ ((hh >> 2) + (hh << 5));

		if ((hh ^ uTreasure) >> (8 + 5 + 3))
			continue;

		for (int i2 = 0; i2 < astrlen(s_szDictionary); i2++)
		{
			h2 = h1 ^ ((h1 >> 2) + (h1 << 5) + s_szDictionary[i2]);
			hh = h2 ^ ((h2 >> 2) + (h2 << 5));
			if ((hh ^ uTreasure) >> (8 + 3))
				continue;

			for (int i3 = 0; i3 < astrlen(s_szDictionary); i3++)
			{
				h3 = h2 ^ ((h2 >> 2) + (h2 << 5) + s_szDictionary[i3]);
				if (h3 == uTreasure)
				{
					pszInput[iInputLen - 3] = s_szDictionary[i1];
					pszInput[iInputLen - 2] = s_szDictionary[i2];
					pszInput[iInputLen - 1] = s_szDictionary[i3];
					return true;
				}
			}
		}
	}
	return false;
}


static bool ScanNext(char* pszInput, int uIndex, unsigned int uPrevHash) noexcept
{
	bool res;

	for (int i = 0; i < astrlen(s_szDictionary); i++)
	{
		auto h = uPrevHash ^ ((uPrevHash >> 2) + (uPrevHash << 5) + s_szDictionary[i]);

		if (uIndex + 1 < iInputLen - 3)
			res = ScanNext(pszInput, uIndex + 1, h);
		else
			res = ScanLast3(pszInput, h);

		if (res)
		{
			pszInput[uIndex] = s_szDictionary[i];
			return true;
		}
	}
	return false;
}

static unsigned int RevHash(char* pszString) noexcept
{
	int i = 0;
	unsigned int nHash = 0x4E67C6A7;
	int c = pszString[i++];

	while (c)
	{
		nHash = nHash ^ ((nHash >> 2) + (nHash << 5) + c);
		c = pszString[i++];
	}

	return nHash;
}

static bool RevSpoof(char* pszDest, int uSID) noexcept
{
	uTreasure = uSID;
	iInputLen = strlen(pszDest);

	auto i = iInputLen - 7;
	i = (i < 0) ? 0 : i;
	pszDest[i] = '\0';
	auto h = RevHash(pszDest);
	return ScanNext(pszDest, i, h);
}

int GenerateRevEmu2013(void* pDest, uint32_t nSteamID, uint32_t* outRevHash) noexcept
{
	int PacketSize = 0;
	if (!PacketSize)
	{
		PacketSize = 202;
	}

	char szhwid[33];

	for (int tries = 5; ; tries--)
	{
		CreateRandomString(szhwid, 32);
		if (!RevSpoof(szhwid, nSteamID) && !tries)
			return 0;
		else
			break;
	}

	memset(pDest, 0, PacketSize);


	auto pTicket = (int*)pDest;
	auto pbTicket = (unsigned char*)pDest;

	auto revHash = RevHash(szhwid);

	if (revHash == 0)
		revHash = 0xDFDC1C3D;

	pTicket[0] = 'S';                      // +0
	pTicket[1] = revHash;                  // +4
	pTicket[2] = 'rev';					   // +8 'rev'
	pTicket[3] = 0;                        // +12
	pTicket[4] = revHash << 1;             // +16
	pTicket[5] = 0x01100001;               // +20
	pTicket[6] = (int)_time64(0) + 90123;  // +24
	pbTicket[27] = ~(pbTicket[27] + pbTicket[24]);
	pTicket[7] = ~(int)_time64(0);         // +28
	pTicket[8] = revHash * 2 >> 3;         // +32
	pTicket[9] = 0;                        // +36

	if (outRevHash)
		*outRevHash = revHash;

	const char c_szAESKeyRand[] = "0123456789ABCDEFGHIJKLMNOPQRSTUV";

	char szAESHashRand[32];
	auto AESRand = CRijndael();
	AESRand.MakeKey(c_szAESKeyRand, CRijndael::sm_chain0, 32, 32);
	AESRand.EncryptBlock(szhwid, szAESHashRand);
	memcpy(&pbTicket[40], szAESHashRand, 32);

	const char c_szAESKeyRev[] = "_YOU_SERIOUSLY_NEED_TO_GET_LAID_";
	char AESHashRev[32];
	auto AESRev = CRijndael();
	AESRev.MakeKey(c_szAESKeyRev, CRijndael::sm_chain0, 32, 32);
	AESRev.EncryptBlock(c_szAESKeyRand, AESHashRev);
	memcpy(&pbTicket[72], AESHashRev, 32);

	char szSHAHash[32];
	auto sha = CSHA(CSHA::SHA256);
	sha.AddData(szhwid, 32);
	sha.FinalDigest(szSHAHash);
	memcpy(&pbTicket[104], szSHAHash, 32);


	int i = 0;
	for (i = 136; i < 167; i++)
		pbTicket[i] = 0;
	pbTicket[168] = 32;
	for (i = 169; i < 171; i++)
		pbTicket[i] = 0;
	pbTicket[172] = 3;
	pbTicket[176] = 14;
	pbTicket[180] = 66;
	pbTicket[181] = 90;
	pbTicket[182] = 104;
	pbTicket[183] = 49;
	pbTicket[184] = 23;
	pbTicket[185] = 114;
	pbTicket[186] = 69;
	pbTicket[187] = 56;
	pbTicket[188] = 80;
	pbTicket[189] = 144;


	return PacketSize;
}