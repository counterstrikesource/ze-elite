#pragma once

class CUserCmd;
class CBaseEntity;
class IMoveHelper;
class CMoveData;

class IPrediction
{
public:

	void Update
	(
		int startframe,
		bool validframe,
		int incoming_acknowledged,
		int outgoing_command
	)
	{
		using Type = void(__thiscall*)(void*, int, bool, int, int);

		GetVirtualMethod<Type>(this, 3)(this, startframe, validframe, incoming_acknowledged, outgoing_command);
	}

	void SetupMove(CBaseEntity* player, CUserCmd* cmd, IMoveHelper* pHelper, CMoveData* move)
	{
		using Type = void(__thiscall*)(void*, CBaseEntity*, CUserCmd*, IMoveHelper*, CMoveData*);

		GetVirtualMethod<Type>(this, 18)(this, player, cmd, pHelper, move);
	}

	void FinishMove(CBaseEntity* player, CUserCmd* cmd, CMoveData* move)
	{
		using Type = void(__thiscall*)(void*, CBaseEntity*, CUserCmd*, CMoveData*);

		GetVirtualMethod<Type>(this, 19)(this, player, cmd, move);
	}

	DECLARE_OFFSET_FUNCTION(IsEnginePaused, 0xB, bool);
};