#ifndef CCSPlayer_H
#define CCSPlayer_H

#pragma once

class CBaseCombatWeapon;
class CCSWeaponInfo;
class CUserCmd;

class CBaseEntity
{
#define GetClientRenderable PVOID(this + IClientRenderable)
#define GetClientNetworkable PVOID(this + IClientNetworkable)
#define GetClientThinkable PVOID(this + IClientThinkable)
public:
	NETVAR(GetFlags, DT_BasePlayer, m_fFlags, uintptr_t);

	NETVAR(GetPrecipType, DT_Precipitation, m_nPrecipType, uintptr_t);

	NETVAR(GetRenderMode, DT_BaseEntity, m_nRenderMode, std::uint8_t);

	NETVAR(GetShotsFired, DT_CSPlayer, m_iShotsFired, uintptr_t);

	NETVAR(GetPunchAngle, DT_BasePlayer, m_vecPunchAngle, Vector);

	NETVAR(GetWaterLevel, DT_BasePlayer, m_nWaterLevel, char);

	NETVAR(GetTickBase, DT_BasePlayer, m_nTickBase, uintptr_t);

	NETVAR(GetViewModel, DT_BasePlayer, m_hViewModel[0], void*);

	NETVAR(GetVelocity, DT_BasePlayer, m_vecVelocity[0], Vector);

	NETVAR(GetHitboxSet, DT_BaseAnimating, m_nHitboxSet, int);

	NETVAR(GetModelIndex, DT_BaseEntity, m_nModelIndex, int);

	NETVAR(GetTeamNum, DT_BaseEntity, m_iTeamNum, uintptr_t);

	NETVAR(GetNetworkMoveParent, DT_BaseEntity, moveparent, int);

	NETVAR(GetAccount, DT_CSPlayer, m_iAccount, int);

	NETVAR(GetArmorValue, DT_CSPlayer, m_ArmorValue, int);

	NETVAR(HasHelmet, DT_CSPlayer, m_bHasHelmet, bool);

	NETVAR(GetFlashMaxAlpha, DT_CSPlayer, m_flFlashMaxAlpha, float);

	NETVAR(GetVecViewOffset, DT_BasePlayer, m_vecViewOffset[0], Vector);

	NETVAR(GetNextAttack, DT_BaseCombatCharacter, m_flNextAttack, float);

	NETVAR(GetVecOrigin, DT_BaseEntity, m_vecOrigin, Vector);

	NETVAR(GetThrower, DT_BaseGrenade, m_hThrower, BYTE);

	NETVAR(GetEyeAngles, DT_CSPlayer, m_angEyeAngles[0], Vector);

	NETVAR(GetRenderColor, DT_BaseEntity, m_clrRender, Color);

	NETVAR(GetOwner, DT_BaseCombatWeapon, m_hOwner, uint8_t);

	NETVAR(GetMins, DT_BaseEntity, m_vecMins, Vector&);

	NETVAR(GetMaxs, DT_BaseEntity, m_vecMaxs, Vector&);

	NETVAR(GetEffects, DT_BaseEntity, m_fEffects, uint32_t);

	NETVAR(IsNightVisionOn, DT_CSPlayer, m_bNightVisionOn, uint32_t);

	DECLARE_OFFSET_FUNCTION(GetCurrentUserCommand, 0x101C, CUserCmd*);

public:

	bool IsPlayer()
	{
		using Type = bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 131)(this);
	}

	bool IsAlive()
	{
		using Type = bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 130)(this);
	}

	int GetHealth()
	{
		using Type = int(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 106)(this);
	}

	int GetMaxHealth()
	{
		using Type = int(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 107)(this);
	}

	datamap_t* GetPredDescMap()
	{
		using Type = datamap_t * (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 15)(this);
	}

	const Vector& GetAbsAngles()
	{
		using Type = const Vector& (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 10)(this);
	}

	const Vector& GetAbsOrigin()
	{
		using Type = const Vector& (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 9)(this);
	}

	CBaseCombatWeapon* GetActiveWeapon()
	{
		using Type = CBaseCombatWeapon * (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 222)(this);
	}

	void SetLocalViewAngles(const QAngle& viewAngles)
	{
		using Type = void(__thiscall*)(void*, const QAngle&);

		GetVirtualMethod<Type>(this, 293)(this, viewAngles);
	}

	void GetRenderBounds(Vector& mins, Vector& maxs)
	{
		using Type = void(__thiscall*)(void*, Vector&, Vector&);

		return GetVirtualMethod<Type>(GetClientRenderable, 20)(GetClientRenderable, mins, maxs);
	}

	const Vector& WorldSpaceCenter()
	{
		using Type = Vector & (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 66)(this);
	}

	const Vector& GetRenderOrigin()
	{
		using Type = const Vector& (__thiscall*)(void*);

		return GetVirtualMethod<Type>(GetClientRenderable, 1)(GetClientRenderable);
	}

	matrix3x4& GetRgflCoordinateFrame()
	{
		using Type = matrix3x4 & (__thiscall*)(void*);

		return GetVirtualMethod<Type>(GetClientRenderable, 34)(GetClientRenderable);
	}

	bool SetupBones(matrix3x4* pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime)
	{
		using Type = bool(__thiscall*)(void*, matrix3x4*, int, int, float);

		return GetVirtualMethod<Type>(GetClientRenderable, 16)(GetClientRenderable, pBoneToWorldOut, nMaxBones, boneMask, currentTime);
	}

	model_t* GetModel()
	{
		using Type = const model_t* (__thiscall*)(void*);

		return const_cast<model_t*>(GetVirtualMethod<Type>(GetClientRenderable, 9)(GetClientRenderable));
	}

	bool ShouldDraw(void)
	{
		using Type = bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(GetClientRenderable, 3)(GetClientRenderable);
	}

	int DrawModel(int flags)
	{
		using Type = int(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(GetClientRenderable, 10)(GetClientRenderable, flags);
	}

	int GetIndex() const
	{
		using Type = int(__thiscall*)(void*);

		return GetVirtualMethod<Type>(GetClientNetworkable, 9)(GetClientNetworkable);
	}

	bool IsDormant()
	{
		using Type = bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(GetClientNetworkable, 8)(GetClientNetworkable);
	}

	void OnPreDataChanged(int updateType)
	{
		using Type = void(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(GetClientNetworkable, 4)(GetClientNetworkable, updateType);
	}

	void OnDataChanged(int updateType)
	{
		using Type = void(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(GetClientNetworkable, 5)(GetClientNetworkable, updateType);
	}

	void PreDataUpdate(int updateType)
	{
		using Type = void(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(GetClientNetworkable, 6)(GetClientNetworkable, updateType);
	}

	void PostDataUpdate(int updateType)
	{
		using Type = void(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(GetClientNetworkable, 7)(GetClientNetworkable, updateType);
	}

	void ClientThink()
	{
		using Type = void(__thiscall*)(void*);

		GetVirtualMethod<Type>(GetClientThinkable, 1)(GetClientThinkable);
	}

public:

	void FollowEntity(void* pEntity, int attachment = 1)
	{
		SetParent(pEntity, attachment);

		GetEffects() |= EF_BONEMERGE;

		GetMoveType() == MOVETYPE_NONE;
	}

	const char* GetClassname()
	{
		if (auto PredDescMap = GetPredDescMap(); PredDescMap)
			return PredDescMap->ClassName;

		return "Unknown";
	}

	bool IsEffectActive(int Effect)
	{
		return (GetEffects() & Effect) != 0;
	}

	const Vector GetEyePosition()
	{
		return GetVecViewOffset() + GetVecOrigin();
	}

	bool IsOnGround()
	{
		return (GetFlags() & FL_ONGROUND) != 0;
	}

	bool IsInAir()
	{
		return IsOnGround() == No;
	}

	bool IsDead()
	{
		return IsAlive() == No;
	}

	bool IsEnemy(CBaseEntity* Entity)
	{
		return GetTeamNum() != Entity->GetTeamNum();
	}

public:

	void Remove() noexcept;

	char GetMoveType() noexcept;

	Vector GetBonePos(int i) noexcept;

	const char* GetDebugName() noexcept;

	uintptr_t GetCreationTick() noexcept;

	const char* GetName() const noexcept;

	float& GetNightVisionAlpha() noexcept;

	CBaseEntity* GetOwnerEntity() noexcept;

	Vector& EstimateAbsVelocity() noexcept;

	void SetModelByIndex(int index) noexcept;

	const Vector GetHitboxPosition(int iHitbox) noexcept;

	void SetParent(void* pParent, int attachment) noexcept;

	void UpdateButtonState(int nUserCmdButtonMask) noexcept;

	bool CanSeePlayer(CBaseEntity* player, const Vector& pos) noexcept;


public:
	static CBaseEntity* GetLocalPlayer() noexcept;

#undef GetClientRenderable
#undef GetClientNetworkable
#undef GetClientThinkable
};


class CBaseCombatWeapon : public CBaseEntity
{
public:

	NETVAR(GetClip1, DT_BaseCombatWeapon, m_iClip1, uintptr_t);

	NETVAR(GetNextPrimaryAttack, DT_BaseCombatWeapon, m_flNextPrimaryAttack, float);

	NETVAR(GetWeaponMode, DT_WeaponCSBase, m_weaponMode, int32_t);

public:

	bool IsFullAuto()
	{
		return GetVirtualMethod<bool(__thiscall*)(void*)>(this, 363)(this);
	}

	int GetWeaponID()
	{
		return GetVirtualMethod<int(__thiscall*)(void*)>(this, 365)(this);
	}

	float GetInaccuracy()
	{
		return GetVirtualMethod<float(__thiscall*)(void*)>(this, 376)(this);
	}

	float GetSpread()
	{
		return GetVirtualMethod<float(__thiscall*)(void*)>(this, 377)(this);
	}

	void UpdateAccuracyPenalty()
	{
		return GetVirtualMethod<void(__thiscall*)(void*)>(this, 378)(this);
	}

public:

	bool IsC4()
	{
		return GetWeaponID() == WEAPON_C4;
	}

	bool IsKnife()
	{
		return GetWeaponID() == WEAPON_KNIFE;
	}

	bool IsGrenade()
	{
		int ID = GetWeaponID();

		return (ID == WEAPON_HEGRENADE || ID == WEAPON_SMOKEGRENADE || ID == WEAPON_FLASHBANG);
	}

	bool CanFire(CBaseEntity* pEntity)
	{
		float ServerTime = pEntity->GetTickBase() * Globals->interval_per_tick;

		return GetNextPrimaryAttack() <= ServerTime;
	}

public:

	CCSWeaponInfo* GetWpnData() noexcept;
};

class CSprite : public CBaseEntity
{
public:

	NETVAR(GetSpriteScale, DT_Sprite, m_flSpriteScale, float);
};

class CSpriteTrail : public CSprite
{
public:

	NETVAR(GetLifeTime, DT_SpriteTrail, m_flLifeTime, float);
	NETVAR(GetStartWidth, DT_SpriteTrail, m_flStartWidth, float);
	NETVAR(GetEndWidth, DT_SpriteTrail, m_flEndWidth, float);
	NETVAR(GetTextureRes, DT_SpriteTrail, m_flTextureRes, float);
};


class C_PlayerResource
{
public:

	const char* GetClanTag(size_t INDEX) noexcept;
};

class CHudTexture;

class CCSWeaponInfo
{
public:

	DECLARE_OFFSET_FUNCTION(GetRange, 0x88C, float);
	DECLARE_OFFSET_FUNCTION(GetDamage, 0x888, intptr_t);
	DECLARE_OFFSET_FUNCTION(GetBullets, 0x894, intptr_t);
	DECLARE_OFFSET_FUNCTION(GetArmorRatio, 0x70C, float);
	DECLARE_OFFSET_FUNCTION(GetMuzzleScale, 0x880, float);
	DECLARE_OFFSET_FUNCTION(GetAmmoType, 0x6C0, intptr_t);
	DECLARE_OFFSET_FUNCTION(GetRangeModifier, 0x890, float);
	DECLARE_OFFSET_FUNCTION(GetPenetration, 0x884, intptr_t);
	DECLARE_OFFSET_FUNCTION(GetIconActive, 0x6D0, CHudTexture*);
};

#endif
