#pragma once

struct datamap_t;

class typedescription_t
{
public:
	uintptr_t Type;
	char* Name;
	intptr_t Offset;
	char Pad[20];
	datamap_t* td;
	char Pad0[16];
};


struct datamap_t
{
	typedescription_t* Desc;
	int NumFields;
	char const* ClassName;
	datamap_t* baseMap;
};
