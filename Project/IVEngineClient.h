#pragma once

typedef unsigned int UINT;

typedef struct player_info_s
{
	char			name[32];
	int				userID;
	char			guid[33];
	unsigned long	friendsID;
	char			friendsName[32];
	bool			fakeplayer;
	bool			ishltv;
	unsigned long	customFiles[4];
	unsigned char	filesDownloaded;
} player_info_t;

class CNetChan
{
public:
	float GetLatency(int flow)
	{
		return GetVirtualMethod<float(__thiscall*)(void*, int)>(this, 9)(this, flow);
	}

	bool SendNetMsg(void*& msg, bool bForceReliable = false, bool bVoice = false)
	{
		return GetVirtualMethod<bool(__thiscall*)(void*, void*&, bool, bool)>(this, 40)(this, msg, bForceReliable, bVoice);
	}
};

class IVEngineClient
{
public:

	void GetScreenSize(int& width, int& height)
	{
		using Type = void(__thiscall*)(void*, int&, int&);

		GetVirtualMethod<Type>(this, 5)(this, width, height);
	}

	bool GetPlayerInfo(int ent_num, player_info_t* pinfo)
	{
		using Type = bool(__thiscall*)(void*, int, player_info_t*);

		return GetVirtualMethod<Type>(this, 8)(this, ent_num, pinfo);
	}

	int GetPlayerForUserID(int userID)
	{
		using Type = int(__thiscall*)(void*, int);

		return GetVirtualMethod<Type>(this, 9)(this, userID);
	}

	bool Con_IsVisible(void)
	{
		using Type = bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 11)(this);
	}

	int GetLocalPlayer(void)
	{
		using Type = int(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 12)(this);
	}

	void GetViewAngles(Vector& va)
	{
		using Type = void(__thiscall*)(void*, Vector&);

		GetVirtualMethod<Type>(this, 19)(this, va);
	}

	void SetViewAngles(Vector& va)
	{
		using Type = void(__thiscall*)(void*, Vector&);

		GetVirtualMethod<Type>(this, 20)(this, va);
	}

	int GetMaxClients(void)
	{
		using Type = int(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 21)(this);
	}

	bool IsInGame(void)
	{
		using Type = bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 26)(this);
	}

	bool IsConnected(void)
	{
		using Type = bool(__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 27)(this);
	}

	const matrix3x4& WorldToScreenMatrix()
	{
		using Type = const matrix3x4& (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 36)(this);
	}

	const matrix3x4& WorldToViewMatrix()
	{
		using Type = const matrix3x4& (__thiscall*)(void*);

		return GetVirtualMethod<Type>(this, 37)(this);
	}

	void ClientCmd_Unrestricted(const char* szCmdString)
	{
		using Type = void(__thiscall*)(void*, const char*);

		GetVirtualMethod<Type>(this, 106)(this, szCmdString);
	}

	void ClientCmd_Unrestricted_Formatted(const char* fmt, ...)
	{
		va_list va_alist;
		char buf[256];
		va_start(va_alist, fmt);
		_vsnprintf(buf, sizeof(buf), fmt, va_alist);
		va_end(va_alist);

		ClientCmd_Unrestricted(buf);
	}
};
