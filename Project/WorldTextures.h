#ifndef WORLD_TEXTURES_H
#define WORLD_TEXTURES_H

namespace World_Textures
{
	extern MaterialActions NightModeModulateAction;
	extern MaterialActions WorldColorModulateAction;
	extern MaterialActions SkyBoxColorModulateAction;

	auto NightModeModulate() noexcept -> void;

	auto WorldColorModulate() noexcept -> void;

	auto SkyBoxColorModulate() noexcept -> void;
}

auto SingleMaterialModulate(std::pair<MaterialActions, pMaterial>& Material, MaterialVarFlags flag, const bool& MenuVar) noexcept -> void;

#endif
