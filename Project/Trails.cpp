#include "Trails.h"

#include "SDK.h"
#include "Assorted.h"

CSpriteTrail* Trails::Entity = nullptr;

void Trails::Precache() noexcept
{
    char Path[MAX_PATH];

    auto& [Names, ModelIndexs] = TrailMaterials;

    for (auto i = 0; i < Names.size(); i++)
    {
        strcpy(Path, "sprites/trails/");

        strcat(Path, Names[i].data());

        strcat(Path, ".vmt");

        PrecacheModel(Path);

        ModelIndexs.at(i) = ModelInfo->GetModelIndex(Path);
    }
}

void Trails::Create(CBaseEntity* pParent) noexcept
{
    if (Entity) return;

    using Type = CSpriteTrail * (__cdecl*)(const char*);

    if (auto Entity = reinterpret_cast<Type>(CreateEntityByName)("env_spritetrail"); Entity)
    {
        Entity->PreDataUpdate(0);

        Entity->OnPreDataChanged(0);

        Entity->GetStartWidth() = Menu::Get.Visuals.Trails.Width;

        Entity->GetTextureRes() = 0.05;

        Entity->GetEndWidth() = Menu::Get.Visuals.Trails.EndWidth;

        Entity->GetLifeTime() = Menu::Get.Visuals.Trails.LifeTime;

        Entity->GetRenderMode() = Menu::Get.Visuals.Trails.RenderMode;

        Entity->OnDataChanged(0);

        Entity->FollowEntity(pParent, 1);

        auto ModelIndex = TrailMaterials.second[Menu::Get.Visuals.Trails.SpriteTexture];

        if (ModelIndex != -1)
        {
            Entity->SetModelByIndex(ModelIndex);
        }
    }
}
