#include "SDK.h"
#include "VoiceChat.h"
#include "Assorted.h"

extern char DirectoryPath[MAX_PATH];

float VoiceChat::EndTime = FLT_MAX;

std::vector<AudioVoice> VoiceChat::SoundList{};

void VoiceChat::ProcessVoiceOff() noexcept
{
	if (EndTime != FLT_MAX && Globals->curtime > EndTime)
	{
		Engine->ClientCmd_Unrestricted("voice_loopback 0;-voicerecord");

		for (auto& SoundInfo : SoundList)
		{
			SoundInfo.KeyWasPressed = false;
		}

		EndTime = FLT_MAX;
	}
}

void VoiceChat::Run(AudioVoice* data) noexcept
{
	using Type = bool(__cdecl*)(const char*, const char*, const char*);

	if (EndTime != FLT_MAX)
		return;

	if(data == nullptr)
	{
		for (auto& SoundInfo : SoundList)
		{
			if (IsVirtualKeyPressed(SoundInfo.KeyBind) && !SoundInfo.KeyWasPressed)
			{
				SoundInfo.KeyWasPressed = true;

				EndTime = Globals->curtime + SoundInfo.SoundDuration;

				Engine->ClientCmd_Unrestricted("voice_loopback 1");

				std::string Path = DirectoryPath + std::string("sound\\ZE-Elite\\") + SoundInfo.Soundname + ".wav";

				reinterpret_cast<Type>(Voice_RecordStart)(NULL, NULL, Path.data());
			}
		}

		return;
	}

	Engine->ClientCmd_Unrestricted("voice_loopback 1");

	std::string Path = DirectoryPath + std::string("sound\\ZE-Elite\\") + data->Soundname + ".wav";

	reinterpret_cast<Type>(Voice_RecordStart)(NULL, NULL, Path.data());

	EndTime = Globals->curtime + data->SoundDuration;
}

void VoiceChat::PrepareFiles() noexcept
{
	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "sound\\ZE-Elite");

	if (DirectoryExists(TempPath)) {

		for (const auto& entry : FileSystem::directory_iterator(TempPath))
		{
			auto FilePath = entry.path().string();

			if (auto Found = FilePath.find_last_of("\\"); Found != std::string::npos)
			{
				strcpy(TempPath, (const char*)&FilePath[Found + 1]);

				if (Found = std::string(TempPath).find(".wav"); Found != std::string::npos)
				{
					char Temp[MAX_PATH];

					strcpy(Temp, "ZE-Elite\\");

					strcat(Temp, TempPath);

					TempPath[Found] = 0;

					using Type = float(__cdecl *)(const char*);

					VoiceChat::SoundList.push_back({ TempPath, reinterpret_cast<Type>(Audio_GetWaveDuration)(Temp) });
				}
			}
		}
	}
}
