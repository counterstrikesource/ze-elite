#include "Arrays.h"

#include "SDK.h"
#include "Assorted.h"

constexpr static auto Temp = "\\";

constexpr static std::array TempArray
{
	"bluemuzzle",
	"gunshipmuzzle",
	"gunshiptracer",
	"bluespark",
	"flashbang",
	"flashbang_white",
	"nightvision",
	"blood_gore",
	"blood_drop",
	"blood_puff",
	"muzzleflashX",
	"muzzleflash1",
	"muzzleflash2",
	"muzzleflash3",
	"muzzleflash4",
	"tesla_glow_noz",
	"combinemuzzle2",
	"fleck_cement1",
	"fleck_cement2",
	"fleck_antlion1",
	"fleck_antlion2",
	"fleck_wood1",
	"fleck_wood2",
	"blood",
	"blood2"
};

void CustomTextures::AssignTexture(IMaterial*& Pointer, int i) noexcept
{
	if (--i > -1 && i < Textures.size())
	{
		Pointer = Textures[i];
	}
}

void CustomTextures::PrepareFiles(const char* Folder) noexcept
{
	strcpy(TempPath, DirectoryPath);

	strcat(TempPath, "materials\\");

	strcat(TempPath, Folder);

	AddFilesNameFromPathToArray(m_szNames);

	for (auto i = 1; i < m_szNames.size(); i++)
	{
		strcpy(TempPath, Folder);

		strcat(TempPath, Temp);

		strcat(TempPath, m_szNames[i].data());

		if (auto pMaterial = MatSystemOther->FindMaterial(TempPath, 0); pMaterial)
		{
			pMaterial->AddRef();

			pMaterial->Precache();

			Textures.push_back(pMaterial);
		}
	}
}

void ProcessEffectsList() noexcept
{
	auto& [Effects, pMuzzles] = ListOfEffects;

	for (auto i = 0; i < TempArray.size(); i++)
	{
		strcpy(TempPath, "effects");

		strcat(TempPath, Temp);

		strcat(TempPath, TempArray[i]);

		Effects.m_szNames.push_back(TempArray[i]);

		if (auto pMaterial = MatSystemOther->FindMaterial(TempPath, 0); pMaterial)
		{
			pMaterial->AddRef();

			pMaterial->Precache();

			Effects.Textures.push_back(pMaterial);
		}
	}

	for (auto& Temp : Effects.Textures)
		pMuzzles.push_back(NULL);
}
